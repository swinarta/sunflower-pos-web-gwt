package com.swinarta.sunflower.web.gwt.client.widget;

import java.util.LinkedHashMap;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.injector.provider.criteria.GetAllCriteriaProvider;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.FormItemInputTransformer;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;

public class CustomComboBoxItem extends ComboBoxItem{

	private Boolean showAnyRecordOption = false;
	private DataSource dataSource;
	
	@Inject
	public CustomComboBoxItem(@Named("UpperCase") FormItemInputTransformer inputTransformer){
		setAutoFetchData(false);
		setInputTransformer(inputTransformer);
	}
			
	public void setDataSource(DataSource dataSource){
			this.dataSource = dataSource;
	}
	
	public void setShowAnyRecordOption(Boolean showAnyRecordOption){
		this.showAnyRecordOption = showAnyRecordOption;
	}

	public void fetchData(){
		
		GetAllCriteriaProvider p = new GetAllCriteriaProvider();
		
		Criteria criteria = null;
		
		if(p!=null){
			criteria = p.get();
		}
					
		dataSource.fetchData(criteria, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
				if(showAnyRecordOption){
					valueMap.put("-1", "");
					setDefaultValue("-1");
				}

				Record[] incomingData = response.getData();
				for (Record record : incomingData) {
					valueMap.put(record.getAttribute(getValueField()), record.getAttribute(getDisplayField()));						
				}
									
				setValueMap(valueMap);
				setDisplayField(null);
				setValueField(null);
			}
		});
	}
			
}
