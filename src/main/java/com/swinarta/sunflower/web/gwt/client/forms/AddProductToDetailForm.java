package com.swinarta.sunflower.web.gwt.client.forms;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class AddProductToDetailForm extends DynamicForm{

	private TextItem skuBarcodeTextItem = new TextItem("searchProduct", "Barcode/SKU");
	private TextItem qtyItem = new TextItem("qty", "Qty");
	private StaticTextItem addProductDescItem = new StaticTextItem("prodDesc");
	private StaticTextItem measurementDescItem = new StaticTextItem("measurement");
	private HiddenItem searchActionId = new HiddenItem("actionId");
	private HiddenItem productIdItem = new HiddenItem("productId");
		
	@Inject
	public AddProductToDetailForm(){
		
		setNumCols(3);
		
		skuBarcodeTextItem.setSelectOnFocus(true);
		skuBarcodeTextItem.setWrapTitle(false);
		skuBarcodeTextItem.setCharacterCasing(CharacterCasing.UPPER);
		skuBarcodeTextItem.setWidth(100);
		skuBarcodeTextItem.setColSpan(3);
		
		qtyItem.setLeft(5);
		qtyItem.setWidth(50);
		
		addProductDescItem.setShowTitle(false);
		addProductDescItem.setStartRow(false);
		addProductDescItem.setEndRow(false);
		addProductDescItem.setAlign(Alignment.LEFT);
		addProductDescItem.setWidth(200);
		addProductDescItem.setColSpan(3);
		
		measurementDescItem.setShowTitle(false);
		measurementDescItem.setStartRow(false);
		measurementDescItem.setAlign(Alignment.LEFT);
		measurementDescItem.setWidth(75);
		
		searchActionId.setValue("searchProduct");
		
		skuBarcodeTextItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		qtyItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);

		setFields(addProductDescItem, skuBarcodeTextItem, qtyItem, measurementDescItem, 
				searchActionId, productIdItem);		
		hideItem("qty");
		hideItem("prodDesc");
		hideItem("measurement");
		
		initState();
	}
	
	public void setShowQty(Boolean showQty){
		if(showQty){
			showItem("qty");
			showItem("prodDesc");
			showItem("measurement");
		}
	}
	
	public void initState(){
		clearValues();
		qtyItem.disable();
		skuBarcodeTextItem.enable();
		focus();
	}
	
	public void focus(){
		if(skuBarcodeTextItem.isDisabled()){
			qtyItem.focusInItem();
		}else{
			skuBarcodeTextItem.focusInItem();
		}
		super.focus();
	}
		
	
}
