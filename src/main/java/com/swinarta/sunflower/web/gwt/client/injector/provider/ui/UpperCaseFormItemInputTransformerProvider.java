package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.FormItemInputTransformer;
import com.smartgwt.client.widgets.form.fields.FormItem;

public class UpperCaseFormItemInputTransformerProvider implements Provider<FormItemInputTransformer>{

	public FormItemInputTransformer get() {
		
		FormItemInputTransformer f = new FormItemInputTransformer() {
			
			public Object transformInput(DynamicForm form, FormItem item, Object value, Object oldValue) {
				if (value != null) {
					String valueUp = (((String)value).toUpperCase());
					if (valueUp.matches("[A-Z0-9\\.\\-_]+" )) {
						// allow only uppercase char, digits, '.', '_' and '-' entries
						return valueUp;
					}
					// invalid input : return previous entry
					return oldValue;
				}
				return value;			
			}
		};
		return f;
	}

}
