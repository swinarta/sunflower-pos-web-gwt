package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem;

import com.google.inject.Provider;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class TermOfPaymentTextItemProvider implements Provider<TextItem> {

	public TextItem get() {
		TextItem item = new TextItem("termOfPayment", "Term Of Payment");
		item.setLength(3);
		item.setWidth(50);
		item.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		return item;
	}

}
