package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;

public class ConfigurationRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	
	@Inject
	public ConfigurationRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding
		){
		this.fetchOperationBinding = fetchOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/config/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding);
		
		DataSourceTextField keyTextField = new DataSourceTextField("key");
		DataSourceTextField valueTextField = new DataSourceTextField("value");
		
		ds.setFields(keyTextField, valueTextField);
		
		return ds;
	}

}