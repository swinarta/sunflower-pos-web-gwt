package com.swinarta.sunflower.web.gwt.client.forms;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.SubmitValuesEvent;
import com.smartgwt.client.widgets.form.events.SubmitValuesHandler;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class ProductMainForm extends DynamicForm{
	
	final private CheckboxItem active = new CheckboxItem("active", "Active");
	final private TextItem shortDescriptionTextItem = new TextItem("shortDescription", "Short Description");
	final private TextItem longDescriptionTextItem = new TextItem("longDescription", "Long Description");
	final private CheckboxItem autoSkuCheckboxItem = new CheckboxItem("autoSku", "Auto SKU");
	final private CheckboxItem autoBarcodeCheckboxItem = new CheckboxItem("autoBarcode", "Auto Barcode");
	final private TextItem skuTextItem = new TextItem("sku", "SKU");
	final private TextItem barcodeTextItem = new TextItem("barcode", "Barcode");
	final private CheckboxItem consignmentItem = new CheckboxItem("consignment");
	final private CheckboxItem scallableItem = new CheckboxItem("scallable");

	final private CustomComboBoxItem categoryComboBoxItem;

	private DSCallback saveCallback;
	
	@Inject
	public ProductMainForm(
			@Named("Products") RestDataSource productDataSource,
			@Named("Utility") RestDataSource utilityDataSource,
			@Named("Category") CustomComboBoxItem categoryComboBoxItem
			){

		this.categoryComboBoxItem = categoryComboBoxItem;

		
		setTitleOrientation(TitleOrientation.TOP);		
		setDataSource(productDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);
						
		setFields(active, categoryComboBoxItem, 
				autoSkuCheckboxItem, skuTextItem, autoBarcodeCheckboxItem, barcodeTextItem,
				longDescriptionTextItem, shortDescriptionTextItem,
				consignmentItem, scallableItem);
		
		init();
		initHandler();
	}
	
	protected void init(){
		skuTextItem.setWidth(100);
		skuTextItem.setLength(8);
		skuTextItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);

		longDescriptionTextItem.setWidth(200);
		longDescriptionTextItem.setLength(255);

		shortDescriptionTextItem.setWidth(170);
		shortDescriptionTextItem.setLength(24);

		shortDescriptionTextItem.setCharacterCasing(CharacterCasing.UPPER);
		longDescriptionTextItem.setCharacterCasing(CharacterCasing.UPPER);
		categoryComboBoxItem.setRequired(true);

		consignmentItem.setDefaultValue(false);
		scallableItem.setDefaultValue(false);

		barcodeTextItem.setWidth(125);
		barcodeTextItem.setLength(13);
		barcodeTextItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
	}
	
	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	@Override
	public void editNewRecord(){
		clearErrors(true);
		setAutoFocus(true);		
		initNewRecord();
		super.editNewRecord();
	}
	
	@Override
	public void editRecord(Record record){
		clearErrors(true);
		Integer categoryId = record.getAttributeAsRecord("category").getAttributeAsInt("id");
		categoryComboBoxItem.setDefaultValue(categoryId);
		active.setDefaultValue(!record.getAttributeAsBoolean("deleteInd"));
		
		initEditRecord();
		focusInItem("longDescription");
		setAutoFocus(true);
		super.editRecord(record);
	}

	private void initHandler(){
		
		addSubmitValuesHandler(new SubmitValuesHandler() {
			
			public void onSubmitValues(SubmitValuesEvent event) {
				saveData();
			}
		});
		
		autoSkuCheckboxItem.addChangedHandler(new ChangedHandler() {
			
			public void onChanged(ChangedEvent event) {
				Boolean value = (Boolean)event.getValue();
				changeSkuTextItemState(value);
			}
		});
		
		autoBarcodeCheckboxItem.addChangedHandler(new ChangedHandler() {
			
			public void onChanged(ChangedEvent event) {
				Boolean value = (Boolean)event.getValue();
				changeBarcodeTextItemState(value);
				autoSkuCheckboxItem.setDisabled(value);
				autoSkuCheckboxItem.setValue(value);
				changeSkuTextItemState(value);
			}
		});
		
		scallableItem.addChangedHandler(new ChangedHandler() {
			
			public void onChanged(ChangedEvent event) {
				Boolean value = (Boolean)event.getValue();
				autoSkuCheckboxItem.setDisabled(value);
				autoBarcodeCheckboxItem.setDisabled(value);
				skuTextItem.clearValue();
				barcodeTextItem.clearValue();
				if(value){
					skuTextItem.setLength(5);
				}else{
					skuTextItem.setLength(8);
				}
			}
		});
	}
	
	private void changeSkuTextItemState(boolean isAuto){
		skuTextItem.setRequired(!isAuto);
		skuTextItem.setDisabled(isAuto);
	}

	private void changeBarcodeTextItemState(boolean isAuto){
		barcodeTextItem.setRequired(!isAuto);
		barcodeTextItem.setDisabled(isAuto);
	}

	private void initNewRecord(){
		active.hide();
		active.setDisabled(true);
		
		autoSkuCheckboxItem.setDisabled(false);
		autoSkuCheckboxItem.setValue(false);
		autoBarcodeCheckboxItem.setValue(false);
		changeSkuTextItemState(false);
		changeBarcodeTextItemState(false);
	}
	
	private void initEditRecord(){
		active.show();
		active.setRequired(true);
		active.setDisabled(false);
		
		autoSkuCheckboxItem.hide();
		autoBarcodeCheckboxItem.hide();
		changeSkuTextItemState(false);
		changeBarcodeTextItemState(false);

	}
			
	public void saveData(){
		if(validate()){
			if(!active.isDisabled() && !active.getValueAsBoolean()){
				SC.ask("Product is set to NOT ACTIVE, CONTINUE?", new BooleanCallback() {				
					public void execute(Boolean value) {
						if(value){
							saveData(saveCallback);
						}
						
					}
				});
			}else{
				super.saveData(saveCallback);
			}
		}
	}
	
	public void setSku(String sku){
		skuTextItem.setValue(sku);
	}

	public void setBarcode(String Barcode){
		barcodeTextItem.setValue(Barcode);
	}
	
	public Boolean isAutoSku(){
		return autoSkuCheckboxItem.getValueAsBoolean();
	}

	public Boolean isAutoBarcode(){
		return autoBarcodeCheckboxItem.getValueAsBoolean();
	}

}