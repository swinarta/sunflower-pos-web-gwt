package com.swinarta.sunflower.web.gwt.client.injector.provider.validator;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;

public class BarcodeLengthRangeValidatorProvider implements Provider<LengthRangeValidator>{

	public LengthRangeValidator get() {
		LengthRangeValidator skuLengthValidator = new LengthRangeValidator();
		skuLengthValidator.setMin(13);
		skuLengthValidator.setMax(13);
		skuLengthValidator.setErrorMessage("Invalid Barcode Length");
		return skuLengthValidator;
	}

}
