package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.HLayout;

public class PromoBasicCanvas extends DataCanvas{

	private DisplayLabel id = new DisplayLabel("ID");
	private DisplayLabel startDate = new DisplayLabel("Start Date");
	private DisplayLabel endDate = new DisplayLabel("End Date");
	private DisplayLabel promoType = new DisplayLabel("Promo Type");
	private DisplayLabel promoValue = new DisplayLabel("Promo Value");
	private DisplayLabel description = new DisplayLabel("Description");

	@Inject
	public PromoBasicCanvas(){
		super(DataCanvasLayout.VERTICAL);

		setGroupTitle("Purchasing Order Information");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setMargin(3);
		//setForm(poForm);
		
		HLayout layout1 = new HLayout(15);
		layout1.addMember(id);
		layout1.addMember(description);
		
		HLayout layout2 = new HLayout(15);
		layout2.addMember(startDate);
		layout2.addMember(endDate);
		
		HLayout layout3 = new HLayout(15);
		layout3.addMember(promoType);
		layout3.addMember(promoValue);
		
		dataComposite.addMember(layout1);
		dataComposite.addMember(layout2);
		dataComposite.addMember(layout3);
		
	}
	
	public void loadData(Record r){
		id.setContents(r.getAttribute("id"));
		description.setContents(r.getAttribute("description"));
		startDate.setContents(r.getAttributeAsDate("startDate"), true);
		endDate.setContents(r.getAttributeAsDate("endDate"), true);
		promoType.setContents(r.getAttribute("promoType"));
		promoValue.setContents(r.getAttributeAsDouble("promoValue"));
	}
}
