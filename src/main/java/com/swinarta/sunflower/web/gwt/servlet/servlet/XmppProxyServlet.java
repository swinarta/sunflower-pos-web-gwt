package com.swinarta.sunflower.web.gwt.servlet.servlet;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;

public class XmppProxyServlet extends EPRoxyServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2707177497330730788L;	
	private Logger logger = Logger.getLogger("XmppProxyServlet");
	
	@Override
	public void init(ServletConfig servletConfig){
		
		String remotePath;
		String remoteServer;
		Integer remotePort;
		
		InitialContext ic;
		try {

			ic = new InitialContext();
			remotePath = (String)ic.lookup("java:comp/env/xmppServerPath");
			remoteServer = (String)ic.lookup("java:comp/env/xmppServerHost");
			remotePort = (Integer)ic.lookup("java:comp/env/xmppServerPort");

			setRemotePath(remotePath);
			setRemoteServer(remoteServer);
			setRemotePort(remotePort);		
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "error:" + e.getMessage());
		}		
		
	}
	
}
