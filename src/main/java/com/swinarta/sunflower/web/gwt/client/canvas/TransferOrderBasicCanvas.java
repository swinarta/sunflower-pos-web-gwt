package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.forms.TransferOrderForm;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;

public class TransferOrderBasicCanvas extends DataCanvas{

	private DisplayLabel toCode = new DisplayLabel("Transfer Code");
	private DisplayLabel status = new DisplayLabel("Status");
	private DisplayLabel toDate = new DisplayLabel("Transfer Date");
	private DisplayLabel remarks = new DisplayLabel("Remarks");
	private DisplayLabel fromStore = new DisplayLabel("From Store");
	private DisplayLabel toStore = new DisplayLabel("To Store");
		
	final private TransferOrderForm toForm;
	final private LinkItem processLinkItem;
	final private LinkItem completeLinkItem;
	final private LinkItem cancelLinkItem;
	final private LinkItem downloadAsPdfLinkItem;
	final private LinkItem downloadAsTextLinkItem;
	
	private Record record;
	private ListGrid listGrid;
	private DSCallback updateStatusCallback;
	private DSCallback cancelCallBack;
	private DSCallback processCallBack;
	private DSCallback completeCallBack;
	
	@Inject
	public TransferOrderBasicCanvas(
			TransferOrderForm toForm,
			@Named("DisplaySupplier") final Window supplierWindow,
			@Named("ProcessPurchasingOrder") LinkItem processLinkItem,
			@Named("CancelPurchasingOrder") LinkItem cancelLinkItem,
			@Named("CompleteReceivingOrder") LinkItem completeLinkItem,
			@Named("DownloadAsPdf") LinkItem downloadAsPdfLinkItem,
			@Named("DownloadAsText") LinkItem downloadAsTextLinkItem
		){
		super(DataCanvasLayout.VERTICAL);
		
		this.toForm = toForm;
		this.processLinkItem = processLinkItem;
		this.cancelLinkItem = cancelLinkItem;
		this.completeLinkItem = completeLinkItem;
		this.downloadAsPdfLinkItem = downloadAsPdfLinkItem;
		this.downloadAsTextLinkItem = downloadAsTextLinkItem;

		setGroupTitle("Transfer Order Information");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setMargin(3);
				
		setForm(toForm);
		
		final DSCallback saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
					addOrUpdateWindow.hide();
					updateStatusCallback.execute(response, rawData, request);
				}
				
			}
		};
		
		cancelCallBack = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Transfer Order is successfuly CANCELLED");
				saveCallback.execute(response, rawData, request);
			}
		};
		
		processCallBack = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Transfer Order is successfuly PROCESSED");
				saveCallback.execute(response, rawData, request);
			}
		};
		
		completeCallBack = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Transfer Order is successfuly COMPLETED");
				saveCallback.execute(response, rawData, request);
			}
		};
		
		toForm.setSaveCallback(saveCallback);

		HLayout layout1 = new HLayout(15);
		layout1.addMember(toCode);
		layout1.addMember(toDate);
		layout1.addMember(status);

		HLayout layout2 = new HLayout(15);
		layout2.addMember(fromStore);
		layout2.addMember(toStore);
		layout2.addMember(remarks);

		editForm.setWidth(500);
		editForm.setColWidths(100,100,100,100,100,100);
		editForm.setNumCols(4);
		editForm.setFields(editItemLink, processLinkItem, completeLinkItem, cancelLinkItem, downloadAsPdfLinkItem, downloadAsTextLinkItem);

		dataComposite.addMember(layout1);
		dataComposite.addMember(layout2);
		dataComposite.addMember(editForm);
		
		initWindow();
		initHandler();
	}

	public void setUpdateStatusCallback(DSCallback updateStatusCallback) {
		this.updateStatusCallback = updateStatusCallback;
	}

	public void setListGrid(ListGrid listGrid) {
		this.listGrid = listGrid;
	}

	private void initHandler(){
		processLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				processLinkClicked();
			}
		});
		cancelLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				cancelLinkClicked();
			}
		});
		
		completeLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				completeLinkClicked();
			}
		});
	}
	
	private void initWindow(){

	}
	
	public void loadData(Record r) {
		this.record = r;
		
		String statusStr = r.getAttributeAsString("status");
		
		toCode.setContents(r.getAttributeAsString("transferId"));
		status.setContents(statusStr);
		toDate.setContents(r.getAttributeAsDate("transferDate"), true);
		remarks.setContents(r.getAttributeAsString("remarks"));
		fromStore.setContents(r.getAttributeAsString("fromStoreCode"));
		toStore.setContents(r.getAttributeAsString("toStoreCode"));
		
		if("NEW".equalsIgnoreCase(statusStr)){
			processLinkItem.show();
			cancelLinkItem.show();
			editItemLink.show();
			completeLinkItem.hide();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			status.setContentStyle("poNewLabel");
		}else if("PROCESSED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.show();
			editItemLink.hide();
			downloadAsPdfLinkItem.show();
			downloadAsTextLinkItem.show();
			completeLinkItem.show();
			
			Integer id = r.getAttributeAsInt("id");
			downloadAsPdfLinkItem.setValue(IConstants.REST_DS + "/report/transfer/" + id + "/pdf");
			downloadAsTextLinkItem.setValue(IConstants.REST_DS + "/report/transfer/" + id + "/txt");
			status.setContentStyle("poProcessedLabel");
		}else if("COMPLETED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.hide();
			completeLinkItem.hide();
			editItemLink.hide();
			downloadAsPdfLinkItem.show();
			downloadAsTextLinkItem.show();
			status.setContentStyle("poCompletedLabel");
		}else if("CANCELLED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.hide();
			completeLinkItem.hide();
			editItemLink.hide();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			status.setContentStyle("poCancelledLabel");
		}
	}
	
	private void processLinkClicked(){
				
		if(listGrid.getTotalRows() > 0){
			record.setAttribute("status", "PROCESSED");
			toForm.editRecord(record);
			if(!toForm.validate(false)){
				@SuppressWarnings("unchecked")
				Map<String, String> errors = (Map<String, String>)toForm.getErrors();
				String str = "";
				for(Map.Entry<String, String> e : errors.entrySet()){							
				    str = str + toForm.getItem(e.getKey()).getTitle()+": "+e.getValue() + "<br/>";
				}
				SC.warn(str);
			}else{
				SC.ask("Process Transfer Order?", new BooleanCallback() {			
					public void execute(Boolean value) {
						if(value){
							toForm.editRecord(record);
							toForm.saveData(processCallBack);
						}
					}
				});
			}			
		}else{
			SC.warn("Order Product not found");
		}
		
	}

	protected void completeLinkClicked() {
		record.setAttribute("status", "COMPLETED");
		SC.ask("Complete Transfer Order?", new BooleanCallback() {			
			public void execute(Boolean value) {
				if(value){
					toForm.editRecord(record);
					toForm.saveData(completeCallBack);
				}
			}
		});
	}

	private void cancelLinkClicked(){
		SC.ask("Cancel Transfer Order?", new BooleanCallback() {
			
			public void execute(Boolean value) {
				if(value){
					record.setAttribute("status", "CANCELLED");					
					toForm.editRecord(record);
					toForm.saveData(cancelCallBack);
				}
			}
		});
	}

	protected void editLinkClicked(){
		customizeWindow("Edit Transfer Order", 200, 250);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
		toForm.editRecord(record);
	}

	protected void buttonWindowClicked(){
		if(toForm.validate(false)){
			toForm.saveData();
		}
	}

}
