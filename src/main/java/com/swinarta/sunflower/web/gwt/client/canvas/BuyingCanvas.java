package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.forms.BuyingForm;
import com.swinarta.sunflower.web.gwt.client.model.ClientDisplayProductMeasurement;
import com.swinarta.sunflower.web.gwt.client.util.PriceUtil;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.widgets.layout.HLayout;

public class BuyingCanvas extends DataCanvas{

	private DisplayLabel buyingPrice = new DisplayLabel("Buying Price");
	private DisplayLabel supplier = new DisplayLabel("Supplier");
	private DisplayLabel disc1Pct = new DisplayLabel("Disc-1");
	private DisplayLabel disc2Pct = new DisplayLabel("Disc-2");
	private DisplayLabel disc3Pct = new DisplayLabel("Disc-3");
	private DisplayLabel disc4Pct = new DisplayLabel("Disc-4");
	private DisplayLabel discValue = new DisplayLabel("Disc Price");
	private DisplayLabel tax = new DisplayLabel("Buying Tax");
	private DisplayLabel measurement = new DisplayLabel("Measurement");
	private DisplayLabel costPricePerUnit = new DisplayLabel("Cost Price per Unit");
	
	final private RestDataSource buyingDataSource;
	final private BuyingForm buyingForm;
		
	private Record record;
	private Integer productId;
	private DSCallback saveCallback;
	
	private Map<Integer, Integer> measurementOverrideMap;
	
	@Inject
	public BuyingCanvas(
			BuyingForm buyingForm,
			@Named("Buying") RestDataSource buyingDataSource
			){
		super(DataCanvasLayout.VERTICAL);
		
		this.buyingForm = buyingForm;
		this.buyingDataSource = buyingDataSource;
		
		setGroupTitle("Buying Information");
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setMargin(3);

		HLayout discLayout = new HLayout(10);
		discLayout.addMember(disc1Pct);
		discLayout.addMember(disc2Pct);
		discLayout.addMember(disc3Pct);
		discLayout.addMember(disc4Pct);
		
		setForm(buyingForm);
		
		saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);						
					addOrUpdateWindow.hide();
				}
			}
		};
		
		buyingForm.setSaveCallback(saveCallback);
		
		dataComposite.addMember(supplier);
		dataComposite.addMember(measurement);
		dataComposite.addMember(buyingPrice);
		dataComposite.addMember(discLayout);
		dataComposite.addMember(discValue);
		dataComposite.addMember(tax);
		dataComposite.addMember(costPricePerUnit);
		dataComposite.addMember(editForm);
		
	}

	public void setMeasurementOverrideMap(Map<Integer, Integer> measurementOverrideMap) {
		this.measurementOverrideMap = measurementOverrideMap;
	}

	public void setMeasurementOverrideCMap(Map<Integer, ClientDisplayProductMeasurement> measurementOverrideCMap) {
		buyingForm.setMeasurementOverrideMap(measurementOverrideCMap);
	}
		
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@SuppressWarnings("unchecked")
	private void loadData(Record r) {
		this.record = r;
	
		buyingPrice.setContents(r.getAttributeAsDouble("buyingPrice"));
		supplier.setContents((String)r.getAttributeAsMap("supplier").get("name"));
		disc1Pct.setContentsInPercent(r.getAttributeAsDouble("disc1"));
		disc2Pct.setContentsInPercent(r.getAttributeAsDouble("disc2"));
		disc3Pct.setContentsInPercent(r.getAttributeAsDouble("disc3"));
		disc4Pct.setContentsInPercent(r.getAttributeAsDouble("disc4"));
		discValue.setContents(r.getAttributeAsDouble("discPrice"));
		
		boolean taxIncluded = r.getAttributeAsBoolean("taxIncluded");
		String taxStr;
		
		if(taxIncluded){
			taxStr = "Tax Included (10%)";
		}else{
			taxStr = "Tax Excluded (10%)";
		}
		
		tax.setContents(taxStr);
		
		Map<String, Object> measurementMap = r.getAttributeAsMap("measurement");
		String measurementStr = convertMeasurement(measurementMap, measurementOverrideMap);
		
		measurement.setContents(measurementStr);
		
		Integer measurementDefaultQty = getMeasurementQty(measurementMap, measurementOverrideMap);
		Double costPrice = PriceUtil.getCostPricePerUnit(r, measurementDefaultQty);
		
		costPricePerUnit.setContents(costPrice);

		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("costPrice", costPrice);

		loadDataFound(onCompleteCallback, attrs);		
	}
	
	private void loadDataNotFound(final onCompleteCallback callback){
		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("costPrice", 0d);		
		super.loadDataNotFound(callback, attrs);
	}

	public void fetchBuyingData(){
		Criteria criteria = new Criteria();
		criteria.addCriteria("id", productId);
		
		buyingDataSource.fetchData(criteria, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				
				if(response.getTotalRows() > 0){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
				}else{
					loadDataNotFound(onCompleteCallback);
				}
				
			}
		});
		
	}
	
	protected void editLinkClicked(){
		customizeWindow("Edit Buying", 200, 480);
		buyingForm.clearErrors(true);
		buyingForm.editRecord(record);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
		buyingForm.focusInItem("buyingPrice");
	}
	
	protected void addLinkClicked(){
		customizeWindow("Add Buying", 200, 480);
		buyingForm.clearErrors(true);
		buyingForm.editNewRecord(productId);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
	}

	protected void buttonWindowClicked(){
		if(buyingForm.validate(false)){
			buyingForm.saveData();
		}
		
	}

	private String convertMeasurement(Map<String, Object> measurementMap, Map<Integer, Integer> measurementOverrideMap){
		StringBuffer retval = new StringBuffer();
		String measurementDesc = (String)measurementMap.get("description");		
		Integer measurementDefaultQty = getMeasurementQty(measurementMap, measurementOverrideMap);
		
		retval.append(measurementDesc);
		retval.append(" (");
		retval.append(measurementDefaultQty);
		retval.append(")");
		return retval.toString();
	}
	
	private Integer getMeasurementQty(Map<String, Object> measurementMap, Map<Integer, Integer> measurementOverrideMap){
		Integer measurementDefaultQty = (Integer) measurementMap.get("defaultQty");
		Integer measurementId = (Integer) measurementMap.get("id");
		if(measurementOverrideMap.size() > 0 && measurementOverrideMap.get(measurementId) != null){
			measurementDefaultQty = measurementOverrideMap.get(measurementId);
		}
		
		return measurementDefaultQty;

	}

}
