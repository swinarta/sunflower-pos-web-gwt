package com.swinarta.sunflower.web.gwt.client.widget.tabset;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.History;
import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.canvas.ProductDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

public class ProductTabSet extends TabSet{

	@Inject
	public ProductTabSet(){
		
		setTabBarPosition(Side.TOP);  
		setTabBarAlign(Side.LEFT);
		
		addTabSelectedHandler(new TabSelectedHandler() {
			
			public void onTabSelected(TabSelectedEvent event) {
				Tab selectedTab = event.getTab();				
				History.newItem("product-"+ selectedTab.getID());
			}
		});
		
	}
	
	public void loadProduct(final Record record){
		
		SC.showPrompt("Loading Product ... ");
		
		DeferredCommand.addCommand(new Command() {
			
			public void execute() {
				String tabId = record.getAttribute("id");
				if(getTab(tabId) == null){
					String title = record.getAttribute("longDescription");

					GardenGinjector injector = Ginjector.getInstance();
					ProductDetailCanvas productDetailCanvas = injector.getProductDetailCanvas();
					
					Tab tab = new Tab(title);
					tab.setCanClose(true);
					tab.setPane(productDetailCanvas);
					tab.setID(tabId);
					
					productDetailCanvas.fetchData(record);
					addTab(tab);						
				}else{
					SC.clearPrompt();
				}
				
				selectTab(tabId);
							
			}
		});
		
	}
		
}