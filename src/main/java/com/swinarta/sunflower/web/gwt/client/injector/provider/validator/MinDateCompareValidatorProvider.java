package com.swinarta.sunflower.web.gwt.client.injector.provider.validator;

import com.google.inject.Provider;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator.COMPARATOR;


public class MinDateCompareValidatorProvider implements Provider<DateCompareValidator>{

	public DateCompareValidator get() {
		DateCompareValidator validator = new DateCompareValidator();
		validator.setOtherField("min");
		validator.setComparator(COMPARATOR.GREATER_OR_EQUALS);
		validator.setErrorMessage("Invalid Max Value");
		return validator;
	}

}
