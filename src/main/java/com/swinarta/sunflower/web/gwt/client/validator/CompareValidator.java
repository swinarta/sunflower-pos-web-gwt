package com.swinarta.sunflower.web.gwt.client.validator;

import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class CompareValidator extends CustomValidator{

	private String otherField;
	
	private COMPARATOR comparator;
	
	public void setOtherField(String otherField){
		this.otherField = otherField;
	}
	
	public void setComparator(COMPARATOR comparator){
		this.comparator = comparator;
	}
	
	public CompareValidator(){
		setStopIfFalse(true);		
	}
	
	@Override
	protected boolean condition(Object value) {
	
		if(getRecord() == null) return true;
		
		String otherFieldStr = getRecord().getAttribute(otherField);
		
		if(otherFieldStr == null) return true; 
		
		Integer otherFieldInt = Integer.parseInt(otherFieldStr);
		Integer currInt = Integer.parseInt(value.toString());
		
		if(comparator == COMPARATOR.GREATER_OR_EQUALS){
			return (currInt >= otherFieldInt);
		}
		
		return false;
	}
	
	public enum COMPARATOR{
		GREATER_OR_EQUALS
	}

}
