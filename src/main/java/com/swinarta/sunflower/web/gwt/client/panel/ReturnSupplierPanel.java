package com.swinarta.sunflower.web.gwt.client.panel;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.forms.ReturnSupplierForm;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ReturnToSupplierListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.ReturnToSupplierTabSet;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ReturnSupplierPanel extends BasePanel{

	private static final String DESCRIPTION = "returnsupplier";

    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
        	ReturnSupplierPanel panel = injector.getReturnSupplierPanel();
        	id = DESCRIPTION;
            return panel;
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }

    private DynamicForm addPoForm;
    private DynamicForm searchForm;
    private TextItem searchTextItem;
    private ButtonItem searchButton;
    
    final private Tab searchRestSupTab;
    final private LinkItem addReturnSupplierLinkItem;
    final private Window addReturnSupplierWindow;
    final private CustomComboBoxItem supplierComboBoxItem;
    final private Button saveButton;
    final private DynamicForm returnSupplierForm;
    final private ReturnToSupplierTabSet returnToSupplierTabSet;
    final private ReturnToSupplierListGrid listGrid;
    
    @Inject
    public ReturnSupplierPanel(
    		ReturnToSupplierTabSet returnStoSupplierTabSet,
    		@Named("SupplierAny") CustomComboBoxItem supplierComboBoxItem,
    		ReturnToSupplierListGrid listGrid,
    		@Named("SearchReturnSupplier") Tab searchRetSupTab,
    		@Named("AddReturnSupplier") LinkItem addReturnSupplierLinkItem,
    		@Named("AddReturnSupplier") Window addReturnSupplierWindow,
    		ReturnSupplierForm returnSupplierForm,
    		@Named("SaveInForm") Button saveButton
    		){
    	
    	this.supplierComboBoxItem = supplierComboBoxItem;
    	this.listGrid = listGrid;
    	this.searchRestSupTab = searchRetSupTab;
    	this.returnToSupplierTabSet = returnStoSupplierTabSet;
    	this.addReturnSupplierLinkItem = addReturnSupplierLinkItem;
    	this.addReturnSupplierWindow = addReturnSupplierWindow;
    	this.returnSupplierForm = returnSupplierForm;
    	this.saveButton = saveButton;
    	
    	init();
    	initComponentHandler();
		initWindow();    	
    	
    }
    
	@Override
	public Canvas getViewPanel() {
		
		addPoForm = new DynamicForm();
		addPoForm.setWidth(300);
		addPoForm.setFields(addReturnSupplierLinkItem);
		
		searchTextItem = new TextItem("searchRetSupp");  
		searchTextItem.setTitle("Search Return To Supplier");  
		searchTextItem.setSelectOnFocus(true);
		searchTextItem.setWrapTitle(false);
		searchTextItem.setCharacterCasing(CharacterCasing.UPPER);

		searchForm = new DynamicForm();
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);

		searchButton = new ButtonItem("search", "Search");
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);
		
		searchForm.setFields(supplierComboBoxItem, searchTextItem, searchButton);		
		
		listGrid.setWidth(620);
		listGrid.setHeight(400);
		listGrid.hide();
		
		VLayout c = new VLayout();
		c.addMember(addPoForm);
		c.addMember(searchForm);
		c.addMember(listGrid);
				
		c.setLayoutTopMargin(10);
		c.setLayoutLeftMargin(25);
		
		searchRestSupTab.setPane(c);
		
		returnToSupplierTabSet.addTab(searchRestSupTab);
				
		return returnToSupplierTabSet;
	}
	
	private void initComponentHandler(){
		addReturnSupplierLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				addReturnSupplierWindow.centerInPage();
				addReturnSupplierWindow.show();

				returnSupplierForm.editNewRecord();
			}
		});
		
		saveButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				performValidateAndSave();
			}
		});
		
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchReturn();
				}  
			});
		
		searchTextItem.addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					searchReturn();
				}				
			}
		});
		listGrid.addDoubleClickHandler(new DoubleClickHandler() {

			public void onDoubleClick(DoubleClickEvent event) {
				ReturnToSupplierListGrid listGrid = (ReturnToSupplierListGrid)event.getSource();
				returnToSupplierTabSet.loadReturnToSupplier(listGrid.getSelectedRecord());
			}
		});
		
	}

	private void performValidateAndSave(){
		if(returnSupplierForm.validate()){
			returnSupplierForm.saveData(new DSCallback() {						
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					returnToSupplierTabSet.loadReturnToSupplier(response.getData()[0]);
					addReturnSupplierWindow.hide();
				}
			});
		}
	}
	
	private void initWindow(){
		addReturnSupplierWindow.setTitle("Add Return To Supplier");
		addReturnSupplierWindow.setWidth(250);
		addReturnSupplierWindow.setHeight(340);
		
		addReturnSupplierWindow.addItem(returnSupplierForm);
		addReturnSupplierWindow.addItem(saveButton);
	}

	private void searchReturn(){
		String text = searchForm.getValueAsString("searchRetSupp");
		Integer supplierId = FormUtil.getIntegerValueFromFormItem(supplierComboBoxItem);
		
		if((text != null && text.length() > 3) || 
				(supplierId != null && supplierId != -1)
				){
			listGrid.show();
			listGrid.fetchData(searchForm.getValuesAsCriteria());
		}
		searchForm.focus();	
	}
	
}