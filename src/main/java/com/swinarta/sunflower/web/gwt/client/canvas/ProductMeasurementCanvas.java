package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.forms.ProductMeasurementForm;
import com.swinarta.sunflower.web.gwt.client.model.ClientDisplayProductMeasurement;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductMeasurementsGridList;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.widgets.grid.ListGridField;

public class ProductMeasurementCanvas extends DataCanvas{

	final private ProductMeasurementsGridList listGrid;
	final private RestDataSource productMeasurementDataSource;
	final private ProductMeasurementForm productMeasurementForm;
	
	private Map<Integer, Integer> measurementOverrideMap = new TreeMap<Integer, Integer>();
	private Map<Integer, ClientDisplayProductMeasurement> measurementOverrideCMap = new HashMap<Integer, ClientDisplayProductMeasurement>();;

	private Integer productId;
	private DSCallback saveCallback;
	
	@Inject
	public ProductMeasurementCanvas(
			@Named("ProductMeasurements") RestDataSource productMeasurementDataSource,
			ProductMeasurementsGridList listGrid,
			ProductMeasurementForm productMeasurementForm
			){
		
		super(DataCanvasLayout.VERTICAL);
		
		this.productMeasurementDataSource = productMeasurementDataSource;
		this.listGrid = listGrid;
		this.productMeasurementForm = productMeasurementForm;
		
		setGroupTitle("Product Measurement Information");
		setIsGroup(true);
		setCanSelectText(true);
		setMargin(3);
		
		setForm(productMeasurementForm);

		saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					loadData(response.getData());
					addOrUpdateWindow.hide();
				}
			}
		};
		
		productMeasurementForm.setSaveCallback(saveCallback);
		
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutBottomMargin(8);
		setLayoutRightMargin(8);
		setShowAddLink(false);
				
		ListGridField descriptionField = new ListGridField("description", "Measurement");
		ListGridField qtyField = new ListGridField("qty", "Qty");
		ListGridField idDefaultField = new ListGridField("isDefault");				
		idDefaultField.setHidden(true);
		
		listGrid.setFields(descriptionField, qtyField, idDefaultField);
		
		dataComposite.addMember(listGrid);
		dataComposite.addMember(editForm);
	}
	
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public void fetchProductMeasurementData(){
		Criteria criteria = new Criteria("productId", String.valueOf(productId));
		productMeasurementDataSource.fetchData(criteria, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				loadData(response.getData());
			}
		});
	}

	private void loadData(Record[] records){
		listGrid.setData(records);
		
		for (Record record : records) {
			
			Integer measurementId = record.getAttributeAsInt("id");
			
			if(!record.getAttributeAsBoolean("isDefault")){
				Integer overideQty = record.getAttributeAsInt("qty");
				measurementOverrideMap.put(measurementId, overideQty);
			}
								
			ClientDisplayProductMeasurement m = new ClientDisplayProductMeasurement();
			m.setDescription(record.getAttributeAsString("description"));
			m.setQty(record.getAttributeAsInt("qty"));
			m.setIsDefault(record.getAttributeAsBoolean("isDefault"));
			m.setIsMutable(record.getAttributeAsBoolean("isMutable"));
			measurementOverrideCMap.put(measurementId, m);
			
		}				
		
		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("measurementOverrideMap", measurementOverrideMap);
		attrs.put("measurementOverrideCMap", measurementOverrideCMap);
		
		productMeasurementForm.setMeasurementOverrideCMap(measurementOverrideCMap);
		loadDataFound(onCompleteCallback, attrs);
		
	}
	
	protected void editLinkClicked(){
		customizeWindow("Edit Product Measurement", 200, 200);
		productMeasurementForm.clearErrors(true);
		productMeasurementForm.editRecord(productId);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
	}
	
	protected void buttonWindowClicked(){
		if(productMeasurementForm.validate(false)){
			productMeasurementForm.saveData();
		}
	}		

}