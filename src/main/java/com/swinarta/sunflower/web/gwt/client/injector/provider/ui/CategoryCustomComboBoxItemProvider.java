package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.smartgwt.client.data.RestDataSource;

public class CategoryCustomComboBoxItemProvider implements Provider<CustomComboBoxItem>{

	final private RestDataSource datasource;
	final private CustomComboBoxItem comboBox;
	
	@Inject
	public CategoryCustomComboBoxItemProvider(
			@Named("Categories") RestDataSource datasource,
			CustomComboBoxItem comboBox){
		
		this.datasource = datasource;
		this.comboBox = comboBox;
	}

	public CustomComboBoxItem get() {
		comboBox.setName("categoryId");
		comboBox.setTitle("Category");
		comboBox.setDisplayField("description");
		comboBox.setValueField("id");
		comboBox.setSortField("description");
		comboBox.setShowAllOptions(true);
		comboBox.setAnimatePickList(true);
		comboBox.setShowAnyRecordOption(false);
		comboBox.setDataSource(datasource);

		comboBox.fetchData();
		comboBox.setSelectOnFocus(true);

		return comboBox;
	}

}
