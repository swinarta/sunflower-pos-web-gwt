package com.swinarta.sunflower.web.gwt.client.injector.provider.criteria;

import com.google.inject.Provider;
import com.smartgwt.client.data.Criteria;

public class GetNextSkuCriteriaProvider implements Provider<Criteria>{

	public Criteria get() {
		Criteria criteria = new Criteria();
		criteria.addCriteria("actionId", "getNextSku");
		return criteria;
	}

}
