package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.util.JSOHelper;

public class ReceivingOrdersRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	
	@Inject
	public ReceivingOrdersRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.UPDATE){
					int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
					dsRequest.setActionURL(getUpdateDataURL()+id);					
				}
				return super.transformRequest(dsRequest);
			}
		};
		ds.setFetchDataURL("/receivingorders/");
		ds.setAddDataURL("/receivingorders/");
		ds.setUpdateDataURL("/receivingorder/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, addOperationBinding, updateOperationBinding);

		DataSourceTextField roIdTextField = new DataSourceTextField("roId", "RO NUMBER");
		roIdTextField.setHidden(true);
		
		DataSourceDateField roDateTextField = new DataSourceDateField("roDate", "Receive Date");
		roDateTextField.setRequired(true);
		roDateTextField.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		DataSourceTextField statusTextField = new DataSourceTextField("status", "STATUS");
		statusTextField.setHidden(true);

		DataSourceTextField remarksTextField = new DataSourceTextField("remarks", "Remarks");

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);
						
		ds.setFields(idTextField, roIdTextField, roDateTextField, remarksTextField, statusTextField);

		return ds;
	}

}