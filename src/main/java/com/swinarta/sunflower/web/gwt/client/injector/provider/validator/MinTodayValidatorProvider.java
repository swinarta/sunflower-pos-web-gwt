package com.swinarta.sunflower.web.gwt.client.injector.provider.validator;

import java.util.Date;

import com.google.inject.Provider;
import com.swinarta.sunflower.web.gwt.client.util.DateUtils;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;

public class MinTodayValidatorProvider implements Provider<DateRangeValidator>{

	public DateRangeValidator get() {
		DateRangeValidator validator = new DateRangeValidator();
						
		Date today = new Date();
		DateUtils.resetTime(today);
		
		validator.setMin(today);
		validator.setErrorMessage("Invalid Date");
		return validator;
	}

}
