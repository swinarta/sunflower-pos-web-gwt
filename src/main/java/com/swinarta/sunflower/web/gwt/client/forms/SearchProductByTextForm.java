package com.swinarta.sunflower.web.gwt.client.forms;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class SearchProductByTextForm extends DynamicForm{

	private TextItem searchProductTextItem = new TextItem("searchProduct", "Search Product");
	private HiddenItem actionItem = new HiddenItem("actionId");
	
	public SearchProductByTextForm(){
		
		searchProductTextItem.setSelectOnFocus(true);
		searchProductTextItem.setWrapTitle(false);
		searchProductTextItem.setCharacterCasing(CharacterCasing.UPPER);
		searchProductTextItem.setAlign(Alignment.LEFT);
		
		actionItem.setValue("searchProduct");

		setFields(searchProductTextItem, actionItem);
		
	}	
}
