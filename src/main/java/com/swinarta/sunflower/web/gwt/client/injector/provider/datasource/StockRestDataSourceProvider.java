package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.validator.CompareValidator;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;

public class StockRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	final private OperationBinding fetchOperationBinding;
	final private IntegerRangeValidator positiveIntegerRangeValidatorProvider;
	final private CompareValidator minCompareValidator;

	@Inject
	public StockRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding,
			@Named("Fetch") OperationBinding fetchOperationBinding,
			@Named("Positive") IntegerRangeValidator positiveIntegerRangeValidatorProvider,
			@Named("Min") CompareValidator minCompareValidator
			){
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
		this.fetchOperationBinding = fetchOperationBinding;
		this.positiveIntegerRangeValidatorProvider = positiveIntegerRangeValidatorProvider;
		this.minCompareValidator = minCompareValidator;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/ds/stock/");
		ds.setUpdateDataURL("/ds/stock/");
		ds.setAddDataURL("/ds/stock/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, updateOperationBinding, addOperationBinding);

		DataSourceFloatField currentFloatField = new DataSourceFloatField("current", "Current");
		currentFloatField.setCanEdit(true);
		currentFloatField.setCanSave(true);
		currentFloatField.setRequired(true);

		DataSourceIntegerField maxIntegerField = new DataSourceIntegerField("max", "Max");
		maxIntegerField.setCanEdit(true);
		maxIntegerField.setCanSave(true);
		maxIntegerField.setRequired(true);

		DataSourceIntegerField minIntegerField = new DataSourceIntegerField("min", "Min");
		minIntegerField.setCanEdit(true);
		minIntegerField.setCanSave(true);
		minIntegerField.setRequired(true);
		
		DataSourceIntegerField stdOrderIntegerField = new DataSourceIntegerField("defaultOrder", "Std Order");
		stdOrderIntegerField.setCanEdit(true);
		stdOrderIntegerField.setCanSave(true);
		stdOrderIntegerField.setRequired(true);

		DataSourceIntegerField productIdTextField = new DataSourceIntegerField("productId");
		productIdTextField.setHidden(true);

		DataSourceIntegerField storeIdTextField = new DataSourceIntegerField("storeId");
		storeIdTextField.setHidden(true);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);


		maxIntegerField.setValidators(positiveIntegerRangeValidatorProvider);
		minIntegerField.setValidators(positiveIntegerRangeValidatorProvider);
		stdOrderIntegerField.setValidators(positiveIntegerRangeValidatorProvider);
		maxIntegerField.setValidators(minCompareValidator);
		
		ds.setFields(currentFloatField, maxIntegerField, minIntegerField, stdOrderIntegerField, productIdTextField, storeIdTextField);

		return ds;
	}

}