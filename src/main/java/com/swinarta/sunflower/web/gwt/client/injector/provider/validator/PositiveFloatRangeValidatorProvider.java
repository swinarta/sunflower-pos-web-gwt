package com.swinarta.sunflower.web.gwt.client.injector.provider.validator;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;

public class PositiveFloatRangeValidatorProvider implements Provider<FloatRangeValidator>{

	public FloatRangeValidator get() {
		FloatRangeValidator validator = new FloatRangeValidator();
		validator.setMin(0);
		validator.setErrorMessage("Invalid Value");
		return validator;
	}

}
