package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.ProductStatusLayoutProvider;
import com.swinarta.sunflower.web.gwt.client.model.ClientDisplayProductMeasurement;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas.onCompleteCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ProductDetailCanvas extends VLayout{
		
	private onCompleteCallback productOnCompleteCallback;
	private onCompleteCallback buyingOnCompleteCallback;
	private onCompleteCallback sellingOnCompleteCallback;
	private onCompleteCallback productMeasurementOnCompleteCallback;

	private Boolean productLoaded = false;
	private Boolean buyingLoaded = false;
	private Boolean sellingLoaded = false;
	private Boolean measurementLoaded = false;

	private Store currentStore = (Store) ClientSession.objects.get("store");
	
	final private ProductBasicCanvas productBasicCanvas;
	final private BuyingCanvas buyingCanvas;
	final private SellingCanvas sellingCanvas;
	final private StockCanvas stockCanvas;
	final private ProductMeasurementCanvas productMeasurementCanvas;
	final private RelatedProductCanvas relatedProductCanvas;
	final private ProductImageCanvas productImageCanvas;
	final private Layout statusLayout;

	@Inject
	public ProductDetailCanvas(
			ProductBasicCanvas productBasicCanvas,
			ProductImageCanvas productImageCanvas,
			final BuyingCanvas buyingCanvas,
			final SellingCanvas sellingCanvas,
			final StockCanvas stockCanvas,
			final ProductMeasurementCanvas productMeasurementCanvas,
			final RelatedProductCanvas relatedProductCanvas,
			final PrintActionCanvas printActionCanvas,
			@Named("ProductStatus") Layout statusLayout
			){
		
		this.productBasicCanvas = productBasicCanvas;
		this.buyingCanvas = buyingCanvas;
		this.sellingCanvas = sellingCanvas;
		this.stockCanvas = stockCanvas;
		this.productMeasurementCanvas = productMeasurementCanvas;
		this.relatedProductCanvas = relatedProductCanvas;
		this.productImageCanvas = productImageCanvas;
		this.statusLayout = statusLayout;
		
		setCanSelectText(true);
		setAnimateMembers(true);
										
		productImageCanvas.setWidth(180);
		
		HLayout basicLayout = new HLayout();
		basicLayout.setHeight(200);
		basicLayout.addMember(productBasicCanvas);
		basicLayout.addMember(productImageCanvas);

		HLayout priceLayout = new HLayout();
		priceLayout.setHeight(300);
		priceLayout.addMember(buyingCanvas);
		priceLayout.addMember(sellingCanvas);

		HLayout stockMeasurementLayout = new HLayout();
		stockMeasurementLayout.setHeight(170);
		stockMeasurementLayout.addMember(stockCanvas);
		stockMeasurementLayout.addMember(printActionCanvas);		
		stockMeasurementLayout.addMember(productMeasurementCanvas);
		
		addMember(statusLayout);
		addMember(basicLayout);
		addMember(priceLayout);
		addMember(stockMeasurementLayout);
		addMember(relatedProductCanvas);
				
		productBasicCanvas.setShowAddLink(currentStore.getIsHq());
		productBasicCanvas.setShowEditLink(currentStore.getIsHq());
		
		productOnCompleteCallback = new onCompleteCallback() {
			
			public void loadComplete(Map<String, Object> attributes) {
				
				Integer categoryId = (Integer)attributes.get("categoryId");
				String description = (String)attributes.get("description");
				Boolean isActive = (Boolean)attributes.get("isActive");
				String barcode = (String)attributes.get("barcode");
				String shortDescription = (String)attributes.get("shortDescription");
				
				stockCanvas.setShowAddLink(isActive);
				stockCanvas.setShowEditLink(isActive);
				
				buyingCanvas.setShowAddLink(isActive && currentStore.getIsHq());
				buyingCanvas.setShowEditLink(isActive && currentStore.getIsHq());

				sellingCanvas.setShowAddLink(isActive && currentStore.getIsHq());
				sellingCanvas.setShowEditLink(isActive && currentStore.getIsHq());

				productMeasurementCanvas.setShowEditLink(isActive && currentStore.getIsHq());

				relatedProductCanvas.setCategoryId(categoryId);
				relatedProductCanvas.setDescription(description);
				
				printActionCanvas.setProductDescription(shortDescription);
				printActionCanvas.setBarcode(barcode);
				
				displayStatusLabel(isActive);
				
				productLoaded = true;
				checkLoading();
			}
		};
		
		buyingOnCompleteCallback = new onCompleteCallback() {
			
			public void loadComplete(Map<String, Object> attributes) {
				final Double costPrice = (Double)attributes.get("costPrice");
				sellingCanvas.setCostPrice(costPrice);
				sellingCanvas.fetchSellingData(sellingOnCompleteCallback);
				buyingLoaded = true;
				checkLoading();
			}
		};
		
		sellingOnCompleteCallback = new onCompleteCallback() {
			
			public void loadComplete(Map<String, Object> attributes) {
				Double sellingPricePerUnit = (Double)attributes.get("sellingPricePerUnit");
				Double sellingPrice = (Double)attributes.get("sellingPrice");
				sellingCanvas.setSellingPrice(sellingPricePerUnit);
				printActionCanvas.setPrice(sellingPrice);
				sellingLoaded = true;
				checkLoading();
			}
		};
		
		productMeasurementOnCompleteCallback = new onCompleteCallback() {
			
			@SuppressWarnings("unchecked")
			public void loadComplete(Map<String, Object> attributes) {

				Map<Integer, Integer> measurementOverrideMap = (Map<Integer, Integer>) attributes.get("measurementOverrideMap");
				Map<Integer, ClientDisplayProductMeasurement> measurementOverrideCMap = (Map<Integer, ClientDisplayProductMeasurement>) attributes.get("measurementOverrideCMap");

				
				buyingCanvas.setMeasurementOverrideCMap(measurementOverrideCMap);
				buyingCanvas.setMeasurementOverrideMap(measurementOverrideMap);
				
				sellingCanvas.setMeasurementOverrideCMap(measurementOverrideCMap);
				sellingCanvas.setMeasurementOverrideMap(measurementOverrideMap);
				
				buyingCanvas.fetchBuyingData();
				
				measurementLoaded = true;
				checkLoading();
			}
		};
		
		productBasicCanvas.setOnCompleteCallback(productOnCompleteCallback);
		buyingCanvas.setOnCompleteCallback(buyingOnCompleteCallback);
		sellingCanvas.setOnCompleteCallback(sellingOnCompleteCallback);
		productMeasurementCanvas.setOnCompleteCallback(productMeasurementOnCompleteCallback);
	}
	
	public void fetchData(Record productRecord){
		
		SC.showPrompt("Loading Product ... ");
		
		Integer productId = productRecord.getAttributeAsInt("id");
		Integer storeId = currentStore.getId();
		
		relatedProductCanvas.setProductId(productId);		
		buyingCanvas.setProductId(productId);
		sellingCanvas.setProductId(productId);
		productMeasurementCanvas.setProductId(productId);
		productImageCanvas.setProductId(productId);
		
		productBasicCanvas.loadProductBasicData(productRecord, productOnCompleteCallback);
		productMeasurementCanvas.fetchProductMeasurementData();
		stockCanvas.fetchStockData(productId, storeId);
		productImageCanvas.fetchImage();
		
		Boolean isActive = !productRecord.getAttributeAsBoolean("deleteInd");
		displayStatusLabel(isActive);
	}
	
	private void displayStatusLabel(boolean isActive){
		
		boolean show = false;
		
		if(isActive){
			statusLayout.getMember(ProductStatusLayoutProvider.INDEX_NOT_ACTIVE).hide();
		}else{
			statusLayout.getMember(ProductStatusLayoutProvider.INDEX_NOT_ACTIVE).show();
			show = true;
		}
		
		if(show){
			showMember(statusLayout);
		}else{
			hideMember(statusLayout);
		}
	}
	
	private void checkLoading(){
		
		if(productLoaded){
			SC.showPrompt("Loading Product ... Product Basic Info Loaded");
		}

		if(buyingLoaded){
			SC.showPrompt("Loading Product ... Product Buying Info Loaded");
		}

		if(sellingLoaded){
			SC.showPrompt("Loading Product ... Product Selling Info Loaded");
		}

		if(measurementLoaded){
			SC.showPrompt("Loading Product ... Product Measurement Info Loaded");
		}

		if(productLoaded && buyingLoaded && sellingLoaded && measurementLoaded){
			SC.clearPrompt();
		}
		
	}
		
}