package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Provider;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DSProtocol;

public class CategoryRestDataSourceProvider implements Provider<RestDataSource>{
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/ds/category/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		OperationBinding fetch = new OperationBinding();
		fetch.setOperationType(DSOperationType.FETCH);
		fetch.setDataProtocol(DSProtocol.POSTMESSAGE);
		ds.setOperationBindings(fetch);
		
		DataSourceIntegerField idIntegerField = new DataSourceIntegerField("id");
		idIntegerField.setPrimaryKey(true);
		
		ds.setFields(idIntegerField);
		
		return ds;
	}

}
