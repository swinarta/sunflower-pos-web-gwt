package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import java.util.LinkedHashMap;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.SelectItem;

public class PromoTypeSelectItemProvider implements Provider<SelectItem>{
	
	public SelectItem get() {
		
		SelectItem selectItem = new SelectItem();
		
		selectItem.setName("promoTypeStr");
		selectItem.setTitle("Promo Type");
		selectItem.setShowAllOptions(true);
		selectItem.setAnimatePickList(true);
		selectItem.setAutoFetchData(false);
		
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("PCT", "PCT");
		valueMap.put("VALUE", "VALUE");
		selectItem.setValueMap(valueMap);
		
		selectItem.setDefaultValue("VALUE");
		
		return selectItem;
	}

}
