package com.swinarta.sunflower.web.gwt.client.util;

public class PrinterUtil {
	
	public static String PRINT_KEYWORD = "print";
	public static String SEPARATOR = " ";
	public static String PROD_SEPARATOR = ";";

	public static String constructPrinterActionStr(Integer printMode, String barcode, Integer qty, String description, Double price){
		StringBuffer action = new StringBuffer();
		action.append(PRINT_KEYWORD);
		action.append(SEPARATOR);
		action.append(printMode);
		action.append(SEPARATOR);
		action.append(barcode);
		action.append(SEPARATOR);
		action.append(qty);
		action.append(SEPARATOR);
		action.append(price.intValue());
		action.append(SEPARATOR);
		action.append(description);
		return action.toString();
	}

	public static String constructPrinterActionStrIndividual(Integer printMode, String barcode, Integer qty, String description, Double price){
		StringBuffer action = new StringBuffer();
		action.append(printMode);
		action.append(SEPARATOR);
		action.append(barcode);
		action.append(SEPARATOR);
		action.append(qty);
		action.append(SEPARATOR);
		action.append(price.intValue());
		action.append(SEPARATOR);
		action.append(description);
		return action.toString();
	}

}
