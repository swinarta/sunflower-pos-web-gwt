package com.swinarta.sunflower.web.gwt.client.connectivity.impl;

import com.calclab.emite.core.client.bosh.BoshSettings;
import com.calclab.emite.core.client.bosh.Connection;
import com.calclab.emite.core.client.xmpp.session.Session;
import com.calclab.emite.core.client.xmpp.stanzas.XmppURI;
import com.calclab.suco.client.Suco;
import com.calclab.suco.client.events.Listener;
import com.google.gwt.core.client.GWT;
import com.swinarta.sunflower.web.gwt.client.connectivity.XmppClient;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.smartgwt.client.data.Record;

public class XmppClientImpl implements XmppClient{
	
	public void init(){
		  final Connection connection = Suco.get(Connection.class);
		  final Session session = Suco.get(Session.class);
		  
		  connection.setSettings(new BoshSettings("proxy", "localhost"));
		  
		  session.onStateChanged(new Listener<Session>() {
			public void onEvent(Session session) {
	            GWT.log("Session: " + session);
	            GWT.log("Session state: " + session.getState());
			}
	      });
		  
		  Record configRecord = (Record) ClientSession.objects.get("config");
		  String username = configRecord.getAttribute("xmpp.web.username");
		  String host = configRecord.getAttribute("xmpp.web.host");
		  String password = configRecord.getAttribute("xmpp.web.password");
		  	  
		  session.login(XmppURI.uri(username, host, "sunflower-" + System.currentTimeMillis()), password);
	}

}
