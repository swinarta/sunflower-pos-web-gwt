package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.callback.OnStoreLoadedCompleteEvent;
import com.swinarta.sunflower.web.gwt.client.forms.TransferOrderForm;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransferOrderListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.TransferOrderTabSet;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class TransferPanel extends BasePanel implements OnStoreLoadedCompleteEvent{

	private static final String DESCRIPTION = "transfer";
	
    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
            id = DESCRIPTION;
            return injector.getTransferPanel();
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }

	final private DynamicForm addToForm = new DynamicForm();
	final private DynamicForm searchForm = new DynamicForm();
	final private ButtonItem searchButton = new ButtonItem("searchProduct", "Search");
	
	final TransferOrderForm transferOrderForm;
	final private LinkItem addTransferOrderLinkItem;
	final private Window addTransferOrderWindow;
	final private Button saveButton;
	final private TransferOrderTabSet tabSet;
	final private Tab searchToTab;
	final private TransferOrderListGrid listGrid;
	final private SelectItem statusSelectItem;

	@Inject
	public TransferPanel(
		@Named("TransferOrderStatus") SelectItem statusSelectItem,
		TransferOrderTabSet tabSet,
		@Named("SearchTransferOrder") Tab searchToTab,
		TransferOrderForm transferOrderForm,
		@Named("AddTransferOrder") LinkItem addTransferOrderLinkItem,
		@Named("AddTransferOrder") Window addTransferOrderWindow,
		@Named("SaveInForm") Button saveButton,
		TransferOrderListGrid listGrid
		){
		
		this.transferOrderForm = transferOrderForm;
		this.addTransferOrderLinkItem = addTransferOrderLinkItem;
		this.addTransferOrderWindow = addTransferOrderWindow;
		this.saveButton = saveButton;
		this.tabSet = tabSet;
		this.searchToTab = searchToTab;
		this.listGrid = listGrid;
		this.statusSelectItem = statusSelectItem;
		
		init();
		initComponentHandler();
		initWindow();
	}

	private void initComponentHandler(){
		
		addTransferOrderLinkItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addTransferOrderWindow.centerInPage();
				addTransferOrderWindow.show();
				
				transferOrderForm.editNewRecord();
			}
		});

		saveButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				performValidateAndSave();
			}
		});
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchTransferOrder();
			}  
		});
		listGrid.addDoubleClickHandler(new DoubleClickHandler() {

			public void onDoubleClick(DoubleClickEvent event) {
				TransferOrderListGrid listGrid = (TransferOrderListGrid)event.getSource();
				tabSet.loadTO(listGrid.getSelectedRecord());
			}
		});

	}	
	
	protected void searchTransferOrder() {
		listGrid.show();
		listGrid.fetchData(searchForm.getValuesAsCriteria());
		searchForm.focus();	
	}

	private void initWindow(){
		addTransferOrderWindow.setTitle("Add Transfer Order");
		addTransferOrderWindow.setWidth(250);
		addTransferOrderWindow.setHeight(340);
		
		addTransferOrderWindow.addItem(transferOrderForm);
		addTransferOrderWindow.addItem(saveButton);
	}

	private void performValidateAndSave(){
		if(transferOrderForm.validate()){
			transferOrderForm.saveData(new DSCallback() {						
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					tabSet.loadTO(response.getData()[0]);
					addTransferOrderWindow.hide();
				}
			});
		}
	}

	@Override
	public Canvas getViewPanel() {
		
		addToForm.setWidth(300);
		addToForm.setFields(addTransferOrderLinkItem);

		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);
		
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);
		
		statusSelectItem.setTitle("Status");
		
		searchForm.setFields(statusSelectItem, searchButton);		

		listGrid.setWidth(450);
		listGrid.setHeight(600);
		listGrid.hide();
				
		VLayout c = new VLayout();
		c.addMember(addToForm);
		c.addMember(searchForm);
		c.addMember(listGrid);

		c.setLayoutTopMargin(10);
		c.setLayoutLeftMargin(25);

		searchToTab.setPane(c);
		tabSet.addTab(searchToTab);
		
		return tabSet;
	}

	public void performOnStoreLoaded(Store store) {
		if(store != null && store.getIsHq()){
			addTransferOrderLinkItem.show();
		}else{
			addTransferOrderLinkItem.hide();
		}		
	}
	
}
