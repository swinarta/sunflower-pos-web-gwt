package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.forms.ProductMainForm;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.widgets.layout.HLayout;

public class ProductBasicCanvas extends DataCanvas{
		
	private DisplayLabel barcode = new DisplayLabel("Barcode");
	private DisplayLabel sku = new DisplayLabel("SKU");
	private DisplayLabel shortDescription = new DisplayLabel("Short Description", "descriptionLabel");
	private DisplayLabel longDescription = new DisplayLabel("Long Description", "descriptionLabel");
	private DisplayLabel consignment = new DisplayLabel("Consignment");
	private DisplayLabel scallable = new DisplayLabel("Scallable");
	private DisplayLabel category = new DisplayLabel("Category");
	
	private Record record;
	private DSCallback saveCallback;
	
	final private ProductMainForm productForm;
	
	@Inject
	public ProductBasicCanvas(
			ProductMainForm productForm
			){

		super(DataCanvasLayout.VERTICAL);
		this.productForm = productForm;
		
		setGroupTitle("Product Information");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);		
		setMargin(3);

		HLayout barcodeSkuLayout = new HLayout(15);
		barcodeSkuLayout.addMember(barcode);
		barcodeSkuLayout.addMember(sku);

		HLayout consignmentScallableLayout = new HLayout(15);
		consignmentScallableLayout.addMember(consignment);
		consignmentScallableLayout.addMember(scallable);
		
		HLayout descLayout = new HLayout(15);
		descLayout.addMember(longDescription);
		descLayout.addMember(shortDescription);
				
		setForm(productForm);
		
		saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadProductBasicData(r, onCompleteCallback);
					addOrUpdateWindow.hide();
				}
			}
		};
		
		productForm.setSaveCallback(saveCallback);
		
		dataComposite.addMember(category);
		dataComposite.addMember(barcodeSkuLayout);
		dataComposite.addMember(descLayout);
		dataComposite.addMember(consignmentScallableLayout);
		dataComposite.addMember(editForm);
	}

	public void loadProductBasicData(Record r, onCompleteCallback callback) {
		
		this.record = r;
		Boolean isActive = !r.getAttributeAsBoolean("deleteInd");
		
		String descriptionStr = r.getAttributeAsString("longDescription");
		String shortDescriptionStr = r.getAttributeAsString("shortDescription");
		String barcodeStr = r.getAttributeAsString("barcode");
		
		barcode.setContents(barcodeStr);
		sku.setContents(r.getAttributeAsString("sku"));
		shortDescription.setContents(shortDescriptionStr);
		longDescription.setContents(descriptionStr);
		consignment.setContents(r.getAttributeAsBoolean("consignment"));
		scallable.setContents(r.getAttributeAsBoolean("scallable"));
		
		Record categoryRecord = r.getAttributeAsRecord("category");
		Integer categoryId = categoryRecord.getAttributeAsInt("id");
		
		String categoryStr = constructCategory(categoryRecord);
		category.setContents(categoryStr);
		
		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("categoryId", categoryId);
		attrs.put("barcode", barcodeStr);
		attrs.put("description", descriptionStr);
		attrs.put("isActive", isActive);
		attrs.put("shortDescription", shortDescriptionStr);		
		
		if(callback != null){
			callback.loadComplete(attrs);
		}
	}

	private String constructCategory(Record categoryRecord){
		String desc = categoryRecord.getAttribute("description");
		String main = null;
				
		if(categoryRecord.getAttributeAsJavaScriptObject("mainCategory") != null){
			Record mainCategoryRecord = categoryRecord.getAttributeAsRecord("mainCategory");
			main = mainCategoryRecord.getAttribute("description");
		}		
		
		StringBuffer retval = new StringBuffer();
		if(main != null){
			retval.append(main);
			retval.append(" - ");
		}
		retval.append(desc);
		return retval.toString();
	}

	protected void editLinkClicked(){
		customizeWindow("Edit Product", 270, 385);
		productForm.editRecord(record);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
	}
	
	protected void buttonWindowClicked(){
		if(productForm.validate(false)){
			productForm.saveData();
		}
	}
	
}