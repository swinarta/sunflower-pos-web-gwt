package com.swinarta.sunflower.web.gwt.client.canvas;

import com.smartgwt.client.widgets.Canvas;

public class MainCanvas extends Canvas{

	public MainCanvas(){
      setBackgroundImage("[SKIN]/shared/background.gif");
      setWidth100();
      setHeight100();
	}
	
}
