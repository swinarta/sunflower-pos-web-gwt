package com.swinarta.sunflower.web.gwt.client.widget.tabset;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.History;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

public class TransactionTabSet extends TabSet{

	public TransactionTabSet(){
		setTabBarPosition(Side.TOP);  
		setTabBarAlign(Side.LEFT);
		
		addTabSelectedHandler(new TabSelectedHandler() {
			
			public void onTabSelected(TabSelectedEvent event) {
				Tab selectedTab = event.getTab();				
				History.newItem("transaction-"+ selectedTab.getID());
			}
		});
		
	}

/*	public void loadPromo(final Record record){
		
		SC.showPrompt("Loading Promo ... ");
		
		DeferredCommand.addCommand(new Command() {
			
			public void execute() {
				String tabId = record.getAttribute("id");
				if(getTab(tabId) == null){
					String title = "Promo #" + record.getAttribute("id");

					GardenGinjector injector = Ginjector.getInstance();
					PromoDetailCanvas promoDetailCanvas = injector.getPromoDetailCanvas();
					
					Tab tab = new Tab(title);
					tab.setCanClose(true);
					tab.setPane(promoDetailCanvas);
					tab.setID(tabId);
					
					promoDetailCanvas.loadBasicData(record);
					
					addTab(tab);						
				}
				
				selectTab(tabId);
				
				SC.clearPrompt();
			
			}
		});
	}
*/}
