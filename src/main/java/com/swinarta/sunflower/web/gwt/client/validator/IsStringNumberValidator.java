package com.swinarta.sunflower.web.gwt.client.validator;

import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class IsStringNumberValidator extends CustomValidator{
	
	public IsStringNumberValidator(){
		setStopIfFalse(true);
	}
	
	@Override
	protected boolean condition(Object value) {

		boolean valid = false;
		
		try{
			Long.parseLong(String.valueOf(value));
			valid = true;
		}catch (Exception e) {
		}
				
		return valid;
	}
	
}
