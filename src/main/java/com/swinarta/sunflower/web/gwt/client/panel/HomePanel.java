package com.swinarta.sunflower.web.gwt.client.panel;

import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.smartgwt.client.widgets.Canvas;

public class HomePanel extends BasePanel{

	private static final String DESCRIPTION = "home";

    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
        	HomePanel panel = injector.getHomePanel();
            //id = panel.getID();
        	id = DESCRIPTION;
            return panel;
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }
           	    
    public HomePanel(
    		){    	
    	init();
    }
        
	@Override
	public Canvas getViewPanel() {
		
		return new Canvas();
	}
	
}