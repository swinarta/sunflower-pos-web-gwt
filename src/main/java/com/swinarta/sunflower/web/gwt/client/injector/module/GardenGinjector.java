package com.swinarta.sunflower.web.gwt.client.injector.module;

import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.Init;
import com.swinarta.sunflower.web.gwt.client.canvas.MainCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ProductDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PromoDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PurchasingOrderDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReceivingOrderDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReturnToSupplierDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.SupplierDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.TransferOrderDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.panel.HomePanel;
import com.swinarta.sunflower.web.gwt.client.panel.ProductMainPanel;
import com.swinarta.sunflower.web.gwt.client.panel.PromoPanel;
import com.swinarta.sunflower.web.gwt.client.panel.PurchasingOrderPanel;
import com.swinarta.sunflower.web.gwt.client.panel.ReceivingOrderPanel;
import com.swinarta.sunflower.web.gwt.client.panel.ReturnSupplierPanel;
import com.swinarta.sunflower.web.gwt.client.panel.StockSummaryPanel;
import com.swinarta.sunflower.web.gwt.client.panel.SupplierPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransactionPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransactionSummaryPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransferPanel;
import com.swinarta.sunflower.web.gwt.client.widget.SideNavTree;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.LinkItem;

@GinModules({GardenModule.class, ConnectivityModule.class, DataSourceModule.class, CriteriaModule.class, ValidatorModule.class})
public interface GardenGinjector extends Ginjector{

	Init getInit();
	
	SideNavTree getSideNavTree();
	MainCanvas getMainCanvas();
	
	@Named("MainTitle") Label getMainTitleLabel();
	@Named("UserName") Label getUserNameLabel();
	@Named("Logout") LinkItem getLogoutLinkItem();
	
	SupplierPanel getSupplierPanel();
	PurchasingOrderPanel getPurchasingOrderPanel();
	PromoPanel getPromoPanel();
	ReceivingOrderPanel getReceivingOrderPanel();
	HomePanel getHomePanel();
	ProductMainPanel getProductMainPanel();
	TransferPanel getTransferPanel();
	TransactionSummaryPanel getTransactionSummaryPanel();
	ReturnSupplierPanel getReturnSupplierPanel();
	TransactionPanel getTransactionPanel();
	StockSummaryPanel getStockSummaryPanel();
		
	ProductDetailCanvas getProductDetailCanvas();
	SupplierDetailCanvas getSupplierDetailCanvas();
	PurchasingOrderDetailCanvas getPurchasingOrderCanvas();
	PromoDetailCanvas getPromoDetailCanvas();
	ReceivingOrderDetailCanvas getReceivingOrderCanvas();
	ReturnToSupplierDetailCanvas getReturnToSupplierCanvas();
	TransferOrderDetailCanvas getTransferOrderCanvas();

}
