package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.LinkItem;

public class PrintBarcodeLinkItemProvider implements Provider<LinkItem>{

	public LinkItem get() {
		LinkItem link = new LinkItem("printBarcode");

		link.setShowTitle(false);
		link.setLinkTitle("Print Barcode >>");
		link.setShowDisabled(true);

		return link;
	}

}
