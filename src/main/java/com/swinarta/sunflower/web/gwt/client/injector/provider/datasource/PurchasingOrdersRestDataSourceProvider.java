package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.util.JSOHelper;

public class PurchasingOrdersRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	
	@Inject
	public PurchasingOrdersRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.FETCH){
					String actionId = JSOHelper.getAttribute(dsRequest.getData(), "actionId");
					if("fetchSingle".equalsIgnoreCase(actionId)){
						int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
						dsRequest.setActionURL(getUpdateDataURL()+id);
					}
				}else if(dsRequest.getOperationType() == DSOperationType.UPDATE){
					int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
					dsRequest.setActionURL(getUpdateDataURL()+id);					
				}
				return super.transformRequest(dsRequest);
			}
		};
		ds.setFetchDataURL("/purchasingorders/");
		ds.setAddDataURL("/purchasingorders/");
		ds.setUpdateDataURL("/purchasingorder/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, addOperationBinding, updateOperationBinding);

		DataSourceTextField poIdTextField = new DataSourceTextField("poId", "PO NUMBER");
		poIdTextField.setHidden(true);
		
		DataSourceDateField poDateTextField = new DataSourceDateField("poDate", "Order Date");
		poDateTextField.setRequired(true);
		poDateTextField.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		DataSourceDateField cancelDateTextField = new DataSourceDateField("cancelDate", "Cancel Date");
		cancelDateTextField.setRequired(true);
		cancelDateTextField.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		DataSourceTextField statusTextField = new DataSourceTextField("status", "STATUS");
		statusTextField.setHidden(true);

		DataSourceTextField remarksTextField = new DataSourceTextField("remarks", "Remarks");

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);
						
		ds.setFields(idTextField, poIdTextField, poDateTextField, cancelDateTextField, remarksTextField, statusTextField);

		return ds;
	}

}