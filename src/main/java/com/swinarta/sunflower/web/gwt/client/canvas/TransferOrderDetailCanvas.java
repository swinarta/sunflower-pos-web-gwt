package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.VLayout;

public class TransferOrderDetailCanvas extends VLayout{

	private Store currentStore = (Store) ClientSession.objects.get("store");

	final private TransferOrderBasicCanvas basicCanvas;
	final private TransferOrderListCanvas listCanvas;
		
	@Inject
	public TransferOrderDetailCanvas(
		final TransferOrderBasicCanvas basicCanvas,
		final TransferOrderListCanvas poListCanvas
		){
		
		this.basicCanvas = basicCanvas;
		this.listCanvas = poListCanvas;
		
		setCanSelectText(true);

		basicCanvas.setHeight(125);
		
		poListCanvas.setParentLayout(this);
		
		addMember(basicCanvas);
		addMember(poListCanvas);
		
		basicCanvas.setShowAddLink(currentStore.getIsHq());
		basicCanvas.setShowEditLink(currentStore.getIsHq());
				
		basicCanvas.setListGrid(poListCanvas.getOrderList());
		basicCanvas.setUpdateStatusCallback(new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record record = response.getData()[0];
				String status = record.getAttributeAsString("status");
				poListCanvas.setStatus(status);
			}
		});
				
	}
	
	public void loadBasicData(Record record){
				
		Integer id = record.getAttributeAsInt("id");
		String status = record.getAttributeAsString("status");
		
		basicCanvas.loadData(record);
		listCanvas.loadData(id);
		listCanvas.setStatus(status);
	}
	
}