package com.swinarta.sunflower.web.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HTML;
import com.swinarta.sunflower.web.gwt.client.canvas.MainCanvas;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.widget.SideNavTree;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Application implements EntryPoint, ValueChangeHandler<String> {

	private GardenGinjector injector = Ginjector.getInstance();

	private MainCanvas mainCanvas;
	private SideNavTree sideNav;
	private Label title;
	private Label username;
	private LinkItem logoutLinkItem;

	private Init init;
		
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad()
	{	  
		
		init = injector.getInit();
		sideNav = injector.getSideNavTree();
		mainCanvas = injector.getMainCanvas();
		title = injector.getMainTitleLabel();
		username = injector.getUserNameLabel();
		logoutLinkItem = injector.getLogoutLinkItem();
				
		Label separator = new Label("|");
		separator.setWidth(3);

		Label signOutLabel = new Label();
		signOutLabel.setContents("<a href=\"/sunflower/logout\">Sign out</a>");
		signOutLabel.setWidth(50);
		
		DynamicForm logoutForm = new DynamicForm();
		logoutForm.setFields(logoutLinkItem);
		logoutForm.setWidth(50);
		
		HTML hr = new HTML("<hr/>");
		hr.setHeight("3");
		hr.setWidth("100%");
		
		init.init();

		VLayout mainLayout = new VLayout();	  
		mainLayout.setWidth100();
		mainLayout.setHeight100();
		mainLayout.setLayoutMargin(5);
		mainLayout.setStyleName("tabSetContainer");
				
		HLayout topLayout = new HLayout(5);
		topLayout.setHeight(10);
		topLayout.addMember(username);
		topLayout.addMember(separator);
		topLayout.addMember(logoutForm);
		topLayout.setAlign(Alignment.RIGHT);
		
		HLayout titleLayout = new HLayout();
		titleLayout.addMember(title);
		titleLayout.setLayoutLeftMargin(5);        

		mainLayout.addMember(topLayout);
		mainLayout.addMember(hr);
		mainLayout.addMember(titleLayout);
		
		/* Center Panel */

		HLayout hLayout = new HLayout();
		hLayout.setLayoutMargin(5);
		hLayout.setWidth100();
		hLayout.setHeight100();

		VLayout sideNavLayout = new VLayout();
		sideNavLayout.setHeight100();
		sideNavLayout.setWidth(185);
		sideNavLayout.setShowResizeBar(true);

		sideNavLayout.addMember(sideNav);
		hLayout.addMember(sideNavLayout);      

		//canvas.addChild(mainTabSet);
		//add home panel
		//mainCanvas.addChild(injector.getPurchasingOrderPanel());
		//mainCanvas.addChild(injector.getReceivingOrderPanel());

		hLayout.addMember(mainCanvas);
		mainLayout.addMember(hLayout);

		History.addValueChangeHandler(this);
		History.fireCurrentHistoryState();
		
		mainLayout.draw();

	}

	public void onValueChange(ValueChangeEvent<String> event) {
		String token = History.getToken();
		if(token != null && token.length() != 0){
			if(token.startsWith("product")){
				injector.getProductMainPanel().changeHistoryState(token);
				
				if(!"product".equalsIgnoreCase(sideNav.getCurrentId()) && sideNav.getCurrentId() != null){
					sideNav.showPage("product");
				}

			}else if(token.startsWith("supplier")){
				injector.getSupplierPanel().changeHistoryState(token);
				
				if(!"supplier".equalsIgnoreCase(sideNav.getCurrentId()) && sideNav.getCurrentId() != null){
					sideNav.showPage("supplier");
				}
				
			}
		}
	}

}