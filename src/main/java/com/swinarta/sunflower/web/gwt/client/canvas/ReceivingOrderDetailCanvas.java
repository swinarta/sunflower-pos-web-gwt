package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrintBarcodeActionCallback;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrinterChangePresenceEvent;
import com.swinarta.sunflower.web.gwt.client.connectivity.impl.PrinterImpl;
import com.swinarta.sunflower.web.gwt.client.panel.ReceivingOrderPanel;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.VLayout;

public class ReceivingOrderDetailCanvas extends VLayout{

	final private ReceivingOrderBasicCanvas roBasicCanvas;
	final private ReceivingOrderListCanvas roListCanvas;
	private OnPrinterChangePresenceEvent onPrinterChangePresenceEvent;
	
	@Inject
	public ReceivingOrderDetailCanvas(
			final ReceivingOrderBasicCanvas roBasicCanvas,
			final ReceivingOrderListCanvas roListCanvas,
			final ReceivingOrderPanel roPanel
		){
		
		this.roBasicCanvas = roBasicCanvas;
		this.roListCanvas = roListCanvas;
		
		setCanSelectText(true);
		
		roBasicCanvas.setHeight(125);
		
		addMember(roBasicCanvas);
		addMember(roListCanvas);
		
		roBasicCanvas.setUpdateStatusCallback(new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record record = response.getData()[0];
				String status = record.getAttributeAsString("status");				
				roListCanvas.loadStatus(status);
				
				roPanel.populateListGrid();
				
			}
		});

		roBasicCanvas.setPrintBarcodeCallback(new OnPrintBarcodeActionCallback() {			
			public void execute() {
				roListCanvas.printBarcode();
			}
		});
		
		onPrinterChangePresenceEvent = new OnPrinterChangePresenceEvent() {
			
			public void changeStatus(String id, Boolean isAvailable, String statusStr) {
				if(PrinterImpl.PRINTER01 != null && PrinterImpl.PRINTER01.equalsIgnoreCase(id)){
					roBasicCanvas.setPrinterAvailability(isAvailable);
				}
			}
		};
		
		roListCanvas.setOnPrinterChangePresenceEvent(onPrinterChangePresenceEvent);
		
	}
	
	public void loadBasicData(Record record){
		roBasicCanvas.loadData(record);
		roListCanvas.loadData(record);
	}
	
}
