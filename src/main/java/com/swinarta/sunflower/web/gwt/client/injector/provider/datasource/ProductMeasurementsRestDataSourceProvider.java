package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;

public class ProductMeasurementsRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding addOperationBinding;
	final private OperationBinding fetchOperationBinding;

	@Inject
	public ProductMeasurementsRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Fetch") OperationBinding fetchOperationBinding
			){
		this.addOperationBinding = addOperationBinding;
		this.fetchOperationBinding = fetchOperationBinding;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/ds/productmeasurements/");
		ds.setAddDataURL("/ds/productmeasurements/");
		ds.setDataFormat(DSDataFormat.JSON);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("productId");
		idTextField.setHidden(true);
		
		ds.setOperationBindings(fetchOperationBinding, addOperationBinding);

		return ds;
	}

}