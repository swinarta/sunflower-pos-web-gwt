package com.swinarta.sunflower.web.gwt.client.forms;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;

public class TransferOrderForm extends DynamicForm{

	final private TextAreaItem remarksItem = new TextAreaItem("remarks", "Remarks");
	final private SelectItem fromStoreSelectItem;
	final private SelectItem toStoreSelectItem;
	
	private DSCallback saveCallback;
	
	@Inject
	public TransferOrderForm(
			@Named("TransferOrders") RestDataSource dataSource,
			@Named("Store") SelectItem fromStoreSelectItem,
			@Named("Store") SelectItem toStoreSelectItem
			){
		
		this.fromStoreSelectItem = fromStoreSelectItem;
		this.toStoreSelectItem = toStoreSelectItem;
		
		setTitleOrientation(TitleOrientation.TOP);

		setDataSource(dataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);
				
		fromStoreSelectItem.setRequired(true);
		toStoreSelectItem.setRequired(true);
		
		fromStoreSelectItem.setName("fromStoreCode");
		toStoreSelectItem.setName("toStoreCode");
		
		fromStoreSelectItem.setTitle("From Store");
		toStoreSelectItem.setTitle("To Store");
		
		setFields(fromStoreSelectItem, toStoreSelectItem, remarksItem);
	}
	
	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void editNewRecord(){
		clearErrors(true);
		
		super.editNewRecord();
	}
	
	public void editRecord(Record record){
		clearErrors(true);
		
		toStoreSelectItem.hide();
		fromStoreSelectItem.hide();
				
		super.editRecord(record);
	}
		
	public void saveData(){
		super.saveData(saveCallback);
	}	
	
}
