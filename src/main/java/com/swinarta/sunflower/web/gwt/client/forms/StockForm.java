package com.swinarta.sunflower.web.gwt.client.forms;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.SubmitValuesEvent;
import com.smartgwt.client.widgets.form.events.SubmitValuesHandler;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class StockForm extends DynamicForm{
	
	private TextItem currentItem = new TextItem("current");
	private TextItem maxItem = new TextItem("max");
	private TextItem minItem = new TextItem("min");
	private TextItem defaultOrderItem = new TextItem("defaultOrder");
	
	private HiddenItem productIdItem = new HiddenItem("productId");
	private HiddenItem storeIdItem = new HiddenItem("storeId");
	
	private DSCallback saveCallback;
	
	@Inject
	public StockForm(
			@Named("Stock") RestDataSource stockDataSource
			){
		setTitleOrientation(TitleOrientation.TOP);

		setDataSource(stockDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);
		
		currentItem.setSelectOnFocus(true);
		maxItem.setSelectOnFocus(true);
		minItem.setSelectOnFocus(true);
		defaultOrderItem.setSelectOnFocus(true);
		
		currentItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		maxItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		minItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		defaultOrderItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		
		setFields(currentItem, maxItem, minItem, defaultOrderItem, productIdItem, storeIdItem);
		
		initHandler();
	}
	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}
	
	public void setProductId(Integer productId){
		productIdItem.setValue(productId);
	}

	public void setStoreId(Integer storeId){
		storeIdItem.setValue(storeId);
	}

	public void initHandler(){
		addSubmitValuesHandler(new SubmitValuesHandler() {
			
			public void onSubmitValues(SubmitValuesEvent event) {
				saveData();
			}
		});
	}
	
	public void editNewRecord(Integer productId, Integer storeId){
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("productId", productId);
		map.put("storeId", storeId);
		currentItem.focusInItem();
		super.editNewRecord(map);
	}
	
	public void editRecord(Record record){
		currentItem.focusInItem();
		super.editRecord(record);
	}
	
	public void saveData(){
		super.saveData(saveCallback);
	}
}
