package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;

public class ReceivingOrderDetailsRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	final private OperationBinding updateOperationBinding;

	@Inject
	public ReceivingOrderDetailsRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource() {			
			
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.FETCH){
					int roId = JSOHelper.getAttributeAsInt(dsRequest.getData(), "roId");
					dsRequest.setActionURL(getFetchDataURL()+roId);
				} else if(dsRequest.getOperationType() == DSOperationType.UPDATE){
					int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
					dsRequest.setActionURL(getUpdateDataURL()+id);					
				}
				return super.transformRequest(dsRequest);
			}
			
		};
		
		ds.setFetchDataURL("/receivingorderdetails/");
		ds.setUpdateDataURL("/receivingorderdetail/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, updateOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
				
		ds.setFields(idTextField);
		
		return ds;
	}

}