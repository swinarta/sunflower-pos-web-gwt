package com.swinarta.sunflower.web.gwt.client.forms;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;

public class ReceivingOrderForm extends DynamicForm{

	final private DateItem roDateItem = new DateItem("roDate", "Receiving Date");
	final private TextAreaItem remarksItem = new TextAreaItem("remarks", "Remarks");
	final private DateCompareValidator minDateCompareValidator;
	
	private DSCallback saveCallback;
	
	@Inject
	public ReceivingOrderForm(
			@Named("ReceivingOrders") RestDataSource receivingOrdersDataSource,
			@Named("Min") DateCompareValidator minDateCompareValidator
			){
		this.minDateCompareValidator = minDateCompareValidator;
		
		setTitleOrientation(TitleOrientation.TOP);

		setDataSource(receivingOrdersDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);

		Date today = new Date();		
		roDateItem.setStartDate(today);

		roDateItem.setRequired(true);
		
		minDateCompareValidator.setOtherField("poDate");
		minDateCompareValidator.setErrorMessage("Cancel Date must be greater than PO Date");
				
		setFields(roDateItem, remarksItem);
	}
	
	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void editRecord(Record record){
		clearErrors(true);		
		super.editRecord(record);
	}
		
	public void saveData(){
		super.saveData(saveCallback);
	}	
	
}