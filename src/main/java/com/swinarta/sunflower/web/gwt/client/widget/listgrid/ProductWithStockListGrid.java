package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ProductWithStockListGrid extends ListGrid{

	private static Integer CURR_STOCK_FIELD = 3;
	
	@Inject
	public ProductWithStockListGrid(
			@Named("ProductWithStocks") RestDataSource dataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
		setDataSource(dataSource);
		
		setDataPageSize(50);
		setCanGroupBy(false);
		setCanFreezeFields(false);
		setCanEdit(false);
		setSortField(2);

		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);

		ListGridField deletedField = new ListGridField("deleteInd");
		deletedField.setHidden(true);

		ListGridField skuField = new ListGridField("sku", "SKU");
		skuField.setWidth(75);
		
		ListGridField barcodeField = new ListGridField("barcode", "Barcode");
		barcodeField.setWidth(100);
		
		ListGridField descriptionField = new ListGridField("longDescription", "Description");

		ListGridField stockCurrentField = new ListGridField("stock", "Curr. Stock");
		stockCurrentField.setWidth(50);
		stockCurrentField.setAlign(Alignment.RIGHT);
		
		stockCurrentField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record stockRecord = record.getAttributeAsRecord("stock");				
				
				if(stockRecord != null && stockRecord.getAttributeAsFloat("current") != null){
					return StringFormatUtil.getFormat(stockRecord.getAttributeAsFloat("current"));
				}
				
				return null;

			}
		});

		ListGridField stockMinField = new ListGridField("minstock", "Min. Stock");
		stockMinField.setWidth(50);
		stockMinField.setAlign(Alignment.RIGHT);
		
		stockMinField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record stockRecord = record.getAttributeAsRecord("stock");				
				
				if(stockRecord != null && stockRecord.getAttributeAsFloat("min") != null){
					return StringFormatUtil.getFormat(stockRecord.getAttributeAsInt("min"));
				}
				
				return null;

			}
		});

		ListGridField stockMaxField = new ListGridField("maxstock", "Max. Stock");
		stockMaxField.setWidth(50);
		stockMaxField.setAlign(Alignment.RIGHT);
		
		stockMaxField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record stockRecord = record.getAttributeAsRecord("stock");			
				
				if(stockRecord != null && stockRecord.getAttributeAsFloat("max") != null){
					return StringFormatUtil.getFormat(stockRecord.getAttributeAsInt("max"));
				}
				
				return null;

			}
		});

		setFields(skuField, barcodeField, descriptionField, stockCurrentField, stockMinField, stockMaxField);
		
	}

	@Override
	protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {

		if(colNum == CURR_STOCK_FIELD){
			Record stockRecord = record.getAttributeAsRecord("stock");
			if(stockRecord != null && stockRecord.getAttributeAsFloat("current") != null){
				if(stockRecord.getAttributeAsFloat("current") <= stockRecord.getAttributeAsInt("min")){
					return "color:red;";
				}else if(stockRecord.getAttributeAsFloat("current") >= stockRecord.getAttributeAsInt("max")){
					return "color:green;";
				}
			}
		}
		
		return super.getCellCSSText(record, rowNum, colNum);		
	}

}
