package com.swinarta.sunflower.web.gwt.client.connectivity;

import com.swinarta.sunflower.web.gwt.client.callback.OnPrinterChangePresenceEvent;


public interface Printer {

	public void init();
	public Boolean getStatus(String id);
	public String getStatusString(String id);
	public void sendPrintMessage(String printerId, String msg);
	public void register(OnPrinterChangePresenceEvent onChangePresenceEvent);
	
}
