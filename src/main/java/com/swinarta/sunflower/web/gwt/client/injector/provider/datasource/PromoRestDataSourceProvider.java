package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.DateUtils;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;

public class PromoRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;

	@Inject
	public PromoRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.FETCH){
					
					String actionId = JSOHelper.getAttribute(dsRequest.getData(), "actionId");
					if("checkPromo".equalsIgnoreCase(actionId)){
						Integer productId = JSOHelper.getAttributeAsInt(dsRequest.getData(), "productId");
						Date startDate = JSOHelper.getAttributeAsDate(dsRequest.getData(), "startDate");
						Date endDate = JSOHelper.getAttributeAsDate(dsRequest.getData(), "endDate");
						
						dsRequest.setActionURL(getFetchDataURL() + productId + "/" + DateUtils.getIsoDateFormat().format(startDate) + "/" + DateUtils.getIsoDateFormat().format(endDate));
					}
					
				}
				return super.transformRequest(dsRequest);
			}
		};
			
		ds.setFetchDataURL("/promo/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);

		DataSourceDateField startDateField = new DataSourceDateField("startDate", "Start Date");				
		startDateField.setCanEdit(true);
		startDateField.setCanSave(true);
		startDateField.setRequired(true);

		DataSourceDateField endDateField = new DataSourceDateField("endDate", "End Date");				
		endDateField.setCanEdit(true);
		endDateField.setCanSave(true);
		endDateField.setRequired(true);

		DataSourceTextField descriptionField = new DataSourceTextField("description", "Description");				
		descriptionField.setCanEdit(true);
		descriptionField.setCanSave(true);
		descriptionField.setRequired(false);

		DataSourceFloatField promoValueField = new DataSourceFloatField("promoValue", "Promo Value");				
		promoValueField.setCanEdit(true);
		promoValueField.setCanSave(true);
		promoValueField.setRequired(true);
		
		ds.setFields(idTextField, startDateField, endDateField, 
				promoValueField, descriptionField);

		return ds;
	}

}