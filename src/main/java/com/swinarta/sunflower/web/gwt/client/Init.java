package com.swinarta.sunflower.web.gwt.client;

import java.util.HashSet;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.callback.OnStoreLoadedCompleteEvent;
import com.swinarta.sunflower.web.gwt.client.canvas.MainCanvas;
import com.swinarta.sunflower.web.gwt.client.connectivity.Printer;
import com.swinarta.sunflower.web.gwt.client.connectivity.XmppClient;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.panel.ProductMainPanel;
import com.swinarta.sunflower.web.gwt.client.panel.SupplierPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransferPanel;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.swinarta.sunflower.web.gwt.client.widget.LoginWindow;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.rpc.HandleTransportErrorCallback;
import com.smartgwt.client.rpc.LoginRequiredCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.google.gwt.http.client.Response;

public class Init {

	private GardenGinjector injector = Ginjector.getInstance();
	
	final private RestDataSource storeDataSource;
	final private RestDataSource configDataSource;
	final private RestDataSource loginDataSource;
	final private XmppClient xmppClient;
	final private Printer printer;
	final private Label mainTitleLabel;
	final private Label userNameLabel;
	final private MainCanvas mainCanvas;
	final private LoginWindow loginWindow;
	
	final private ProductMainPanel productPanel;
	final private SupplierPanel supplierPanel;
	final private TransferPanel transferPanel;
	
	@Inject
	public Init(
		@Named("Config") RestDataSource configDataSource, 
		@Named("Store") RestDataSource storeDataSource,
		@Named("Login") RestDataSource loginDataSource,
		XmppClient xmppClient, 
		Printer printer,
		@Named("MainTitle") Label mainTitleLabel,
		MainCanvas mainCanvas,
		LoginWindow loginWindow,
		@Named("UserName") Label userNameLabel,
		ProductMainPanel productPanel,
		SupplierPanel supplierPanel,
		TransferPanel transferPanel
		){
		
		this.configDataSource = configDataSource;
		this.storeDataSource = storeDataSource;
		this.loginDataSource = loginDataSource;
		this.xmppClient = xmppClient;
		this.printer = printer;
		this.mainTitleLabel = mainTitleLabel;
		this.mainCanvas = mainCanvas;
		this.loginWindow = loginWindow;
		this.userNameLabel = userNameLabel;
		this.productPanel = productPanel;
		this.supplierPanel = supplierPanel;
		this.transferPanel = transferPanel;
	}
	
	public void init(){
		
		final Set<OnStoreLoadedCompleteEvent> storeCallback = new HashSet<OnStoreLoadedCompleteEvent>();		
		storeCallback.add(productPanel);
		storeCallback.add(supplierPanel);
		storeCallback.add(transferPanel);
		
		loginWindow.setLoginSuccessfullCallback(new LoginSuccessfullCallback() {
			public void execute() {
				initUserNameLabel();
				loginWindow.hide();
			}
		});
				
		RPCManager.setHandleTransportErrorCallback(new HandleTransportErrorCallback() {
			
			public void handleTransportError(final int transactionNum, int status, int httpResponseCode, String httpResponseText) {
				if(httpResponseCode == Response.SC_FORBIDDEN){
					
					SC.ask("You do not authorized to perform this action, Re-Login with other account?", new BooleanCallback() {
						
						public void execute(Boolean value) {
							if(value){
								loginWindow.setShowCloseButton(true);
								loginWindow.show();
								loginWindow.bringToFront();								
							}
						}
					});
				}
			}
		});
		
		RPCManager.setLoginRequiredCallback(new LoginRequiredCallback() {
			
			public void loginRequired(int transactionNum, RPCRequest request, RPCResponse response) {
				loginWindow.setShowCloseButton(false);
				loginWindow.show();
				loginWindow.bringToFront();
			}
		});

		configDataSource.fetchData(null, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record record = response.getData()[0];				
				ClientSession.objects.put("config", record);
				
				xmppClient.init();
				printer.init();

			}
		});

		storeDataSource.fetchData(null, new DSCallback() {
				
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					
					Record r = ((Record[])response.getData())[0];
					Store s = new Store(r);
					ClientSession.objects.put("store", s);
					
					mainTitleLabel.setContents("<b>SunFlower - " + s.getName() + "</b>");

					for (OnStoreLoadedCompleteEvent onStoreLoadedCompleteEvent : storeCallback) {
						onStoreLoadedCompleteEvent.performOnStoreLoaded(s);
					}					
					
					mainCanvas.addChild(injector.getProductMainPanel());
					
				}
			});

		if(ClientSession.objects.get("username") == null){
			initUserNameLabel();
		}
		
	}
	
	public void initUserNameLabel(){
		
		loginDataSource.fetchData(null, new DSCallback() {		
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record r = ((Record[])response.getData())[0];
				String username = r.getAttribute("username");
				userNameLabel.setContents(username);
				ClientSession.objects.put("username", username);
			}
		});		
	}
	
	
}
