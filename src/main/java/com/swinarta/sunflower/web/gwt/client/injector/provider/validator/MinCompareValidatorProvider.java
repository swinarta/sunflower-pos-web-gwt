package com.swinarta.sunflower.web.gwt.client.injector.provider.validator;

import com.google.inject.Provider;
import com.swinarta.sunflower.web.gwt.client.validator.CompareValidator;
import com.swinarta.sunflower.web.gwt.client.validator.CompareValidator.COMPARATOR;

public class MinCompareValidatorProvider implements Provider<CompareValidator>{

	public CompareValidator get() {
		CompareValidator validator = new CompareValidator();
		validator.setOtherField("min");
		validator.setComparator(COMPARATOR.GREATER_OR_EQUALS);
		validator.setErrorMessage("Invalid Max Value");
		return validator;
	}

}
