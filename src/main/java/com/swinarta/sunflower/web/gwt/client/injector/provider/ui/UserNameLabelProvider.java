package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Provider;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;

public class UserNameLabelProvider implements Provider<Label>{

	public Label get() {
		Label label = new Label();	
		label.setWidth(50);
		label.setAlign(Alignment.RIGHT);
		return label;
	}

}
