package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem;

import com.google.inject.Provider;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class Address2TextItemProvider implements Provider<TextItem> {

	public TextItem get() {
		TextItem item = new TextItem("address2", "Address2");
		item.setLength(255);
		item.setWidth(200);
		item.setCharacterCasing(CharacterCasing.UPPER);
		return item;
	}

}
