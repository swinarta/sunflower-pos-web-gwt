package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.window;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.Window;

public class AddProductWindowProvider implements Provider<Window> {

	public Window get() {
		Window window = new Window();
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.centerInPage();
		window.setShowCloseButton(true);
		window.setDismissOnEscape(true);

		return window;
	}

}
