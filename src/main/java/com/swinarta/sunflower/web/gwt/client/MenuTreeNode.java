package com.swinarta.sunflower.web.gwt.client;

import com.smartgwt.client.widgets.tree.TreeNode;

public class MenuTreeNode extends TreeNode{
	
	private String name;
	private String parent;
	private String id;
	
	public MenuTreeNode(String id, String name, String parent){
		this.name = name;
		this.parent = parent;
		this.id = id;
	}
		
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}	

}
