package com.swinarta.sunflower.web.gwt.client.forms;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.model.ClientDisplayProductMeasurement;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.util.PriceUtil;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.SubmitValuesEvent;
import com.smartgwt.client.widgets.form.events.SubmitValuesHandler;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.IsFloatValidator;

public class SellingForm extends DynamicForm{
	
	private Map<Integer, ClientDisplayProductMeasurement> measurementOverrideMap;
	
	private TextItem marginItem = new TextItem("marginItem", "Selling Margin - %");
	private StaticTextItem sellingPricePerUnitItem = new StaticTextItem("sellingPricePerUnitItem", "Selling Price per Unit");
	private SelectItem measurementItem = new SelectItem("measurementSelectionId", "Measurement");	
	private TextItem sellingPriceItem = new TextItem("sellingPrice");
	private StaticTextItem costPricePerUnitItem = new StaticTextItem("costPricePerUnitItem", "Cost Price per Unit");
	
	private Double costPricePerUnit;
	private DSCallback saveCallback;
	
	@Inject
	public SellingForm(
			@Named("Selling") RestDataSource sellingDataSource,
			@Named("Positive") FloatRangeValidator positiveFloatRangeValidator,
			IsFloatValidator isFloatValidator
			){
			
		setTitleOrientation(TitleOrientation.TOP);

		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);

		marginItem.setWidth(70);
		
		marginItem.setRequired(true);
		measurementItem.setRequired(true);
		sellingPricePerUnitItem.setRequired(true);
		costPricePerUnitItem.setRequired(true);
						
		setDataSource(sellingDataSource);
		setUseAllDataSourceFields(true);
		
		sellingPriceItem.setSelectOnFocus(true);
		
		sellingPriceItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		
		marginItem.setValidators(positiveFloatRangeValidator, isFloatValidator);
				
		setFields(costPricePerUnitItem, measurementItem, marginItem, sellingPricePerUnitItem, sellingPriceItem);
		
		initHandler();
	}

	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}
	
	public void setMeasurementOverrideMap(Map<Integer, ClientDisplayProductMeasurement> measurementOverrideMap) {
		this.measurementOverrideMap = measurementOverrideMap;
	}
	
	public void setCostPricePerUnit(Double costPricePerUnit) {
		this.costPricePerUnit = costPricePerUnit;
	}
	
	public void initHandler(){
		addSubmitValuesHandler(new SubmitValuesHandler() {
			
			public void onSubmitValues(SubmitValuesEvent event) {
				saveData();
			}
		});
		
		ChangedHandler priceChangedHandler = new ChangedHandler() {
			
			public void onChanged(ChangedEvent event) {
				calculateMarginOnChange();
			}
		};
		
		ChangedHandler marginChangedHandler = new ChangedHandler() {
			
			public void onChanged(ChangedEvent event) {
				calculatePriceOnChange();
			}
		};
		
		marginItem.addChangedHandler(marginChangedHandler);
		sellingPriceItem.addChangedHandler(priceChangedHandler);
		measurementItem.addChangedHandler(priceChangedHandler);
	}

	@SuppressWarnings("unchecked")
	public void editRecord(Record record){
				
		Map<String, Object> measurementMap = record.getAttributeAsMap("measurement");
		Integer defaultId = (Integer)measurementMap.get("id");		
		
		measurementItem.setDefaultValue(String.valueOf(defaultId));
		measurementItem.setValueMap(constructValueMap(measurementOverrideMap));
		
		super.editRecord(record);
		
		calculateMarginOnChange();
	}

	public void editNewRecord(Integer id){
		measurementItem.setValueMap(constructValueMap(measurementOverrideMap));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("costPricePerUnitItem", StringFormatUtil.getFormat(costPricePerUnit));
		
		super.editNewRecord(map);
	}
	
	public void saveData(){
		//TODO: read from db config in the future
		Boolean roundingCheck = true;
		Integer smallestRoundPrice = 25;
		
		Double sellingPrice = FormUtil.getDoubleValueFromFormItem(sellingPriceItem);		
		if(roundingCheck && (sellingPrice % smallestRoundPrice != 0)){
			SC.ask("Selling Price ROUNDING Warning, CONTINUE?", new BooleanCallback() {				
				public void execute(Boolean value) {
					if(value){
						saveDataParentForm(saveCallback);
					}else{
						sellingPriceItem.focusInItem();
					}
				}
			});
		}else{
			saveDataParentForm(saveCallback);
		}
	}

	private void saveDataParentForm(DSCallback callback){
		super.saveData(callback);
	}

	private void calculateMarginOnChange(){

		Double sellingPrice = null;
		Integer qty = null;

		try{
			
			sellingPrice = FormUtil.getDoubleValueFromFormItem(sellingPriceItem);		
			String measurementIdStr = measurementItem.getSelectedRecord().getAttribute("sellingPrice");
			Integer measurementId = Integer.parseInt(measurementIdStr);
			
			ClientDisplayProductMeasurement measurement = measurementOverrideMap.get(measurementId);
			qty = measurement.getQty();

		}catch (Exception e) {
		}

		calculateMargin(sellingPrice, qty);
	}
	
	private void calculatePriceOnChange(){
		
		Double margin = null;
		Integer qty = null;
		
		try{
			margin = FormUtil.getDoubleValueFromFormItem(marginItem);
			String measurementIdStr = measurementItem.getSelectedRecord().getAttribute("sellingPrice");
			Integer measurementId = Integer.parseInt(measurementIdStr);
			
			ClientDisplayProductMeasurement measurement = measurementOverrideMap.get(measurementId);
			qty = measurement.getQty();
			
		}catch (Exception e) {
		}
		
		calculateSellingPrice(margin, qty);
	}
	
	private void calculateMargin(Double sellingPrice, Integer measurementQty){
		Double margin = 0d;
		Double sellingPricePerUnit = 0d;
		try{
			sellingPricePerUnit = PriceUtil.getPricePerUnit(sellingPrice, measurementQty);
			margin = PriceUtil.getMargin(costPricePerUnit, sellingPricePerUnit);
		}catch (Exception e) {	
		}
		sellingPricePerUnitItem.setValue(StringFormatUtil.getFormat(sellingPricePerUnit));
		costPricePerUnitItem.setValue(StringFormatUtil.getFormat(costPricePerUnit));

		marginItem.setValue(margin);
	}
	
	private void calculateSellingPrice(Double margin, Integer measurementQty){
		Double sellingPricePerUnit = 0d;
		Double sellingPrice = 0d;
		
		try{
			sellingPricePerUnit = PriceUtil.getSellingPricePerUnit(costPricePerUnit, margin);
			sellingPrice = PriceUtil.getPrice(sellingPricePerUnit, measurementQty);
		}catch (Exception e) {
		}
		sellingPricePerUnitItem.setValue(StringFormatUtil.getFormat(sellingPricePerUnit));
		sellingPriceItem.setValue(sellingPrice);
		
	}
	
	private LinkedHashMap<String, String> constructValueMap(Map<Integer, ClientDisplayProductMeasurement> measurementOverrideMap){
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		for (Integer measurementId : measurementOverrideMap.keySet()) {
			ClientDisplayProductMeasurement measument = measurementOverrideMap.get(measurementId);
			valueMap.put(String.valueOf(measurementId), measument.getDescription() + " (" + measument.getQty() + ")");
		}
		
		return valueMap;
	}
	
}
