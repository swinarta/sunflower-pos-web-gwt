package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;

public class PromosRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	final private OperationBinding fetchOperationBinding;
	final private FloatRangeValidator positiveFloatRangeValidatorProvider;

	@Inject
	public PromosRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding,
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Positive") FloatRangeValidator positiveFloatRangeValidatorProvider,
			@Named("Min") DateCompareValidator minDateCompareValidator
			){
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
		this.fetchOperationBinding = fetchOperationBinding;
		this.positiveFloatRangeValidatorProvider = positiveFloatRangeValidatorProvider;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
			
		ds.setFetchDataURL("/promos/");
		ds.setUpdateDataURL("/promo/");
		ds.setAddDataURL("/promos/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, updateOperationBinding, addOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);

		DataSourceDateField startDateField = new DataSourceDateField("startDate", "Start Date");				
		startDateField.setCanEdit(true);
		startDateField.setCanSave(true);
		startDateField.setRequired(true);

		DataSourceDateField endDateField = new DataSourceDateField("endDate", "End Date");				
		endDateField.setCanEdit(true);
		endDateField.setCanSave(true);
		endDateField.setRequired(true);

		DataSourceTextField descriptionField = new DataSourceTextField("description", "Description");				
		descriptionField.setCanEdit(true);
		descriptionField.setCanSave(true);
		descriptionField.setRequired(false);

		DataSourceFloatField promoValueField = new DataSourceFloatField("promoValue", "Promo Value");				
		promoValueField.setCanEdit(true);
		promoValueField.setCanSave(true);
		promoValueField.setRequired(true);
		
		promoValueField.setValidators(positiveFloatRangeValidatorProvider);
		
		ds.setFields(idTextField, startDateField, endDateField, 
				promoValueField, descriptionField);

		return ds;
	}

}