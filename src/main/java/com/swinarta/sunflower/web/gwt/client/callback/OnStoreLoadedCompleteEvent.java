package com.swinarta.sunflower.web.gwt.client.callback;

import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;

public interface OnStoreLoadedCompleteEvent {
	public void performOnStoreLoaded(Store store);
}
