package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.Label;

public class MainTitleLabelProvider implements Provider<Label>{

	public Label get() {
		Label label = new Label();
		label.setStyleName("sgwtTitle");
		label.setWidth(300);
		label.setHeight(20);
		return label;
	}

}
