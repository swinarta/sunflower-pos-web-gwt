package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TransferOrderListGrid extends ListGrid{

	@Inject
	public TransferOrderListGrid(
			@Named("TransferOrders") RestDataSource dataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
		setDataSource(dataSource);
		
		setDataPageSize(50);
		setCanGroupBy(false); 
		setCanFreezeFields(false);
		
		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);
		
		ListGridField transferIdField = new ListGridField("transferId", "Transfer ID");
		transferIdField.setWidth(80);
		transferIdField.setAlign(Alignment.CENTER);

		ListGridField transferDateField = new ListGridField("transferDate", "Transfer Date");
		transferDateField.setWidth(100);
		transferDateField.setAlign(Alignment.CENTER);
		transferDateField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return StringFormatUtil.getShortDateFormat(value);
			}
		});

		ListGridField transferStatusField = new ListGridField("status", "Status");
		transferStatusField.setWidth(100);
		transferStatusField.setAlign(Alignment.CENTER);

		ListGridField fromStoreField = new ListGridField("fromStoreCode", "From Store");
		fromStoreField.setWidth(80);
		fromStoreField.setAlign(Alignment.CENTER);

		ListGridField toStoreField = new ListGridField("toStoreCode", "To Store");
		toStoreField.setWidth(80);
		toStoreField.setAlign(Alignment.CENTER);

		setFields(idField, transferIdField, transferDateField, fromStoreField, toStoreField, transferStatusField);
						
	}

}
