package com.swinarta.sunflower.web.gwt.client.model;

import java.io.Serializable;

public class ClientDisplayProductMeasurement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6830723874159947224L;
	
	private Integer qty;
	private String description;
	private Boolean isDefault;
	private Boolean isMutable;
	
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	public Boolean getIsMutable() {
		return isMutable;
	}
	public void setIsMutable(Boolean isMutable) {
		this.isMutable = isMutable;
	}
		
}