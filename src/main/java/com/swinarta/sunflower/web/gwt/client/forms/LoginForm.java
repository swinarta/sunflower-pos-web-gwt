package com.swinarta.sunflower.web.gwt.client.forms;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.LoginSuccessfullCallback;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.BlurbItem;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;

public class LoginForm extends DynamicForm{

	BlurbItem loginFailureItem = new BlurbItem("loginFailure");
    TextItem usernameItem = new TextItem("username", "Username");
    PasswordItem passwordItem = new PasswordItem("password", "Password");
    ButtonItem loginItem = new ButtonItem("Login");

    RPCRequest request = new RPCRequest();
    LoginSuccessfullCallback callback;

    @Inject
	public LoginForm(){

        loginFailureItem.setVisible(false);
        loginFailureItem.setColSpan(2);
        loginFailureItem.setDefaultValue("Invalid username or password");
        loginFailureItem.setCellStyle("formCellError");

        usernameItem.setTitleOrientation(TitleOrientation.LEFT);
        usernameItem.addKeyPressHandler(new KeyPressHandler(){
            public void onKeyPress(KeyPressEvent keyPressEvent) {
                if(keyPressEvent.getKeyName().equalsIgnoreCase("enter")){
                    focusInItem("password");
                }
            }
        });
        passwordItem.setTitleOrientation(TitleOrientation.LEFT);
        passwordItem.addKeyPressHandler(new KeyPressHandler(){
            public void onKeyPress(KeyPressEvent keyPressEvent) {
                if(keyPressEvent.getKeyName().equalsIgnoreCase("enter")){
                    doLogin();
                }
            }
        });
        loginItem.addClickHandler(new ClickHandler(){
            public void onClick(ClickEvent clickEvent) {
                doLogin();
            }
        });
        
        setFields(loginFailureItem, usernameItem, passwordItem, loginItem);
        
		setMargin(5);
	}
   
	public void setCallback(LoginSuccessfullCallback callback) {
		this.callback = callback;
	}
	
	protected void doLogin() {

		request.setContainsCredentials(true);
		request.setActionURL("/sunflower/LoginServlet");
        request.setUseSimpleHttp(true);
        request.setShowPrompt(false);
        request.setServerOutputAsString(true);
        request.setSendNoQueue(true);
        
        Map<String,String> params=new HashMap<String,String>();
        params.put("j_username",getValueAsString("username"));
        params.put("j_password",getValueAsString("password"));
        request.setParams(params);

        RPCManager.sendRequest(request, new RPCCallback(){
            public void execute(RPCResponse response, Object rawData, RPCRequest request) {
                clearValues();
                if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
                    hideItem("loginFailure");
                    RPCManager.resendTransaction();
                                                                                
                    if(callback != null){
                        callback.execute();
                    }
                }else if(response.getStatus() == RPCResponse.STATUS_LOGIN_INCORRECT){
                    showItem("loginFailure");
                }else if(response.getStatus() == RPCResponse.STATUS_MAX_LOGIN_ATTEMPTS_EXCEEDED){
                    SC.warn("Max login attempts exceeded.");
                }
                
                focusInItem("username");
            }
        });

	}

}
