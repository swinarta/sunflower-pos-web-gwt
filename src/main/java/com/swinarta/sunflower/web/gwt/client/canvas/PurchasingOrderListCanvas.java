package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrinterChangePresenceEvent;
import com.swinarta.sunflower.web.gwt.client.connectivity.Printer;
import com.swinarta.sunflower.web.gwt.client.connectivity.impl.PrinterImpl;
import com.swinarta.sunflower.web.gwt.client.util.PrinterUtil;
import com.swinarta.sunflower.web.gwt.client.widget.AddProductToDetailWindow;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PurchasingOrderDetailListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
 
public class PurchasingOrderListCanvas extends VLayout{

	private DynamicForm addProductToDetailForm = new DynamicForm();	
		
	final private PurchasingOrderDetailListGrid orderList;
	final private AddProductToDetailWindow addProductWindow;
	final private LinkItem addProductLinkItem;
	final private Printer printer;
	
	private OnPrinterChangePresenceEvent onPrinterChangePresenceEvent;
	
	@Inject
	public PurchasingOrderListCanvas(
			final PurchasingOrderDetailListGrid orderList,
			@Named("AddProduct") LinkItem addProductLinkItem,
			AddProductToDetailWindow addProductWindow,
			Printer printer
			){
		
		this.orderList = orderList;
		this.addProductWindow = addProductWindow;
		this.addProductLinkItem = addProductLinkItem;
		this.printer = printer;
		
		setGroupTitle("Order Details");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutRightMargin(8);
		setLayoutBottomMargin(8);
		setMargin(3);		
					
		addProductToDetailForm.setFields(addProductLinkItem);
		
		addProductWindow.setAddProductToDetail(orderList);
		addProductWindow.setShowQty(true);
		addProductWindow.hide();
		
		addMember(addProductToDetailForm);
		addMember(orderList);
				
		initHandler();
	}
	
	public void setParentLayout(Layout parent){
		parent.addChild(addProductWindow);
	}
	
	private void initHandler(){
		addProductLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				addProductWindow.centerInPage();
				addProductWindow.show();
			}
		});		
	}

	public void loadData(Integer poId){
		Criteria c = new Criteria();
		c.setAttribute("poId", poId);
		orderList.fetchData(c);
		orderList.setPoId(poId);
		
		onPrinterChangePresenceEvent.changeStatus(PrinterImpl.PRINTER01, printer.getStatus(PrinterImpl.PRINTER01), printer.getStatusString(PrinterImpl.PRINTER01));
	}

	public void setOnPrinterChangePresenceEvent(OnPrinterChangePresenceEvent onPrinterChangePresenceEvent) {
		this.onPrinterChangePresenceEvent = onPrinterChangePresenceEvent;
		printer.register(onPrinterChangePresenceEvent);
	}

	public void setStatus(String status){
		orderList.setStatus(status);
		orderList.setCanEdit("NEW".equalsIgnoreCase(status));
		if("NEW".equalsIgnoreCase(status)){
			addProductToDetailForm.show();
		}else{
			addProductToDetailForm.hide();
		}
	}
	
	public void setSupplierId(Integer supplierId){
		addProductWindow.setSupplierId(supplierId);
	}

	public PurchasingOrderDetailListGrid getOrderList() {
		return orderList;
	}

	public void printBarcode(){
		ListGridRecord[] records = orderList.getSelection();
		if(records.length > 0){
			Boolean toPrint = false;
			StringBuffer buffer = new StringBuffer();
			buffer.append(PrinterUtil.PRINT_KEYWORD);
			buffer.append(PrinterUtil.SEPARATOR);
			for (ListGridRecord listGridRecord : records) {
				Integer qty = listGridRecord.getAttributeAsInt("qty");
				if(qty > 0){
					toPrint = true;
					Record productRecord = listGridRecord.getAttributeAsRecord("product");
					Record sellingRecord = productRecord.getAttributeAsRecord("selling");
					
					Record buyingRecord = productRecord.getAttributeAsRecord("buying");
					Record buyingMeasurementRecord = buyingRecord.getAttributeAsRecord("measurement");
					Record productMeasurementRecord = productRecord.getAttributeAsRecord("productMeasurement");

					Boolean isMutable = buyingMeasurementRecord.getAttributeAsBoolean("mutable");
					
					Integer measurementQty;
					
					if(!isMutable){
						measurementQty = buyingMeasurementRecord.getAttributeAsInt("defaultQty");
					}else{
						measurementQty = getMeasurementQty(productMeasurementRecord, buyingMeasurementRecord);
					}

					
					String keyword = PrinterUtil.constructPrinterActionStrIndividual(IConstants.PRINT_MODE_BARCODE_WITH_PRICE, productRecord.getAttribute("barcode"), qty*measurementQty, productRecord.getAttribute("shortDescription"), sellingRecord.getAttributeAsDouble("sellingPrice"));
					buffer.append(keyword);
					buffer.append(PrinterUtil.PROD_SEPARATOR);
				}
			}
			if(toPrint){
				printer.sendPrintMessage(PrinterImpl.PRINTER01, buffer.toString());
			}
			SC.say("PRINTING SUCCESS (" + records.length + " PRODUCTS)");
		}else{
			SC.say("NO PRODUCT HAS BEEN SELECTED FOR PRINTING");
		}
	
	}

	private Integer getMeasurementQty(Record productMeasurementRecord, Record buyingMeasurementRecord){
		
		Record measurementFromPmRecord = productMeasurementRecord.getAttributeAsRecord("measurement");		
		Integer measurementQty = buyingMeasurementRecord.getAttributeAsInt("defaultQty");
		
		if(productMeasurementRecord != null && measurementFromPmRecord != null){
			if(measurementFromPmRecord.getAttributeAsInt("id").intValue() == buyingMeasurementRecord.getAttributeAsInt("id").intValue()){
				measurementQty = productMeasurementRecord.getAttributeAsInt("overrideQty");
			}			
		}
		
		return measurementQty;
	}

}