package com.swinarta.sunflower.web.gwt.client.widget.tabset;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.History;
import com.swinarta.sunflower.web.gwt.client.canvas.SupplierDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

public class SupplierTabSet extends TabSet{

	public SupplierTabSet(){
		setTabBarPosition(Side.TOP);  
		setTabBarAlign(Side.LEFT);
		
		addTabSelectedHandler(new TabSelectedHandler() {
			
			public void onTabSelected(TabSelectedEvent event) {
				Tab selectedTab = event.getTab();				
				History.newItem("supplier-"+ selectedTab.getID());
			}
		});
		
	}
	
	public void loadSupplier(final Record record){
		
		SC.showPrompt("Loading Supplier ... ");
		
		DeferredCommand.addCommand(new Command() {
			
			public void execute() {
				String tabId = record.getAttribute("id");
				if(getTab(tabId) == null){
					String title = record.getAttribute("name");

					GardenGinjector injector = Ginjector.getInstance();
					SupplierDetailCanvas suppDetailCanvas = injector.getSupplierDetailCanvas();
					
					Tab tab = new Tab(title);
					tab.setCanClose(true);
					tab.setPane(suppDetailCanvas);
					tab.setID(tabId);
					
					suppDetailCanvas.fetchData(record);
					addTab(tab);						
				}
				
				selectTab(tabId);
				
				SC.clearPrompt();
			
			}
		});
		
	}
	
}