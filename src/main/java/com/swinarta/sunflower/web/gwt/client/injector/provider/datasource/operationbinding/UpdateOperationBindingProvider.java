package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding;

import com.google.inject.Provider;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DSProtocol;

public class UpdateOperationBindingProvider implements Provider<OperationBinding>{

	public OperationBinding get() {
		OperationBinding add = new OperationBinding();
		add.setOperationType(DSOperationType.UPDATE);
		add.setDataProtocol(DSProtocol.POSTMESSAGE);
		return add;
	}

}
