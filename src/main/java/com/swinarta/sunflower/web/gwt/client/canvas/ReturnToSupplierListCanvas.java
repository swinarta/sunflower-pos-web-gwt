package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.widget.AddProductToDetailWindow;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ReturnToSupplierDetailListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
 
public class ReturnToSupplierListCanvas extends VLayout{

	private DynamicForm addProductToDetailForm = new DynamicForm();	
		
	final private ReturnToSupplierDetailListGrid listgrid;
	final private AddProductToDetailWindow addProductWindow;
	final private LinkItem addProductLinkItem;
		
	@Inject
	public ReturnToSupplierListCanvas(
			final ReturnToSupplierDetailListGrid listgrid,
			@Named("AddProduct") LinkItem addProductLinkItem,
			AddProductToDetailWindow addProductWindow
			){
		
		this.listgrid = listgrid;
		this.addProductWindow = addProductWindow;
		this.addProductLinkItem = addProductLinkItem;
		
		setGroupTitle("Return To Supplier Details");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutRightMargin(8);
		setLayoutBottomMargin(8);
		setMargin(3);		
					
		addProductToDetailForm.setFields(addProductLinkItem);
		
		addProductWindow.setAddProductToDetail(listgrid);
		addProductWindow.setShowQty(true);
		addProductWindow.hide();
		
		addMember(addProductToDetailForm);
		addMember(listgrid);
				
		initHandler();
	}
	
	public void setParentLayout(Layout parent){
		parent.addChild(addProductWindow);
	}
	
	private void initHandler(){
		addProductLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				addProductWindow.centerInPage();
				addProductWindow.show();
			}
		});		
	}

	public void loadData(Integer retId){
		Criteria c = new Criteria();
		c.setAttribute("retId", retId);
		listgrid.fetchData(c);
		listgrid.setReturnId(retId);		
	}

	public void setStatus(String status){
		listgrid.setStatus(status);
		if("NEW".equalsIgnoreCase(status)){
			addProductToDetailForm.show();
		}else{
			addProductToDetailForm.hide();
		}
	}
	
	public void setSupplierId(Integer supplierId){
		addProductWindow.setSupplierId(supplierId);
	}

	public ListGrid getReturnList() {		
		return listgrid;
	}

}