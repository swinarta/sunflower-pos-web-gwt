package com.swinarta.sunflower.web.gwt.client.injector.module;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.swinarta.sunflower.web.gwt.client.injector.provider.criteria.GetAllCriteriaProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.criteria.GetNextSkuCriteriaProvider;
import com.smartgwt.client.data.Criteria;

public class CriteriaModule extends AbstractGinModule{

	@Override
	protected void configure() {
		bind(Criteria.class).annotatedWith(Names.named("GetAll")).toProvider(GetAllCriteriaProvider.class).in(Singleton.class);
		bind(Criteria.class).annotatedWith(Names.named("GetNextSku")).toProvider(GetNextSkuCriteriaProvider.class).in(Singleton.class);
	}

}
