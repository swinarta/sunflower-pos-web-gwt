package com.swinarta.sunflower.web.gwt.client.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.model.ClientDisplayProductMeasurement;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.SubmitValuesEvent;
import com.smartgwt.client.widgets.form.events.SubmitValuesHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;

public class ProductMeasurementForm extends DynamicForm{

	private List<TextItem> measurementItems = new ArrayList<TextItem>();
	
	final private IntegerRangeValidator positiveIntegerRangeValidatorProvider;
	
	private DSCallback saveCallback;
	
	@Inject
	public ProductMeasurementForm(
			@Named("ProductMeasurements") RestDataSource productMeasurementDataSource,
			@Named("Positive") IntegerRangeValidator positiveIntegerRangeValidatorProvider
			){

		this.positiveIntegerRangeValidatorProvider = positiveIntegerRangeValidatorProvider;
		
		setMargin(10);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);
		
		setDataSource(productMeasurementDataSource);
		
		initHandler();
		
	}

	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void setMeasurementOverrideCMap(Map<Integer, ClientDisplayProductMeasurement> measurementOverrideCMap) {

		measurementItems.clear();
		for (Map.Entry<Integer, ClientDisplayProductMeasurement> entry : measurementOverrideCMap.entrySet()) {
			if(entry.getValue().getIsMutable()){
				TextItem measurementItem = new TextItem(String.valueOf(entry.getKey()), entry.getValue().getDescription());
				measurementItem.setRequired(true);
				measurementItem.setWidth(60);
				measurementItem.setDefaultValue(entry.getValue().getQty());
				measurementItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
				measurementItem.setValidators(positiveIntegerRangeValidatorProvider);
				measurementItems.add(measurementItem);
			}
		}
		setFields(measurementItems.toArray(new TextItem[]{}));
	}

	private void initHandler() {
		addSubmitValuesHandler(new SubmitValuesHandler() {
			
			public void onSubmitValues(SubmitValuesEvent event) {
				saveData();
			}
		});
	}

	public void editRecord(Integer productId){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("productId", productId);
		super.editNewRecord(map);
	}

	public void saveData(){
		super.saveData(saveCallback);
	}

}
