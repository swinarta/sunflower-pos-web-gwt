package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Map;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrintBarcodeActionCallback;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrinterChangePresenceEvent;
import com.swinarta.sunflower.web.gwt.client.connectivity.impl.PrinterImpl;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.VLayout;

public class PurchasingOrderDetailCanvas extends VLayout{

	final private PurchasingOrderBasicCanvas poBasicCanvas;
	final private PurchasingOrderListCanvas poListCanvas;
	
	private OnPrinterChangePresenceEvent onPrinterChangePresenceEvent;
	
	@Inject
	public PurchasingOrderDetailCanvas(
		final PurchasingOrderBasicCanvas poBasicCanvas,
		final PurchasingOrderListCanvas poListCanvas
		){
		
		this.poBasicCanvas = poBasicCanvas;
		this.poListCanvas = poListCanvas;
		
		setCanSelectText(true);

		poBasicCanvas.setHeight(125);
		
		poListCanvas.setParentLayout(this);
		
		addMember(poBasicCanvas);
		addMember(poListCanvas);
		
		poBasicCanvas.setListGrid(poListCanvas.getOrderList());
		poBasicCanvas.setUpdateStatusCallback(new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record record = response.getData()[0];
				String status = record.getAttributeAsString("status");
				poListCanvas.setStatus(status);
			}
		});
		
		poBasicCanvas.setPrintBarcodeCallback(new OnPrintBarcodeActionCallback() {			
			public void execute() {
				poListCanvas.printBarcode();				
			}
		});
		
		onPrinterChangePresenceEvent = new OnPrinterChangePresenceEvent() {
			
			public void changeStatus(String id, Boolean isAvailable, String statusStr) {
				if(PrinterImpl.PRINTER01 != null && PrinterImpl.PRINTER01.equalsIgnoreCase(id)){
					poBasicCanvas.setPrinterAvailability(isAvailable);
				}
			}
		};
		
		poListCanvas.setOnPrinterChangePresenceEvent(onPrinterChangePresenceEvent);
		
	}
	
	@SuppressWarnings("unchecked")
	public void loadBasicData(Record record){
		
		Map<String, Object> suppMap = record.getAttributeAsMap("supplier");
		
		Integer poId = record.getAttributeAsInt("id");
		String status = record.getAttributeAsString("status");
		Integer supplierId = (Integer)suppMap.get("id");
		
		poBasicCanvas.loadData(record);
		poListCanvas.loadData(poId);
		poListCanvas.setStatus(status);
		poListCanvas.setSupplierId(supplierId);
	}
	
}