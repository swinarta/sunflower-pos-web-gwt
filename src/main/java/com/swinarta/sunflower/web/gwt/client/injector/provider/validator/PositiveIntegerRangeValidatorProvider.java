package com.swinarta.sunflower.web.gwt.client.injector.provider.validator;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;

public class PositiveIntegerRangeValidatorProvider implements Provider<IntegerRangeValidator>{

	public IntegerRangeValidator get() {
		IntegerRangeValidator validator = new IntegerRangeValidator();
		validator.setMin(0);
		validator.setErrorMessage("Invalid Value");
		return validator;
	}

}
