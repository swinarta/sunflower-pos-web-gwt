package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.LinkItem;

public class AddReturnSupplierLinkItemProvider implements Provider<LinkItem>{

	public LinkItem get() {
		LinkItem link = new LinkItem("addRetSup");

		link.setTarget("javascript");
		link.setShowTitle(false);
		link.setLinkTitle("Add Return To Supplier >>");

		return link;
	}

}
