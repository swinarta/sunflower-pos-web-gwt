package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PurchasingOrderListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.ReceivingOrderTabSet;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ReceivingOrderPanel extends BasePanel{

	private static final String DESCRIPTION = "ro";

    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
        	ReceivingOrderPanel panel = injector.getReceivingOrderPanel();
        	id = DESCRIPTION;
            return panel;
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }

    final private PurchasingOrderListGrid poListGrid;
    final private Tab tab;
    final private Tab searchTab;
    final private ReceivingOrderTabSet tabSet;
    final private RestDataSource receivingOrdersDataSource;
    final private RestDataSource receivingOrderDataSource;
    
    final private TextItem codeTextItem = new TextItem("searchCode", "PO/RO Code");
    final private ButtonItem searchButton = new ButtonItem("searchCodeBtn", "Search");
    final private DynamicForm searchForm = new DynamicForm();
    
    @Inject
    public ReceivingOrderPanel(
    	PurchasingOrderListGrid poListGrid,
    	@Named("ReceivingOrder") Tab tab,
    	@Named("SearchReceivingOrder") Tab searchTab,
    	ReceivingOrderTabSet tabSet,
    	@Named("ReceivingOrders") RestDataSource receivingOrdersDataSource,
    	@Named("ReceivingOrder") RestDataSource receivingOrderDataSource
    		){
    	
    	this.poListGrid = poListGrid;
    	this.tab = tab;
    	this.searchTab = searchTab;
    	this.tabSet = tabSet;    	
    	this.receivingOrdersDataSource = receivingOrdersDataSource;
    	this.receivingOrderDataSource = receivingOrderDataSource;

    	init();
    	initComponent();
    	initHandler();
    }
    
    private void initComponent(){
		poListGrid.setWidth(610);
		poListGrid.setHeight(600);    	
    }
    
	@Override
	public Canvas getViewPanel() {
		Label currentLabel = new Label("<b>Processed Purchasing Order</b>");
		currentLabel.setHeight(14);

		VLayout layout = new VLayout();
		layout.setLayoutTopMargin(10);
		layout.setLayoutLeftMargin(25);
		
		layout.addMember(currentLabel);
		layout.addMember(poListGrid);
		
		codeTextItem.setSelectOnFocus(true);
		codeTextItem.setWrapTitle(false);
		codeTextItem.setCharacterCasing(CharacterCasing.UPPER);

		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);

		searchForm.setFields(codeTextItem, searchButton);
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);
		
		VLayout layout2 = new VLayout();
		layout2.setLayoutTopMargin(10);
		layout2.setLayoutLeftMargin(25);

		layout2.addMember(searchForm);
		
		tab.setPane(layout);
		searchTab.setPane(layout2);
		
		tabSet.addTab(tab);
		tabSet.addTab(searchTab);
		
		populateListGrid();
		
		return tabSet;
	}
		
	public void populateListGrid(){
		Criteria criteria = new Criteria("poStatusStr","PROCESSED");
		poListGrid.invalidateCache();
		poListGrid.fetchData(criteria);		
	}

	private void initHandler(){
		poListGrid.addDoubleClickHandler(new DoubleClickHandler() {			
			public void onDoubleClick(DoubleClickEvent event) {
				ListGrid list = (ListGrid)event.getSource();
				Integer poId = list.getSelectedRecord().getAttributeAsInt("id");
				String poCode = list.getSelectedRecord().getAttribute("poId");
				searchByPo(poId, poCode);
			}
		});

		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				String text = searchForm.getValueAsString("searchCode");
				searchByCode(text);
				}  
		});
	}

	protected void searchByCode(String code) {
		if((code != null && code.length() > 2)){
			Criteria criteria = new Criteria("actionId", "searchByCode");
			criteria.addCriteria("code", code);
			receivingOrderDataSource.fetchData(criteria, new DSCallback() {					
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData().length > 0){
						tabSet.loadRO(response.getData()[0]);
					}else{
						SC.warn("Receiving Order not found");
					}						
				}
			});
			
		}
		searchForm.focus();	
	}
	
	protected void searchByPo(final Integer poId, final String poCode){
		Criteria criteria = new Criteria("actionId", "searchByPo");
		criteria.addCriteria("poId", poId);
		
		receivingOrderDataSource.fetchData(criteria, new DSCallback() {					
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData().length > 0){
					tabSet.loadRO(response.getData()[0]);
				}else{
					SC.ask("Create Receiving Order for PO " + poCode, new BooleanCallback() {					
						public void execute(Boolean value) {
							if(value){
								Record record = new Record();
								record.setAttribute("poId", poId);
								receivingOrdersDataSource.addData(record, new DSCallback() {											
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										tabSet.loadRO(response.getData()[0]);												
									}
								});
							}
						}
					});
				}						
			}
		});

	}

}
