package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding;

import com.google.inject.Provider;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DSProtocol;

public class FetchOperationBindingProvider implements Provider<OperationBinding>{

	public OperationBinding get() {
		OperationBinding add = new OperationBinding();
		add.setOperationType(DSOperationType.FETCH);
		add.setDataProtocol(DSProtocol.POSTMESSAGE);
		return add;
	}

}
