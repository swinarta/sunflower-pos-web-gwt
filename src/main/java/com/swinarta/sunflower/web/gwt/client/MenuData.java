package com.swinarta.sunflower.web.gwt.client;

import com.swinarta.sunflower.web.gwt.client.panel.ProductMainPanel;
import com.swinarta.sunflower.web.gwt.client.panel.PromoPanel;
import com.swinarta.sunflower.web.gwt.client.panel.PurchasingOrderPanel;
import com.swinarta.sunflower.web.gwt.client.panel.ReceivingOrderPanel;
import com.swinarta.sunflower.web.gwt.client.panel.ReturnSupplierPanel;
import com.swinarta.sunflower.web.gwt.client.panel.StockSummaryPanel;
import com.swinarta.sunflower.web.gwt.client.panel.SupplierPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransactionPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransactionSummaryPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransferPanel;

public class MenuData {

	 private static ExplorerTreeNode[] data;
	 
	 public static ExplorerTreeNode[] getData() {
		 
		 if(data == null){
	            data = new ExplorerTreeNode[]{
	            	new ExplorerTreeNode("Supplier", "supplier", "root", "sunflower/supplier.png", new SupplierPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Product", "product", "root", "sunflower/product.png", new ProductMainPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Purchasing Order", "po", "root", "sunflower/po.png", new PurchasingOrderPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Receiving Order", "ro", "root", "sunflower/ro.png", new ReceivingOrderPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Transfer Product", "transfer", "root", "sunflower/house.png", new TransferPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Return To Supplier", "retsup", "root", "sunflower/retsupp.png", new ReturnSupplierPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Promotion", "promo", "root", "sunflower/promo.png", new PromoPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Transaction", "transaction", "root", "sunflower/transaction.png", new TransactionPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Transaction Summary", "transaction_summ", "root", "sunflower/transaction_summ.png", new TransactionSummaryPanel.Factory(), true, ""),
	            	new ExplorerTreeNode("Stock Summary", "stock", "root", "sunflower/stock_summ.png", new StockSummaryPanel.Factory(), true, ""),
	            };
		 }
		 
		 return data;
		 
	 }
	
}
