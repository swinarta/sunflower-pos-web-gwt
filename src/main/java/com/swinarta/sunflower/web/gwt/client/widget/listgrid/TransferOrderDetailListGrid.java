package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellSavedEvent;
import com.smartgwt.client.widgets.grid.events.CellSavedHandler;

public class TransferOrderDetailListGrid extends ListGrid implements AddProductToDetail{

	public static Integer EDITABLE_COLUMN = 3;
		
	private ListGridField skuField = new ListGridField("sku", "SKU");
	private ListGridField barcodeField = new ListGridField("barcode", "Barcode");
	private ListGridField descriptionField = new ListGridField("desc", "Desc.");
	private ListGridField measurementField = new ListGridField("orderMeasurement", "Measurement");
	private ListGridField qtyField = new ListGridField("qty", "Order Qty");
	
	private RestDataSource poDetailsDataSource;
	private Integer transferId;
	
	@Inject
	public TransferOrderDetailListGrid(
			@Named("TransferOrderDetails") RestDataSource poDetailsDataSource,
			@Named("Positive") FloatRangeValidator positiveFloatRangeValidatorProvider
		){
		
		this.poDetailsDataSource = poDetailsDataSource;
		
		final ListGrid list = this;
		
		setDataSource(poDetailsDataSource);
		setHeaderHeight(44);
				
		skuField.setAlign(Alignment.CENTER);
		skuField.setWidth(65);
		skuField.setCellFormatter(new CellFormatter() {			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				String sku = productRecord.getAttribute("sku");
				list.getRecord(rowNum).setAttribute("sku", sku);
				return sku;
			}
		});		
			
		barcodeField.setAlign(Alignment.CENTER);
		barcodeField.setWidth(90);
		barcodeField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				String barcode = productRecord.getAttribute("barcode");
				list.getRecord(rowNum).setAttribute("barcode", barcode);
				return barcode;
			}
		});

		descriptionField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				return productRecord.getAttribute("longDescription");
			}
		});
				
		measurementField.setAlign(Alignment.CENTER);
		measurementField.setWidth(60);
		measurementField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				
				Record productRecord = record.getAttributeAsRecord("product");
				Record buyingRecord = productRecord.getAttributeAsRecord("buying");
				Record buyingMeasurementRecord = buyingRecord.getAttributeAsRecord("measurement");
				Record productMeasurementRecord = productRecord.getAttributeAsRecord("productMeasurement");
				
				Boolean isMutable = buyingMeasurementRecord.getAttributeAsBoolean("mutable");
				
				Integer measurementQty;
				
				if(!isMutable){
					measurementQty = buyingMeasurementRecord.getAttributeAsInt("defaultQty");
				}else{
					measurementQty = getMeasurementQty(productMeasurementRecord, buyingMeasurementRecord);
				}
								
				return buyingMeasurementRecord.getAttribute("code") + " (" + measurementQty + ")";
			}
		});

		qtyField.setWidth(60);
		qtyField.setAlign(Alignment.RIGHT);
		qtyField.setIncludeInRecordSummary(true);
		qtyField.setShowGridSummary(false);
		qtyField.setType(ListGridFieldType.FLOAT);
		qtyField.setValidators(positiveFloatRangeValidatorProvider);
		qtyField.addCellSavedHandler(new CellSavedHandler() {
			
			public void onCellSaved(CellSavedEvent event) {
				Float newQty = ((Number)event.getNewValue()).floatValue();
				if(newQty.intValue() <= 0){
					int rownum = event.getRowNum();
					deleteRecordInList(rownum);
				}
			}
		});
				
		setHeaderSpans(
				new HeaderSpan("Product", new String[]{"sku", "barcode", "desc"}),
				new HeaderSpan("Order", new String[]{"qty", "orderMeasurement"})
				);
		
		setFields(skuField, barcodeField, descriptionField, qtyField, measurementField);
		
		skuField.setCanEdit(false);
		barcodeField.setCanEdit(false);
		descriptionField.setCanEdit(false);
		measurementField.setCanEdit(false);
		
		qtyField.setCanEdit(true);
				
		setShowGridSummary(true);
		setShowAllRecords(true);
		setModalEditing(true);
		setAnimateRemoveRecord(true);
		
	}
		
	public void setTransferId(Integer transferId) {
		this.transferId = transferId;
	}

	protected void deleteRecordInList(final int rownum) {		
		Record record = getRecord(rownum);
		removeData(record);
	}

	public void setStatus(String status){

	}

	private Integer getMeasurementQty(Record productMeasurementRecord, Record buyingMeasurementRecord){
		
		Record measurementFromPmRecord = productMeasurementRecord.getAttributeAsRecord("measurement");		
		Integer measurementQty = buyingMeasurementRecord.getAttributeAsInt("defaultQty");
		
		if(productMeasurementRecord != null && measurementFromPmRecord != null){
			if(measurementFromPmRecord.getAttributeAsInt("id").intValue() == buyingMeasurementRecord.getAttributeAsInt("id").intValue()){
				measurementQty = productMeasurementRecord.getAttributeAsInt("overrideQty");
			}			
		}
		
		return measurementQty;
	}

	public void addProduct(Integer productId, Float qty){
		Record record = new Record();
		record.setAttribute("qty", qty);
		record.setAttribute("transferId", transferId);
		record.setAttribute("productId", productId);
		final ListGrid list = this;
		poDetailsDataSource.addData(record, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				DeferredCommand.addCommand(new Command() {
					public void execute() {
						list.scrollToRow(list.getTotalRows()-1);
					}
				});
			}
		});
	}

	public int indexOfProductExistInDetail(String text) {
		String propertyToSearch;
		RecordList recordList = getDataAsRecordList();
		int index;
		
		if(text.length() > 10){
			propertyToSearch = "barcode";
		}else{
			propertyToSearch = "sku";
		}
		
		index = recordList.findIndex(propertyToSearch, text);
		
		return index;
	}

	public void productExist(int index) {
		startEditing(index, TransferOrderDetailListGrid.EDITABLE_COLUMN, false);	
	}
	
}
