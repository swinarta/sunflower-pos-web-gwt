package com.swinarta.sunflower.web.gwt.client.sessionobject;

import java.io.Serializable;

import com.smartgwt.client.data.Record;

public class Store implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4880366958934097624L;
	
	private Integer id;
	private String code;
	private String name;
	private Boolean isHq;
	
	public Store(Record r){
		this.code = r.getAttributeAsString("code");
		this.name = r.getAttributeAsString("name");
		this.isHq = r.getAttributeAsBoolean("isHq");
		this.id = r.getAttributeAsInt("id");
	}
	
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public Boolean getIsHq() {
		return isHq;
	}
	public Integer getId() {
		return id;
	}
}