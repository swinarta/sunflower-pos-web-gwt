package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.forms.SupplierForm;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.widgets.layout.HLayout;

public class SupplierBasicCanvas extends DataCanvas{

	private DisplayLabel code = new DisplayLabel("Code");
	private DisplayLabel name = new DisplayLabel("Name");
	private DisplayLabel contact = new DisplayLabel("Contact Name");
	private DisplayLabel termOfPayment = new DisplayLabel("Term Of Payment");
	private DisplayLabel addr1 = new DisplayLabel("Address1");
	private DisplayLabel addr2 = new DisplayLabel("Address2");
	private DisplayLabel city = new DisplayLabel("City");
	private DisplayLabel phone = new DisplayLabel("Phone");
	private DisplayLabel fax = new DisplayLabel("Fax");
	private DisplayLabel mobile = new DisplayLabel("Mobile");
	
	final private SupplierForm supplierForm;
	
	private Record record;
	private DSCallback saveCallback;
	
	@Inject
	public SupplierBasicCanvas(
		SupplierForm supplierForm
		){
		super(DataCanvasLayout.VERTICAL);

		this.supplierForm = supplierForm;
		
		setGroupTitle("Supplier Information");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);		
		setMargin(3);

		setForm(supplierForm);
		
		saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
					addOrUpdateWindow.hide();
				}
			}
		};
		
		supplierForm.setSaveCallback(saveCallback);
		
		HLayout nameLayout = new HLayout(15);
		nameLayout.addMember(code);
		nameLayout.addMember(name);

		HLayout addrLayout = new HLayout(15);
		addrLayout.addMember(addr1);
		addrLayout.addMember(addr2);

		HLayout phoneLayout = new HLayout(15);
		phoneLayout.addMember(phone);
		phoneLayout.addMember(fax);
		phoneLayout.addMember(mobile);

		dataComposite.addMember(nameLayout);
		dataComposite.addMember(termOfPayment);
		dataComposite.addMember(contact);
		dataComposite.addMember(addrLayout);
		dataComposite.addMember(city);
		dataComposite.addMember(phoneLayout);
		dataComposite.addMember(editForm);
	}
	
	public void loadData(Record r) {
		this.record = r;
		
		String codeStr = r.getAttributeAsString("supplierCode");
		String nameStr = r.getAttributeAsString("name");
		String contactStr = r.getAttributeAsString("contactName");
		Integer termOfPaymentInt = r.getAttributeAsInt("termOfPayment");
		String addr1Str = r.getAttributeAsString("address1");
		String addr2Str = r.getAttributeAsString("address2");
		String cityStr = r.getAttributeAsString("city");
		String phoneStr = r.getAttributeAsString("phone");
		String faxStr = r.getAttributeAsString("fax");
		String mobileStr = r.getAttributeAsString("mobile");
		
		code.setContents(codeStr);
		name.setContents(nameStr);
		contact.setContents(contactStr);
		termOfPayment.setContents(termOfPaymentInt);
		addr1.setContents(addr1Str);
		addr2.setContents(addr2Str);
		city.setContents(cityStr);
		phone.setContents(phoneStr);
		fax.setContents(faxStr);
		mobile.setContents(mobileStr);
		
	}

	protected void editLinkClicked(){
		customizeWindow("Edit Supplier", 270, 490);
		supplierForm.editRecord(record);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
	}

	protected void buttonWindowClicked(){
		if(supplierForm.validate(false)){
			supplierForm.saveData();
		}
	}


}
