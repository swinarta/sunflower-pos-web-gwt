package com.swinarta.sunflower.web.gwt.client.panel;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.Layout;

public abstract class BasePanel extends Layout{

	private Canvas viewPanel;
	
	public abstract Canvas getViewPanel();
	
	public void init(){
		
        setWidth100();
        setHeight100();
        
        viewPanel = getViewPanel();
        
        addMember(viewPanel);
	}
	
}
