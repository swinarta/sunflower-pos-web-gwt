package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ProductStatusLayoutProvider implements Provider<Layout>{

	public static int INDEX_NOT_ACTIVE = 0;
	
	private final Label notActiveLabel;
	
	@Inject
	public ProductStatusLayoutProvider(
		@Named("NotActiveProduct") Label notActiveLabel
		){
			this.notActiveLabel = notActiveLabel;
	}
	
	public Layout get() {

		VLayout layout = new VLayout();
		layout.setHeight(20);
		layout.setWidth100();
		layout.setIsGroup(true);
		layout.setGroupTitle("Product Status");
		layout.setAlign(Alignment.CENTER);
		layout.addMember(notActiveLabel);

		return layout;
	}

}
