package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.widgets.Img;

public class ProductImageCanvas extends DataCanvas{

	private Integer productId;
	private Img img;

	@Inject
	public ProductImageCanvas(){

		setGroupTitle("Product Image");
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutBottomMargin(8);
		setLayoutRightMargin(8);
		setMargin(3);
						
		img = new Img();
		img.setSize(150);
		img.setBorder("1px solid gray");
		
		dataComposite.addMember(img);
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public void fetchImage() {
		final String imagePath = "/sunflower/rest/seed/product/image/" + productId;		
		final String imageNotFoundPath = "sunflower/none.gif";

		img.setSrc(imageNotFoundPath);

		RPCRequest rpcReq = new RPCRequest();
		rpcReq.setHttpMethod("GET");
		rpcReq.setActionURL(imagePath);
		rpcReq.setUseSimpleHttp(true);
		rpcReq.setWillHandleError(true);
		RPCManager.sendRequest(rpcReq, new RPCCallback(){
			public void execute(RPCResponse response, Object rawData, RPCRequest request) {
				if (response.getHttpResponseCode()==200) {
					img.setSrc(imagePath);
				}
			}
			
		});
		
	}
		
}
