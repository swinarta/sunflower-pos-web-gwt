package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.forms.StockForm;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.swinarta.sunflower.web.gwt.client.util.DateUtils;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class StockCanvas extends DataCanvas{

	private Record record;
	private Integer productId;
	
	private DisplayLabel current= new DisplayLabel("Current");
	private DisplayLabel max = new DisplayLabel("Max");
	private DisplayLabel min = new DisplayLabel("Min");
	private DisplayLabel std = new DisplayLabel("Std Order");
	private DisplayLabel lastUpdate = new DisplayLabel("Last Update");

	final private StockForm stockForm;
	final private RestDataSource stockDataSource;
	
	private DSCallback saveCallback;
	
	private Store currStore = (Store) ClientSession.objects.get("store");

	@Inject
	public StockCanvas(
			@Named("Stock") RestDataSource stockDataSource,
			StockForm stockForm
			){
		
		this.stockDataSource = stockDataSource;
		this.stockForm = stockForm;
		
		setGroupTitle("Stock Information");
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutBottomMargin(8);
		setLayoutRightMargin(8);
		setMargin(3);
				
		setForm(stockForm);		
		stockForm.setAutoFocus(true);
		
		saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);								
					addOrUpdateWindow.hide();
				}
			}
		};
		
		stockForm.setSaveCallback(saveCallback);		
		
		VLayout stockComposite = new VLayout();
		stockComposite.setGroupTitle(currStore.getCode());
		stockComposite.setIsGroup(true);

		stockComposite.setLayoutTopMargin(8);
		stockComposite.setLayoutLeftMargin(8);
		stockComposite.setLayoutBottomMargin(8);
		stockComposite.setLayoutRightMargin(8);

		HLayout layout1 = new HLayout();
		
		layout1.addMember(current);
		layout1.addMember(max);
		layout1.addMember(min);
		layout1.addMember(std);
		
		HLayout layout2 = new HLayout();
		layout2.addMember(lastUpdate);
		
		stockComposite.addMember(layout1);
		stockComposite.addMember(layout2);
		stockComposite.addMember(editForm);
		
		dataComposite.addMember(stockComposite);
		
	}
	
	private void loadData(Record record){
		this.record = record;
		
		current.setContents(record.getAttributeAsFloat("current"));
		max.setContents(record.getAttributeAsInt("max"));
		min.setContents(record.getAttributeAsInt("min"));
		std.setContents(record.getAttributeAsInt("defaultOrder"));
		
		String lastUpdateStr = record.getAttributeAsString("updatedDt");		
		Date lastUpdateDate = DateUtils.getIsoDateFormat().parse(lastUpdateStr);		
		lastUpdate.setContents(lastUpdateDate);

		loadDataFound();
	}
	
	public void fetchStockData(Integer productId, Integer storeId){
		this.productId = productId;
		
		Criteria criteria = new Criteria();
		criteria.addCriteria("productId", productId);
		criteria.addCriteria("storeId", storeId);
		
		stockDataSource.fetchData(criteria, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				
				if(response.getTotalRows() > 0){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
				}else{
					loadDataNotFound();
				}
				
			}
		});
	}
	
	protected void addLinkClicked(){
		customizeWindow("Add Stock", 200, 250);
		//stockForm.setProductId(productId);
		//stockForm.setStoreId(1);
		Integer storeId = currStore.getId();
		stockForm.editNewRecord(productId, storeId);
		addOrUpdateWindow.show();
	}

	protected void editLinkClicked(){
		customizeWindow("Edit Stock", 200, 250);
		stockForm.clearErrors(true);
		stockForm.editRecord(record);
		addOrUpdateWindow.show();
	}

	protected void buttonWindowClicked(){
		if(stockForm.validate(false)){
			stockForm.saveData();
		}
		
	}

}