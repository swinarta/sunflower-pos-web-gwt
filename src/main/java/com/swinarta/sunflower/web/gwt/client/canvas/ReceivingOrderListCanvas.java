package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrinterChangePresenceEvent;
import com.swinarta.sunflower.web.gwt.client.connectivity.Printer;
import com.swinarta.sunflower.web.gwt.client.connectivity.impl.PrinterImpl;
import com.swinarta.sunflower.web.gwt.client.util.PrinterUtil;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ReceivingOrderDetailListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

public class ReceivingOrderListCanvas extends VLayout{

	final private ReceivingOrderDetailListGrid recevingList;
	final private Printer printer;
	private OnPrinterChangePresenceEvent onPrinterChangePresenceEvent;
	
	@Inject
	public ReceivingOrderListCanvas(
		ReceivingOrderDetailListGrid recevingList,
		Printer printer
		){
		
		this.recevingList = recevingList;
		this.printer = printer;

		setGroupTitle("Receiving Details");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutRightMargin(8);
		setLayoutBottomMargin(8);
		setMargin(3);
		
		addMember(recevingList);
		
	}

	public void loadData(Record record){
		Integer roId = record.getAttributeAsInt("id");
		String status = record.getAttributeAsString("status");
		
		Criteria c = new Criteria();
		c.setAttribute("roId", roId);
		recevingList.fetchData(c);

		loadStatus(status);
		
		onPrinterChangePresenceEvent.changeStatus(PrinterImpl.PRINTER01, printer.getStatus(PrinterImpl.PRINTER01), printer.getStatusString(PrinterImpl.PRINTER01));	

	}
	
	public void loadStatus(String status){
		recevingList.setStatus(status);
	}
	
	public void setOnPrinterChangePresenceEvent(OnPrinterChangePresenceEvent onPrinterChangePresenceEvent) {
		this.onPrinterChangePresenceEvent = onPrinterChangePresenceEvent;
		printer.register(onPrinterChangePresenceEvent);
	}

	public void printBarcode(){
		ListGridRecord[] records = recevingList.getSelection();
		if(records.length > 0){
			Boolean toPrint = false;
			StringBuffer buffer = new StringBuffer();
			buffer.append(PrinterUtil.PRINT_KEYWORD);
			buffer.append(PrinterUtil.SEPARATOR);
			for (ListGridRecord listGridRecord : records) {
				Integer qty = listGridRecord.getAttributeAsInt("qty");
				if(qty > 0){
					toPrint = true;
					Record productRecord = listGridRecord.getAttributeAsRecord("poDetail").getAttributeAsRecord("product");
					Record sellingRecord = productRecord.getAttributeAsRecord("selling");
					Record buyingRecord = productRecord.getAttributeAsRecord("buying");
					Record buyingMeasurementRecord = buyingRecord.getAttributeAsRecord("measurement");
					Record productMeasurementRecord = productRecord.getAttributeAsRecord("productMeasurement");

					Boolean isMutable = buyingMeasurementRecord.getAttributeAsBoolean("mutable");
					
					Integer measurementQty;
					
					if(!isMutable){
						measurementQty = buyingMeasurementRecord.getAttributeAsInt("defaultQty");
					}else{
						measurementQty = getMeasurementQty(productMeasurementRecord, buyingMeasurementRecord);
					}
					
					String keyword = PrinterUtil.constructPrinterActionStrIndividual(IConstants.PRINT_MODE_BARCODE_WITH_PRICE, productRecord.getAttribute("barcode"), qty*measurementQty, productRecord.getAttribute("shortDescription"), sellingRecord.getAttributeAsDouble("sellingPrice"));
					buffer.append(keyword);
					buffer.append(PrinterUtil.PROD_SEPARATOR);
				}
			}
			if(toPrint){
				printer.sendPrintMessage(PrinterImpl.PRINTER01, buffer.toString());
			}
			SC.say("PRINTING SUCCESS (" + records.length + " PRODUCTS)");
		}else{
			SC.say("NO PRODUCT HAS BEEN SELECTED FOR PRINTING");
		}
	
	}

	private Integer getMeasurementQty(Record productMeasurementRecord, Record buyingMeasurementRecord){
		
		Record measurementFromPmRecord = productMeasurementRecord.getAttributeAsRecord("measurement");		
		Integer measurementQty = buyingMeasurementRecord.getAttributeAsInt("defaultQty");
		
		if(productMeasurementRecord != null && measurementFromPmRecord != null){
			if(measurementFromPmRecord.getAttributeAsInt("id").intValue() == buyingMeasurementRecord.getAttributeAsInt("id").intValue()){
				measurementQty = productMeasurementRecord.getAttributeAsInt("overrideQty");
			}			
		}
		
		return measurementQty;
	}

}
