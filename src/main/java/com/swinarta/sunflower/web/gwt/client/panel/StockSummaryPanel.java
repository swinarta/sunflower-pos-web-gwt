package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductWithStockListGrid;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class StockSummaryPanel extends BasePanel{

	private static final String DESCRIPTION = "stocksummary";
	
    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
            id = DESCRIPTION;
            return injector.getStockSummaryPanel();
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }
	
    private DynamicForm searchForm;
    private TextItem searchProductTextItem;
    private ButtonItem searchButton;
    
    final private ProductWithStockListGrid productListGrid;
    final private CustomComboBoxItem supplierComboBoxItem;
    final private SelectItem stockModeSelectItem;
        
    @Inject
    public StockSummaryPanel(
    	@Named("SupplierAny") CustomComboBoxItem supplierComboBoxItem,
    	@Named("StockMode") SelectItem stockModeSelectItem,
    	ProductWithStockListGrid productListGrid
    	){
    	
    	this.productListGrid = productListGrid;
    	this.supplierComboBoxItem = supplierComboBoxItem;
    	this.stockModeSelectItem = stockModeSelectItem;
    	
    	init();
    	initComponent();
    }
    
	@Override
	public Canvas getViewPanel() {
		
		searchForm = new DynamicForm();
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);

		HiddenItem searchActionId = new HiddenItem("actionId");
		searchActionId.setValue("searchProduct");
		
		searchProductTextItem = new TextItem("searchProduct");  
		searchProductTextItem.setTitle("Search Product");  
		searchProductTextItem.setSelectOnFocus(true);
		searchProductTextItem.setWrapTitle(false);
		searchProductTextItem.setCharacterCasing(CharacterCasing.UPPER);
		
		searchButton = new ButtonItem("searchProduct", "Search");
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);
		
		searchForm.setFields(supplierComboBoxItem, stockModeSelectItem, searchButton, searchActionId);

		productListGrid.setWidth(600);
		productListGrid.setHeight(600);
		
		productListGrid.hide();
		
		initComponentHandler();
		
		VLayout layout = new VLayout(10);
		layout.setBorder("1px solid gray");
		layout.setLayoutTopMargin(20);
		layout.setLayoutLeftMargin(20);
		layout.setLayoutBottomMargin(20);
		
		layout.addMember(searchForm);
		layout.addMember(productListGrid);
		return layout;
		
	}

	private void initComponentHandler(){
		
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchProduct();
				}  
		});
		searchProductTextItem.addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					searchProduct();
				}				
			}
		});
		
	}

	private void initComponent(){

	}

	private void searchProduct(){		
		Integer supplierId = FormUtil.getIntegerValueFromFormItem(supplierComboBoxItem);
		Integer stockModeId = FormUtil.getIntegerValueFromFormItem(stockModeSelectItem);
		
		if(supplierId >= 0 || (stockModeId != null && stockModeId > 0)){
			productListGrid.show();
			productListGrid.invalidateCache();
			productListGrid.fetchData(searchForm.getValuesAsCriteria());
			searchForm.focus();	
		}
		
	}
}