package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import java.util.LinkedHashMap;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.SelectItem;

public class StockModeSelectItemProvider implements Provider<SelectItem>{
	
	public SelectItem get() {
		
		SelectItem selectItem = new SelectItem();
		
		selectItem.setName("stockMode");
		selectItem.setTitle("Stock Mode");
		selectItem.setShowAllOptions(true);
		selectItem.setAnimatePickList(true);
		selectItem.setAutoFetchData(false);
		
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("0", "");
		valueMap.put("1","LOW STOCK");
		valueMap.put("2","OVER STOCK");
		selectItem.setValueMap(valueMap);
				
		return selectItem;
	}

}
