package com.swinarta.sunflower.web.gwt.client.widget;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.ExplorerTreeNode;
import com.swinarta.sunflower.web.gwt.client.MenuData;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.canvas.MainCanvas;
import com.smartgwt.client.types.SortArrow;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.LeafClickEvent;
import com.smartgwt.client.widgets.tree.events.LeafClickHandler;

public class SideNavTree extends TreeGrid{

    private String idSuffix = "";
    private ExplorerTreeNode[] menuData = MenuData.getData();
    
    private Canvas mainCanvas;
    private String currentId;

    @Inject
    public SideNavTree(MainCanvas mainCanvas) {
    	this.mainCanvas = mainCanvas;
    	
        setWidth100();
        setHeight100();
        setCustomIconProperty("icon");
        setAnimateFolderTime(100);
        setAnimateFolders(true);
        setAnimateFolderSpeed(1000);
        setNodeIcon("sunflower/application_view_list.png");
        setShowSortArrow(SortArrow.CORNER);
        setShowAllRecords(true);
        setLoadDataOnDemand(false);
        setCanSort(false);
        
        TreeGridField field = new TreeGridField();
        field.setCanFilter(true);
        field.setName("name");
        field.setTitle("<b>Menu</b>");
        setFields(field);

        Tree tree = new Tree();
        tree.setModelType(TreeModelType.PARENT);
        tree.setNameProperty("name");
        tree.setOpenProperty("isOpen");
        tree.setIdField("nodeID");
        tree.setParentIdField("parentNodeID");
        tree.setRootValue("root" + idSuffix);

        tree.setData(menuData);

        setData(tree);
        
        addLeafClickHandler(new LeafClickHandler() {
            public void onLeafClick(LeafClickEvent event) {
                TreeNode node = event.getLeaf();
                showPage(node);
            }
        });
        
    }
    
    public String getCurrentId() {
		return currentId;
	}

	private void showPage(TreeNode node){
  	  ExplorerTreeNode explorerTreeNode = (ExplorerTreeNode) node;
  	  PanelFactory factory = explorerTreeNode.getFactory();
  	  
        if (factory != null) {
            //String panelID = factory.getID();
            Canvas panel = factory.create();
            
            Canvas[] children = mainCanvas.getChildren();
            
            for (Canvas c : children) {
            	mainCanvas.removeChild(c);
            }

            currentId = factory.getID();
            mainCanvas.addChild(panel);
        }	  
    }
	
    public void showPage(String description){    	
    	for (ExplorerTreeNode explorerTreeNode : menuData) {
			if(explorerTreeNode.getNodeID().equalsIgnoreCase(description)){
				showPage(explorerTreeNode);
			}
		}
    	   	  
      }

}
