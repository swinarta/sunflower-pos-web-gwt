package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

public interface AddProductToDetail{

	public void addProduct(Integer productId, Float qty);
	public int indexOfProductExistInDetail(String text);
	public void productExist(int index);
	
}
