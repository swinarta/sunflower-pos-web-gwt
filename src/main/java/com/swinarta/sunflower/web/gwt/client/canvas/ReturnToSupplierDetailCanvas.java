package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.VLayout;

public class ReturnToSupplierDetailCanvas extends VLayout{

	final private ReturnToSupplierBasicCanvas returnBasicCanvas;
	final private ReturnToSupplierListCanvas returnListCanvas;
		
	@Inject
	public ReturnToSupplierDetailCanvas(
		final ReturnToSupplierBasicCanvas poBasicCanvas,
		final ReturnToSupplierListCanvas returnListCanvas
		){
		
		this.returnBasicCanvas = poBasicCanvas;
		this.returnListCanvas = returnListCanvas;
		
		setCanSelectText(true);

		poBasicCanvas.setHeight(125);
		
		returnListCanvas.setParentLayout(this);
		
		addMember(poBasicCanvas);
		addMember(returnListCanvas);
		
		poBasicCanvas.setListGrid(returnListCanvas.getReturnList());
		poBasicCanvas.setUpdateStatusCallback(new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record record = response.getData()[0];
				String status = record.getAttributeAsString("status");
				returnListCanvas.setStatus(status);
			}
		});
		

	}
	
	public void loadBasicData(Record record){
		
		Record suppRecord = record.getAttributeAsRecord("supplier");
		
		Integer retId = record.getAttributeAsInt("id");
		String status = record.getAttributeAsString("status");
		Integer supplierId = suppRecord.getAttributeAsInt("id");
		
		returnBasicCanvas.loadData(record);
		returnListCanvas.loadData(retId);
		returnListCanvas.setStatus(status);
		returnListCanvas.setSupplierId(supplierId);
	}
	
}