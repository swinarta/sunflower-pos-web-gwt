package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.Button;

public class SaveButtonProvider implements Provider<Button>{

	public Button get() {
		Button button = new Button("Save");
		button.setWidth100();
		return button;
	}

}
