package com.swinarta.sunflower.web.gwt.client.injector.module;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.swinarta.sunflower.web.gwt.client.connectivity.Printer;
import com.swinarta.sunflower.web.gwt.client.connectivity.XmppClient;
import com.swinarta.sunflower.web.gwt.client.connectivity.impl.PrinterImpl;
import com.swinarta.sunflower.web.gwt.client.connectivity.impl.XmppClientImpl;

public class ConnectivityModule extends AbstractGinModule{

	@Override
	protected void configure() {
		bind(XmppClient.class).to(XmppClientImpl.class).in(Singleton.class);
		bind(Printer.class).to(PrinterImpl.class).in(Singleton.class);
	}

}
