package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.forms.SellingForm;
import com.swinarta.sunflower.web.gwt.client.model.ClientDisplayProductMeasurement;
import com.swinarta.sunflower.web.gwt.client.util.PriceUtil;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.rpc.RPCResponse;

public class SellingCanvas extends DataCanvas{

	private DisplayLabel measurement = new DisplayLabel("Measurement");
	private DisplayLabel sellingMargin = new DisplayLabel("Selling Margin");
	private DisplayLabel sellingPriceLabel = new DisplayLabel("Selling Price", "sellingPriceLabel");
	private DisplayLabel costPricePerUnitLabel = new DisplayLabel("Cost Price per Unit");
	private DisplayLabel sellingPricePerUnit = new DisplayLabel("Selling Price per Unit");
	
	private Map<Integer, Integer> measurementOverrideMap;
	private Record record;
	private Integer productId;
	private Double costPricePerUnit;
	
	final private SellingForm sellingForm;
	final private RestDataSource sellingDataSource;
	
	private DSCallback saveCallback;

	@Inject
	public SellingCanvas(
			SellingForm sellingForm,
			@Named("Selling") RestDataSource sellingDataSource
			){
		super(DataCanvasLayout.VERTICAL);

		this.sellingForm = sellingForm;
		this.sellingDataSource = sellingDataSource;
		
		setGroupTitle("Selling Information");
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setMargin(3);
		
		setForm(sellingForm);
		
		saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);						
					addOrUpdateWindow.hide();
				}
			}
		};
		
		sellingForm.setSaveCallback(saveCallback);
		
		dataComposite.addMember(costPricePerUnitLabel);
		dataComposite.addMember(measurement);
		dataComposite.addMember(sellingMargin);
		dataComposite.addMember(sellingPricePerUnit);		
		dataComposite.addMember(sellingPriceLabel);
		dataComposite.addMember(editForm);

	}
	
	public void setMeasurementOverrideMap(Map<Integer, Integer> measurementOverrideMap) {
		this.measurementOverrideMap = measurementOverrideMap;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public void setMeasurementOverrideCMap(Map<Integer, ClientDisplayProductMeasurement> measurementOverrideCMap) {
		sellingForm.setMeasurementOverrideMap(measurementOverrideCMap);
	}
	
	@SuppressWarnings("unchecked")
	private void loadData(Record r){
		this.record = r;
		
		Map<String, Object> measurementMap = record.getAttributeAsMap("measurement");
		Integer measurementQty = getMeasurementQty(measurementMap, measurementOverrideMap);
		String measurementStr = convertMeasurement(measurementMap, measurementOverrideMap);
		
		Double sellingPriceDbl = r.getAttributeAsDouble("sellingPrice");
		Double sellingPricePerUnitDbl = sellingPriceDbl/measurementQty;
		
		sellingPriceLabel.setContents(sellingPriceDbl);
		sellingPricePerUnit.setContents(sellingPricePerUnitDbl);
		
		measurement.setContents(measurementStr);
		
		Map<String, Object> attrs = new HashMap<String, Object>();
		attrs.put("sellingPrice", sellingPriceDbl);
		attrs.put("sellingPricePerUnit", sellingPricePerUnitDbl);
		
		loadDataFound(onCompleteCallback, attrs);
		
	}
	
	public void fetchSellingData(final onCompleteCallback callback){
		Criteria criteria = new Criteria();
		criteria.addCriteria("id", productId);
		
		sellingDataSource.fetchData(criteria, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getTotalRows() > 0){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
				}else{
					loadDataNotFound();
				}
			}
		});
				
	}
	
	public void setCostPrice(Double costPrice){
		this.costPricePerUnit = costPrice;
		costPricePerUnitLabel.setContents(costPrice);
		sellingForm.setCostPricePerUnit(costPrice);
	}
	
	public void setSellingPrice(Double sellingPrice){
		Double margin = PriceUtil.getMargin(costPricePerUnit, sellingPrice);		
		sellingMargin.setContentsInPercent(margin);
	}
	
	protected void editLinkClicked(){
		customizeWindow("Edit Selling", 200, 275);
		sellingForm.clearErrors(true);
		sellingForm.editRecord(record);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
		sellingForm.focusInItem("sellingPrice");
	}

	protected void addLinkClicked(){
		customizeWindow("Add Selling", 200, 275);
		sellingForm.clearErrors(true);
		sellingForm.editNewRecord(productId);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
	}

	protected void buttonWindowClicked(){
		if(sellingForm.validate(false)){
			sellingForm.saveData();		
		}
	}

	private String convertMeasurement(Map<String, Object> measurementMap, Map<Integer, Integer> measurementOverrideMap){
		StringBuffer retval = new StringBuffer();
		String measurementDesc = (String)measurementMap.get("description");
		Integer measurementDefaultQty = getMeasurementQty(measurementMap, measurementOverrideMap);
		
		retval.append(measurementDesc);
		retval.append(" (");
		retval.append(measurementDefaultQty);
		retval.append(")");
		return retval.toString();
	}
	
	private Integer getMeasurementQty(Map<String, Object> measurementMap, Map<Integer, Integer> measurementOverrideMap){
		Integer measurementDefaultQty = (Integer) measurementMap.get("defaultQty");
		Integer measurementId = (Integer) measurementMap.get("id");
		if(measurementOverrideMap.size() > 0 && measurementOverrideMap.get(measurementId) != null){
			measurementDefaultQty = measurementOverrideMap.get(measurementId);
		}
		
		return measurementDefaultQty;
	}
	
}