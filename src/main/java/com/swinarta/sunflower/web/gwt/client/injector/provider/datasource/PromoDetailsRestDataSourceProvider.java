package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;

public class PromoDetailsRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	final private OperationBinding addOperationBinding;
	final private OperationBinding removeOperationBinding;

	@Inject
	public PromoDetailsRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Remove") OperationBinding removeOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;
		this.addOperationBinding = addOperationBinding;
		this.removeOperationBinding = removeOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource() {			
			
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.FETCH){
					int promoId = JSOHelper.getAttributeAsInt(dsRequest.getData(), "promoId");
					dsRequest.setActionURL(getFetchDataURL()+promoId);
				}else if(dsRequest.getOperationType() == DSOperationType.REMOVE){
					int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
					dsRequest.setActionURL(getRemoveDataURL()+id);
				}
				return super.transformRequest(dsRequest);
			}
			
		};
		
		ds.setFetchDataURL("/promodetails/");
		ds.setAddDataURL("/promodetails/");
		ds.setRemoveDataURL("/promodetail/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, 
				addOperationBinding, removeOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
				
		ds.setFields(idTextField);
		
		return ds;
	}

}