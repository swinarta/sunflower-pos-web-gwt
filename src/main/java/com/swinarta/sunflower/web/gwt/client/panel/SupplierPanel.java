package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.callback.OnStoreLoadedCompleteEvent;
import com.swinarta.sunflower.web.gwt.client.forms.SupplierForm;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.SupplierListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.SupplierTabSet;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class SupplierPanel extends BasePanel implements OnStoreLoadedCompleteEvent{

	private static final String DESCRIPTION = "supplier";
	
    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
        	SupplierPanel panel = injector.getSupplierPanel();
            id = DESCRIPTION;
            return panel;
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }
	
    private DynamicForm addSupplierForm;
    private DynamicForm searchForm;
    private TextItem supplierTextItem;
    private ButtonItem searchButton;
    
    final private LinkItem addSupplierLinkItem;
    final private Window addSupplierWindow;
    final private Tab searchSupplierTab;
    final private SupplierTabSet tabSet;
    final private SupplierListGrid supplierGrid;
    final private SupplierForm supplierForm;
    final private Button saveSupplierButton;
    
    @Inject
    public SupplierPanel(
    	@Named("AddSupplier") LinkItem addSupplierLinkItem,
    	@Named("AddSupplier") Window addSupplierWindow,
    	@Named("SaveInForm") Button saveSupplierButton,
    	SupplierTabSet tabSet,
    	SupplierListGrid supplierGrid,
    	@Named("SearchSupplier") Tab searchSupplierTab,
    	SupplierForm supplierForm
    	){
    	
    	this.addSupplierLinkItem = addSupplierLinkItem;
    	this.addSupplierWindow = addSupplierWindow;
    	this.saveSupplierButton = saveSupplierButton;
    	this.searchSupplierTab = searchSupplierTab;
    	this.tabSet = tabSet;
    	this.supplierGrid = supplierGrid;
    	this.supplierForm = supplierForm;
    	    	
    	init();
    	initComponent();
		initComponentHandler();    	
    }
    
	@Override
	public Canvas getViewPanel() {
		
		addSupplierForm = new DynamicForm();
		addSupplierForm.setWidth(400);
		addSupplierForm.setFields(addSupplierLinkItem);
		
		searchForm = new DynamicForm();
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);

		HiddenItem searchActionId = new HiddenItem("actionId");
		searchActionId.setValue("searchSupplier");
		
		supplierTextItem = new TextItem("searchSupplier");  
		supplierTextItem.setTitle("Search Supplier");  
		supplierTextItem.setSelectOnFocus(true);
		supplierTextItem.setWrapTitle(false);
		supplierTextItem.setCharacterCasing(CharacterCasing.UPPER);
		
		searchButton = new ButtonItem("searchSupplier", "Search");
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);
		
		searchForm.setFields(supplierTextItem, searchButton, searchActionId);

		supplierGrid.setWidth(600);
		supplierGrid.setHeight(600);		
				
		supplierGrid.hide();
				
		VLayout c = new VLayout();
		c.addMember(addSupplierForm);
		c.addMember(searchForm);
		c.addMember(supplierGrid);
				
		c.setLayoutTopMargin(10);
		c.setLayoutLeftMargin(25);

		searchSupplierTab.setPane(c);
		
		tabSet.addTab(searchSupplierTab);
		return tabSet;
	}

	private void initComponentHandler(){

		addSupplierLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {				
				addSupplierWindow.centerInPage();
				addSupplierWindow.show();

				supplierForm.editNewRecord();
				supplierForm.focus();
			}
		});
		
		supplierGrid.addDoubleClickHandler(new DoubleClickHandler() {

			public void onDoubleClick(DoubleClickEvent event) {
				SupplierListGrid listGrid = (SupplierListGrid)event.getSource();				
				tabSet.loadSupplier(listGrid.getSelectedRecord());
			}
		});

		final DSCallback saveCallback = new DSCallback() {			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				tabSet.loadSupplier(response.getData()[0]);
				addSupplierWindow.hide();				
			}
		};
		
    	supplierForm.setSaveCallback(saveCallback);
				
		saveSupplierButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				if(supplierForm.validate()){
					supplierForm.saveData(saveCallback);
				}				
			}
		});
		
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchSupplier();
				}  
		});
		supplierTextItem.addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					searchSupplier();
				}				
			}
		});
		
	}
	
	private void initComponent(){
		addSupplierWindow.setTitle("Add Supplier");
		addSupplierWindow.setWidth(270);
		addSupplierWindow.setHeight(490);
		
		addSupplierWindow.addItem(supplierForm);
		addSupplierWindow.addItem(saveSupplierButton);
	}

	private void searchSupplier(){
		String text = searchForm.getValueAsString("searchSupplier");
		
		if((text != null && text.length() > 1)){
			supplierGrid.show();
			supplierGrid.fetchData(searchForm.getValuesAsCriteria());
		}
		searchForm.focus();
	}
	
    public void changeHistoryState(String token){
      	 
        if(token.startsWith("supplier")){
        	String idStr = token.substring("supplier".length()+1, token.length());
        	try{
        		String id = String.valueOf(Integer.parseInt(idStr));
            	tabSet.selectTab(id);
        	}catch (Exception e) {
        		tabSet.selectTab(0);
			}
        	
        }
    }

	public void performOnStoreLoaded(Store store) {
		if(store != null && store.getIsHq()){
			addSupplierLinkItem.show();
		}else{
			addSupplierLinkItem.hide();
		}
	}	

}
