package com.swinarta.sunflower.web.gwt.client.callback;

public interface OnPrinterChangePresenceEvent {
	public void changeStatus(String id, Boolean isAvailable, String statusStr);
}
