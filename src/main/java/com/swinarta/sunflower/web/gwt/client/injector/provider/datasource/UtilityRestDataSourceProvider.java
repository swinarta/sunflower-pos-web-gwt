package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;

public class UtilityRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	
	@Inject
	public UtilityRestDataSourceProvider(
		@Named("GetFetch") OperationBinding fetchOperationBinding
		){
			this.fetchOperationBinding = fetchOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/util/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding);
		
		DataSourceTextField keyTextField = new DataSourceTextField("value");
		
		ds.setFields(keyTextField);
		
		return ds;
	}

}