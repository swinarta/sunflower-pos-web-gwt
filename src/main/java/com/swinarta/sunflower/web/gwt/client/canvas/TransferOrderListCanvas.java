package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.swinarta.sunflower.web.gwt.client.widget.AddProductToDetailWindow;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransferOrderDetailListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
 
public class TransferOrderListCanvas extends VLayout{

	private Store currentStore = (Store) ClientSession.objects.get("store");

	final private DynamicForm addProductToDetailForm = new DynamicForm();
		
	final private TransferOrderDetailListGrid orderList;
	final private AddProductToDetailWindow addProductWindow;
	final private LinkItem addProductLinkItem;
	
	@Inject
	public TransferOrderListCanvas(
			final TransferOrderDetailListGrid orderList,
			@Named("AddProduct") LinkItem addProductLinkItem,
			AddProductToDetailWindow addProductWindow
			){
		
		this.orderList = orderList;
		this.addProductWindow = addProductWindow;
		this.addProductLinkItem = addProductLinkItem;
		
		setGroupTitle("Order Details");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutRightMargin(8);
		setLayoutBottomMargin(8);
		setMargin(3);		
					
		addProductToDetailForm.setFields(addProductLinkItem);
		
		addProductWindow.setAddProductToDetail(orderList);
		addProductWindow.setShowQty(true);
		addProductWindow.hide();
		
		addMember(addProductToDetailForm);
		addMember(orderList);
				
		initHandler();
	}
	
	public void setParentLayout(Layout parent){
		parent.addChild(addProductWindow);
	}
	
	private void initHandler(){
		addProductLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				addProductWindow.centerInPage();
				addProductWindow.show();
			}
		});
	}

	public void loadData(Integer id){
		Criteria c = new Criteria();
		c.setAttribute("transferId", id);
		orderList.fetchData(c);
		orderList.setTransferId(id);
	}

	public void setStatus(String status){
		orderList.setStatus(status);
		orderList.setCanEdit("NEW".equalsIgnoreCase(status) && currentStore.getIsHq());
		if("NEW".equalsIgnoreCase(status) && currentStore.getIsHq()){
			addProductToDetailForm.show();
		}else{
			addProductToDetailForm.hide();
		}
	}
		
	public TransferOrderDetailListGrid getOrderList() {
		return orderList;
	}
}