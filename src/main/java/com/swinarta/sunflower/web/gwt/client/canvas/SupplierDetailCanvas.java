package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class SupplierDetailCanvas extends VLayout{

	final private SupplierBasicCanvas supplierBasicCanvas;
	private Store currentStore = (Store) ClientSession.objects.get("store");
	
	@Inject
	public SupplierDetailCanvas(
		SupplierBasicCanvas supplierBasicCanvas
		){
		
		this.supplierBasicCanvas = supplierBasicCanvas;
		
		setCanSelectText(true);

		HLayout basicLayout = new HLayout();
		basicLayout.setHeight(300);
		basicLayout.addMember(supplierBasicCanvas);

		addMember(basicLayout);
		
		supplierBasicCanvas.setShowEditLink(currentStore.getIsHq());

	}
	
	public void fetchData(Record record){
		supplierBasicCanvas.loadData(record);
	}
	
/*	public void fetchData(final Integer suppId){
		supplierBasicCanvas.fetchSupplierData(suppId);
	}
*/	
}
