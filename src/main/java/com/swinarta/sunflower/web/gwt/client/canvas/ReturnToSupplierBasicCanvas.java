package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.forms.ReturnSupplierForm;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLink;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;

public class ReturnToSupplierBasicCanvas extends DataCanvas{

	private DisplayLabel poCode = new DisplayLabel("Return Code");
	private DisplayLabel status = new DisplayLabel("Status");
	private DisplayLabel poDate = new DisplayLabel("Return Date");
	private DisplayLabel remarks = new DisplayLabel("Remarks");
	private DisplayLink supplier = new DisplayLink("Supplier");
	private DisplayLabel invoiceNumber = new DisplayLabel("Invoice Number");
		
	final private Window supplierWindow;
	final private ReturnSupplierForm retForm;
	final private SupplierBasicCanvas suppCanvas;
	final private LinkItem processLinkItem;
	final private LinkItem cancelLinkItem;
	final private LinkItem downloadAsPdfLinkItem;
	final private LinkItem downloadAsTextLinkItem;
	
	private Record record;
	private ListGrid listGrid;
	private DSCallback updateStatusCallback;
	private DSCallback cancelCallBack;
	private DSCallback completeCallBack;
	
	@Inject
	public ReturnToSupplierBasicCanvas(
			SupplierBasicCanvas suppCanvas,
			ReturnSupplierForm retForm,
			@Named("DisplaySupplier") final Window supplierWindow,
			@Named("CompleteReceivingOrder") LinkItem completeLinkItem,
			@Named("CancelPurchasingOrder") LinkItem cancelLinkItem,
			@Named("DownloadAsPdf") LinkItem downloadAsPdfLinkItem,
			@Named("DownloadAsText") LinkItem downloadAsTextLinkItem
		){
		super(DataCanvasLayout.VERTICAL);
		
		this.retForm = retForm;
		this.supplierWindow = supplierWindow;
		this.suppCanvas = suppCanvas;
		this.processLinkItem = completeLinkItem;
		this.cancelLinkItem = cancelLinkItem;
		this.downloadAsPdfLinkItem = downloadAsPdfLinkItem;
		this.downloadAsTextLinkItem = downloadAsTextLinkItem;

		setGroupTitle("Return To Supplier Information");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setMargin(3);
				
		setForm(retForm);
		
		final DSCallback saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
					addOrUpdateWindow.hide();
					updateStatusCallback.execute(response, rawData, request);
				}
				
			}
		};
		
		cancelCallBack = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Return To Supplier is successfuly CANCELLED");
				saveCallback.execute(response, rawData, request);
			}
		};
		
		completeCallBack = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Return To Supplier is successfuly COMPLETED");
				saveCallback.execute(response, rawData, request);
			}
		};

		suppCanvas.setIsGroup(false);
		suppCanvas.setShowAddLink(false);
		suppCanvas.setShowEditLink(false);
		
		retForm.setSaveCallback(saveCallback);

		HLayout poLayout = new HLayout(15);
		poLayout.addMember(poCode);
		poLayout.addMember(status);

		HLayout termLayout = new HLayout(15);
		termLayout.addMember(poDate);
		termLayout.addMember(invoiceNumber);		

		HLayout suppRemarkLayout = new HLayout(15);
		suppRemarkLayout.addMember(supplier);
		suppRemarkLayout.addMember(remarks);

		editForm.setWidth(500);
		editForm.setColWidths(100,100,100,100,100);
		editForm.setNumCols(5);
		editForm.setFields(editItemLink, completeLinkItem, cancelLinkItem, downloadAsPdfLinkItem, downloadAsTextLinkItem);

		dataComposite.addMember(poLayout);
		dataComposite.addMember(termLayout);
		dataComposite.addMember(suppRemarkLayout);
		dataComposite.addMember(editForm);
		
		initWindow();
		initHandler();
	}

	public void setUpdateStatusCallback(DSCallback updateStatusCallback) {
		this.updateStatusCallback = updateStatusCallback;
	}

	public void setListGrid(ListGrid listGrid) {
		this.listGrid = listGrid;
	}

	private void initHandler(){
		supplier.addClickHandlerOverride(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				supplierWindow.centerInPage();
				supplierWindow.show();
			}
		});
		processLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				completeLinkClicked();
			}
		});
		cancelLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				cancelLinkClicked();
			}
		});
	}
	
	private void initWindow(){
		supplierWindow.setWidth(400);
		supplierWindow.setHeight(300);
		supplierWindow.addItem(suppCanvas);
	}
	
	public void loadData(Record r) {
		this.record = r;
		
		String statusStr = r.getAttributeAsString("status");
		
		poCode.setContents(r.getAttributeAsString("returnId"));
		status.setContents(statusStr);
		poDate.setContents(r.getAttributeAsDate("returnDate"), true);
		remarks.setContents(r.getAttributeAsString("remarks"));
		invoiceNumber.setContents(r.getAttributeAsString("invoiceNumber"));

		Record supplierRecord = r.getAttributeAsRecord("supplier");
		supplier.setContents(supplierRecord.getAttribute("name"));
		suppCanvas.loadData(supplierRecord);
		supplierWindow.setTitle(supplierRecord.getAttribute("name"));
		
		if("NEW".equalsIgnoreCase(statusStr)){
			processLinkItem.show();
			cancelLinkItem.show();
			editItemLink.show();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			status.setContentStyle("poNewLabel");
		}else if("COMPLETED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.hide();
			editItemLink.hide();
			downloadAsPdfLinkItem.show();
			downloadAsTextLinkItem.show();
			
			Integer id = r.getAttributeAsInt("id");
			downloadAsPdfLinkItem.setValue(IConstants.REST_DS + "/report/return/" + id + "/pdf");
			downloadAsTextLinkItem.setValue(IConstants.REST_DS + "/report/return/" + id + "/txt");
			
			status.setContentStyle("poCompletedLabel");
		}else if("CANCELLED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.hide();
			editItemLink.hide();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			status.setContentStyle("poCancelledLabel");			
		}
	}
	
	private void completeLinkClicked(){
				
		if(listGrid.getTotalRows() > 0){
			record.setAttribute("status", "COMPLETED");
			retForm.editRecord(record);
			if(!retForm.validate(false)){
				@SuppressWarnings("unchecked")
				Map<String, String> errors = (Map<String, String>)retForm.getErrors();
				String str = "";
				for(Map.Entry<String, String> e : errors.entrySet()){							
				    str = str + retForm.getItem(e.getKey()).getTitle()+": "+e.getValue() + "<br/>";
				}
				SC.warn(str);
			}else{
				SC.ask("Complete Return To Supplier?", new BooleanCallback() {			
					public void execute(Boolean value) {
						if(value){
							retForm.editRecord(record);
							retForm.saveData(completeCallBack);
						}
					}
				});
			}			
		}else{
			SC.warn("Return Product not found");
		}
		
	}

	private void cancelLinkClicked(){
		SC.ask("Cancel Return To Supplier?", new BooleanCallback() {
			
			public void execute(Boolean value) {
				if(value){
					record.setAttribute("status", "CANCELLED");					
					retForm.editRecord(record);
					retForm.saveData(cancelCallBack);
				}
			}
		});
	}

	protected void editLinkClicked(){
		customizeWindow("Edit Return To Supplier", 250, 300);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
		retForm.editRecord(record);
	}

	protected void buttonWindowClicked(){
		if(retForm.validate(false)){
			retForm.saveData();
		}
	}

}