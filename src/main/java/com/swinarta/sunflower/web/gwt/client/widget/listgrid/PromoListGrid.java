package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.DateUtils;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class PromoListGrid extends ListGrid{
	
	@Inject
	public PromoListGrid(
		@Named("Promos") RestDataSource promoDataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setAlternateRecordStyles(true);
		setShowAllRecords(false);
		setDataSource(promoDataSource);
		setCanGroupBy(false);
		setCanFreezeFields(false);
		setCanEdit(false);

		ListGridField idField = new ListGridField("id");
		ListGridField startDateField = new ListGridField("startDate");
		ListGridField endDateField = new ListGridField("endDate");
		ListGridField descriptionField = new ListGridField("description");		
		ListGridField promoTypeField = new ListGridField("promoType", "Promo Type");
		ListGridField promoValueField = new ListGridField("promoValue");
		
		promoValueField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return StringFormatUtil.getFormat(record.getAttributeAsDouble("promoValue"));
			}
		});
		
		
		idField.setWidth(30);
		startDateField.setWidth(100);
		endDateField.setWidth(100);
		promoTypeField.setWidth(100);
		promoValueField.setWidth(100);
		descriptionField.setWidth(150);
		
		startDateField.setAlign(Alignment.CENTER);
		endDateField.setAlign(Alignment.CENTER);
		promoTypeField.setAlign(Alignment.CENTER);
		
		setFields(idField, startDateField, endDateField, 
				promoTypeField, promoValueField, descriptionField);
		
	}

	@Override
	protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {

		Date today = new Date();
		Date startDate = record.getAttributeAsDate("startDate");
		
		DateUtils.resetTime(today);
		DateUtils.resetTime(startDate);
				
		if(startDate.compareTo(today) <= 0){
			return "color:blue;";
		}else{
			return super.getCellCSSText(record, rowNum, colNum);
		}		
	}

	
}
