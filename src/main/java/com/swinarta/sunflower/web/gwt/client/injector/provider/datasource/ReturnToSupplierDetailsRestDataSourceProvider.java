package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;

public class ReturnToSupplierDetailsRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	final private OperationBinding updateOperationBinding;
	final private OperationBinding addOperationBinding;
	final private OperationBinding removeOperationBinding;

	@Inject
	public ReturnToSupplierDetailsRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding,
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Remove") OperationBinding removeOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
		this.addOperationBinding = addOperationBinding;
		this.removeOperationBinding = removeOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource() {			
			
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.UPDATE || dsRequest.getOperationType() == DSOperationType.REMOVE){
					int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
					dsRequest.setActionURL(getUpdateDataURL()+id);
					//dsRequest.setHttpMethod("PUT");
				}
				return super.transformRequest(dsRequest);
			}
			
		};
		
		ds.setFetchDataURL("/retsuppdetails/");
		ds.setAddDataURL("/retsuppdetails/");
		ds.setUpdateDataURL("/retsuppdetail/");
		ds.setRemoveDataURL("/retsuppdetail/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, updateOperationBinding, 
				addOperationBinding, removeOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
				
		ds.setFields(idTextField);
		
		return ds;
	}

}