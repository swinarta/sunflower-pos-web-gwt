package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding;

import com.google.inject.Provider;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DSProtocol;

public class RemoveOperationBindingProvider implements Provider<OperationBinding>{

	public OperationBinding get() {
		OperationBinding op = new OperationBinding();
		op.setOperationType(DSOperationType.REMOVE);
		op.setDataProtocol(DSProtocol.POSTMESSAGE);
		return op;
	}

}
