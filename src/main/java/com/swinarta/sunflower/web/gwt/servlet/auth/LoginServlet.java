package com.swinarta.sunflower.web.gwt.servlet.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;

public class LoginServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2792354862444197500L;

	private HttpClient client = new HttpClient();
	private String url = "/sunflower/rest/seed/j_spring_security_check";

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		

		PostMethod post = new PostMethod("http://"+request.getServerName()+":"+request.getServerPort()+url);
		post.addParameter("j_username", request.getParameter("j_username"));
		post.addParameter("j_password", request.getParameter("j_password"));
		try {
			client.executeMethod(post);
			String resp = post.getResponseBodyAsString();
			
			if(post.getResponseBodyAsString().indexOf("isc_loginSuccess") > 0){		
				Header cookieHeader = post.getResponseHeader("Set-Cookie");
				String sessionValue = cookieHeader.getValue();
				String sessionId = StringUtils.substringBetween(sessionValue, "JSESSIONID=", ";");
				response.addCookie(new Cookie("JSESSIONID", sessionId));
			}

			response.getWriter().write(resp);					

		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			post.releaseConnection();
		}
		
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
