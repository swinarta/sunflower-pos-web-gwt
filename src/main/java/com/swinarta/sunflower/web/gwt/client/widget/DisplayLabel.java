package com.swinarta.sunflower.web.gwt.client.widget;

import java.util.Date;

import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

public class DisplayLabel extends VLayout{

	private Label titleLabel = new Label();
	private Label valueLabel = new Label();
	private String defaultSuffix = ":";

	public DisplayLabel(String title){
		this(title, null);
	}
	
	public DisplayLabel(String title, String styleName){
		titleLabel.setContents(title + defaultSuffix);
		titleLabel.setStyleName("labelTitle");		
		valueLabel.setCanSelectText(true);
		
		setContentStyle(styleName);
		
		setMargin(1);
		setHeight(12);
		
		addMember(titleLabel);
		addMember(valueLabel);
		
	}
	
	public void setContentStyle(String styleName){
		if(styleName != null){
			valueLabel.setBaseStyle(styleName);
		}		
	}
		
	public void setHeight(int height){
		titleLabel.setHeight(height);
		valueLabel.setHeight(height);
	}
	
	public void setContents(String contents){
		valueLabel.setContents(contents);
	}
	
	public void setContents(Double d){
		String value = StringFormatUtil.getFormat(d);
		String contents;
		if(d < 0){
			contents = "<p style=\"color: red;\">" + value + "</p>";
		}else{
			contents = value;
		}
		valueLabel.setContents(contents);
	}

	public void setContents(Float f){
		String value = StringFormatUtil.getFormat(f);
		String contents;
		if(f < 0){
			contents = "<p style=\"color: red;\">" + value + "</p>";
		}else{
			contents = value;
		}
		valueLabel.setContents(contents);
	}

	public void setContents(Integer i){
		String value = StringFormatUtil.getFormat(i);
		String contents;
		if(i < 0){
			contents = "<p style=\"color: red;\">" + value + "</p>";
		}else{
			contents = value;
		}
		valueLabel.setContents(contents);
	}

	public void setContentsInPercent(Double d){
		String value = StringFormatUtil.getPercentFormat(d);
		valueLabel.setContents(value);		
	}

	public void setContents(Boolean b){
		String value;
		if(b){
			value = "Yes";
		}else{
			value = "No";
		}
		valueLabel.setContents(value);
	}

	public void setContents(Date date){
		setContents(date, false);
	}

/*	public void setContentsLongToShortDate(Long date){
		setContentsLongToDate(date, true);
	}
	
	public void setContentsLongToDate(Long date, boolean isShortFormat){
		String value = null;
		if(isShortFormat){
			value = StringFormatUtil.getShortDateFormat(date);
		}
		valueLabel.setContents(value);
	}*/
	
	public void setContents(Date date, boolean isShortFormat){
		String value;
		if(isShortFormat){
			value = StringFormatUtil.getShortDateFormat(date);
		}else{
			value = StringFormatUtil.getDateFormat(date);
		}
		
		valueLabel.setContents(value);
	}

}
