package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrinterChangePresenceEvent;
import com.swinarta.sunflower.web.gwt.client.connectivity.Printer;
import com.swinarta.sunflower.web.gwt.client.connectivity.impl.PrinterImpl;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.util.PrinterUtil;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class PrintActionCanvas extends VLayout{

	private IButton printBarcodeWithTitle = new IButton("Print Barcode With Price");
	private IButton printBarcodeWithoutTitle = new IButton("Print Barcode Without Price");
	private IButton printShelving = new IButton("Print Shelving");
	
	private Window printWindow = new Window();
	private StaticTextItem descriptionItem = new StaticTextItem("description", "Description");
	private StaticTextItem barcodeItem = new StaticTextItem("barcode", "Barcode");
	private StaticTextItem priceItem = new StaticTextItem("price", "Price");
	private	TextItem printQtyItem = new TextItem("printQty", "Print QTY");
	private Button printButton = new Button("Print");
	
	private Map<String, Map<String, Object>> printActionMap = new HashMap<String, Map<String, Object>>();
		
	private OnPrinterChangePresenceEvent event;
	
	private String barcode;
	private String productDescription;
	private Double price;
	
	final private Printer printer;
	
	@Inject
	public PrintActionCanvas(Printer printer){
		
		this.printer = printer;

		setGroupTitle("Print");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutBottomMargin(8);
		setLayoutRightMargin(8);
		setMargin(3);
		
		printQtyItem.setSelectOnFocus(true);
		
		Map<String, Object> printBarcodeWithTitleMap = new HashMap<String, Object>();
		printBarcodeWithTitleMap.put("defaultValue", 2);
		printBarcodeWithTitleMap.put("printMode", IConstants.PRINT_MODE_BARCODE_WITH_PRICE);
		printBarcodeWithTitleMap.put("printerId", PrinterImpl.PRINTER01);
		printActionMap.put(printBarcodeWithTitle.getTitle(), printBarcodeWithTitleMap);

		Map<String, Object> printBarcodeWithoutTitleMap = new HashMap<String, Object>();
		printBarcodeWithoutTitleMap.put("defaultValue", 2);
		printBarcodeWithoutTitleMap.put("printMode", IConstants.PRINT_MODE_BARCODE_WITHOUT_PRICE);
		printBarcodeWithoutTitleMap.put("printerId", PrinterImpl.PRINTER01);
		printActionMap.put(printBarcodeWithoutTitle.getTitle(), printBarcodeWithoutTitleMap);

		Map<String, Object> printShelvingMap = new HashMap<String, Object>();
		printShelvingMap.put("defaultValue", 1);
		printShelvingMap.put("printMode", IConstants.PRINT_MODE_SHELVING);
		printShelvingMap.put("printerId", PrinterImpl.PRINTER02);
		printActionMap.put(printShelving.getTitle(), printShelvingMap);
		
		printBarcodeWithTitle.setAutoFit(true);
		printBarcodeWithoutTitle.setAutoFit(true);
		
		HLayout printer1Layout = new HLayout(8);		
		printer1Layout.setGroupTitle(PrinterImpl.PRINTER01 + " (" + printer.getStatusString(PrinterImpl.PRINTER01) + ")");
		printer1Layout.setIsGroup(true);
		printer1Layout.setLayoutTopMargin(8);
		printer1Layout.setLayoutLeftMargin(8);
		printer1Layout.setLayoutBottomMargin(8);
		printer1Layout.setLayoutRightMargin(8);
		
		printer1Layout.addMember(printBarcodeWithTitle);
		printer1Layout.addMember(printBarcodeWithoutTitle);
		
		Canvas[] items1 = printer1Layout.getMembers();
		for (Canvas canvas : items1) {
			canvas.setDisabled(!printer.getStatus(PrinterImpl.PRINTER01));
		}
		
		HLayout printer2Layout = new HLayout();
		printer2Layout.setGroupTitle(PrinterImpl.PRINTER02 + " (" + printer.getStatusString(PrinterImpl.PRINTER02) + ")");
		printer2Layout.setIsGroup(true);
		printer2Layout.setLayoutTopMargin(8);
		printer2Layout.setLayoutLeftMargin(8);
		printer2Layout.setLayoutBottomMargin(8);
		printer2Layout.setLayoutRightMargin(8);
		printer2Layout.addMember(printShelving);
		
		Canvas[] items2 = printer2Layout.getMembers();
		for (Canvas canvas : items2) {
			canvas.setDisabled(!printer.getStatus(PrinterImpl.PRINTER02));
		}
		
		final Map<String, Layout> printerMap = new HashMap<String, Layout>();
		printerMap.put(PrinterImpl.PRINTER01, printer1Layout);
		printerMap.put(PrinterImpl.PRINTER02, printer2Layout);
		
		event = new OnPrinterChangePresenceEvent() {
			
			public void changeStatus(String id, Boolean isAvailable, String statusStr) {
				Layout layout = printerMap.get(id);
				layout.setGroupTitle(id + " (" + statusStr + ")");
				Canvas[] items = layout.getMembers();
				for (Canvas canvas : items) {
					canvas.setDisabled(!isAvailable);
				}
			}
		};
		
		printer.register(event);
		
		initWindow();
		initButton();
		
		addMember(printer1Layout);
		addMember(printer2Layout);
		
	}	
	
	public void setBarcode(String barcode) {
		this.barcode = barcode;
		barcodeItem.setValue(barcode);		
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
		descriptionItem.setValue(productDescription);
	}
	
	public void setPrice(Double price) {
		this.price = price;
		priceItem.setValue(StringFormatUtil.getFormat(price));
	}

	private void initWindow(){
		printWindow.setWidth(280);
		printWindow.setHeight(160);
		printWindow.setIsModal(true);
		printWindow.setShowModalMask(true);
		printWindow.setShowCloseButton(true);
		printWindow.setShowMinimizeButton(false);

		descriptionItem.setRequired(true);
		barcodeItem.setRequired(true);
		priceItem.setRequired(true);
		printQtyItem.setRequired(true);	

		printQtyItem.setWidth(40);
		printButton.setWidth100();
		
		VLayout layout = new VLayout();
		layout.setWidth100();
		layout.setHeight100();
		
		DynamicForm form = new DynamicForm();
		form.setMargin(8);
		form.setFields(barcodeItem, descriptionItem, priceItem, printQtyItem);
				
		layout.addMember(form);
		
		printWindow.addItem(layout);
		printWindow.addItem(printButton);
	}
	
	private void initButton(){
		
		ClickHandler clickHandler = new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				String id = ((IButton)event.getSource()).getTitle();
				printWindow.setTitle(id);
				printWindow.centerInPage();				
				printWindow.show();
				printQtyItem.setDefaultValue(String.valueOf(printActionMap.get(id).get("defaultValue")));
				printQtyItem.selectValue();
				printQtyItem.focusInItem();
			}
		};
		
		printBarcodeWithTitle.addClickHandler(clickHandler);
		printBarcodeWithoutTitle.addClickHandler(clickHandler);
		printShelving.addClickHandler(clickHandler);
		
		printButton.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				String id = printWindow.getTitle();
				Integer printMode = (Integer)printActionMap.get(id).get("printMode");
				String printerId = (String)printActionMap.get(id).get("printerId");
				Integer qty = FormUtil.getIntegerValueFromFormItem(printQtyItem);
				String keyword = PrinterUtil.constructPrinterActionStr(printMode, barcode, qty, productDescription, price);
				printer.sendPrintMessage(printerId, keyword);
				printWindow.hide();
			}
		});
	}
	
}