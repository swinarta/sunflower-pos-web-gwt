package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;

public class BuyingRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	final private OperationBinding fetchOperationBinding;
	final private FloatRangeValidator positiveFloatRangeValidatorProvider;

	@Inject
	public BuyingRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding,
			@Named("Fetch") OperationBinding fetchOperationBinding,
			@Named("Positive") FloatRangeValidator positiveFloatRangeValidatorProvider
			){
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
		this.fetchOperationBinding = fetchOperationBinding;
		this.positiveFloatRangeValidatorProvider = positiveFloatRangeValidatorProvider;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/ds/buying/");
		ds.setUpdateDataURL("/ds/buying/");
		ds.setAddDataURL("/ds/buying/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, updateOperationBinding, addOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);

		DataSourceFloatField buyingPriceFloatField = new DataSourceFloatField("buyingPrice", "Buying Price");				
		buyingPriceFloatField.setCanEdit(true);
		buyingPriceFloatField.setCanSave(true);
		buyingPriceFloatField.setRequired(true);

		DataSourceFloatField disc1FloatField = new DataSourceFloatField("disc1", "Disc-1");				
		disc1FloatField.setCanEdit(true);
		disc1FloatField.setCanSave(true);
		disc1FloatField.setRequired(true);

		DataSourceFloatField disc2FloatField = new DataSourceFloatField("disc2", "Disc-2");				
		disc2FloatField.setCanEdit(true);
		disc2FloatField.setCanSave(true);
		disc2FloatField.setRequired(true);

		DataSourceFloatField disc3FloatField = new DataSourceFloatField("disc3", "Disc-3");				
		disc3FloatField.setCanEdit(true);
		disc3FloatField.setCanSave(true);
		disc3FloatField.setRequired(true);

		DataSourceFloatField disc4FloatField = new DataSourceFloatField("disc4", "Disc-4");				
		disc4FloatField.setCanEdit(true);
		disc4FloatField.setCanSave(true);
		disc4FloatField.setRequired(true);

		DataSourceFloatField discPriceFloatField = new DataSourceFloatField("discPrice", "Disc Price");				
		discPriceFloatField.setCanEdit(true);
		discPriceFloatField.setCanSave(true);
		discPriceFloatField.setRequired(true);

		DataSourceBooleanField taxIncludedBooleanField = new DataSourceBooleanField("taxIncluded", "Tax Included");
		taxIncludedBooleanField.setCanEdit(true);
		taxIncludedBooleanField.setCanSave(true);
		
		buyingPriceFloatField.setValidators(positiveFloatRangeValidatorProvider);
		disc1FloatField.setValidators(positiveFloatRangeValidatorProvider);
		disc2FloatField.setValidators(positiveFloatRangeValidatorProvider);
		disc3FloatField.setValidators(positiveFloatRangeValidatorProvider);
		disc4FloatField.setValidators(positiveFloatRangeValidatorProvider);
		discPriceFloatField.setValidators(positiveFloatRangeValidatorProvider);

		ds.setFields(idTextField, buyingPriceFloatField, disc1FloatField, disc2FloatField, disc3FloatField, disc4FloatField,
				discPriceFloatField, taxIncludedBooleanField);

		return ds;
	}

}