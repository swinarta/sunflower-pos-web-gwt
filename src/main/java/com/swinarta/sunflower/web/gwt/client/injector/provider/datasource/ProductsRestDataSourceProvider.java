package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.validator.IsStringNumberValidator;
import com.swinarta.sunflower.web.gwt.client.validator.IsValidBarcodeValidator;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;

public class ProductsRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	final private IsStringNumberValidator isIntegerValidator;
	final private LengthRangeValidator skuLengthRangeValidator;
	final private LengthRangeValidator barcodeLengthRangeValidator;
	final private IsValidBarcodeValidator isValidBarcodeValidator;
	
	@Inject
	public ProductsRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding,
			@Named("Sku") LengthRangeValidator skuLengthRangeValidator,
			@Named("Barcode") LengthRangeValidator barcodeLengthRangeValidator,
			IsStringNumberValidator isIntegerValidator,
			IsValidBarcodeValidator isValidBarcodeValidator
			){
		this.fetchOperationBinding = fetchOperationBinding;
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
		this.isIntegerValidator = isIntegerValidator;
		this.skuLengthRangeValidator = skuLengthRangeValidator;
		this.barcodeLengthRangeValidator = barcodeLengthRangeValidator;
		this.isValidBarcodeValidator = isValidBarcodeValidator;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.UPDATE){
					int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
					dsRequest.setActionURL(getUpdateDataURL()+id);
				}else if(dsRequest.getOperationType() == DSOperationType.ADD){
					dsRequest.setActionURL(getAddDataURL());
				}				
				return super.transformRequest(dsRequest);
			}
		};
		ds.setFetchDataURL("/products/");
		ds.setAddDataURL("/products/");
		ds.setUpdateDataURL("/product/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, addOperationBinding, updateOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);

		DataSourceTextField skuTextField = new DataSourceTextField("sku", "SKU");		
		skuTextField.setCanEdit(true);
		skuTextField.setCanSave(true);
		skuTextField.setRequired(true);

		DataSourceTextField barcodeTextField = new DataSourceTextField("barcode", "Barcode");		
		barcodeTextField.setCanEdit(true);
		barcodeTextField.setCanSave(true);
		barcodeTextField.setRequired(true);
		
		DataSourceTextField descriptionTextField = new DataSourceTextField("longDescription", "Long Description");
		descriptionTextField.setCanEdit(true);
		descriptionTextField.setCanSave(true);
		descriptionTextField.setRequired(true);

		DataSourceTextField shortDescriptionTextField = new DataSourceTextField("shortDescription", "Short Description");
		shortDescriptionTextField.setCanEdit(true);
		shortDescriptionTextField.setCanSave(true);
		shortDescriptionTextField.setRequired(true);

		DataSourceBooleanField consignmentBooleanField = new DataSourceBooleanField("consignment", "Consignment");
		consignmentBooleanField.setCanEdit(true);
		consignmentBooleanField.setCanSave(true);

		DataSourceBooleanField scallableBooleanField = new DataSourceBooleanField("scallable", "Scallable");
		scallableBooleanField.setCanEdit(true);
		scallableBooleanField.setCanSave(true);
				
		barcodeTextField.setValidators(barcodeLengthRangeValidator, isIntegerValidator, isValidBarcodeValidator);
		skuTextField.setValidators(skuLengthRangeValidator, isIntegerValidator);
						
		ds.setFields(idTextField, skuTextField, barcodeTextField, descriptionTextField, shortDescriptionTextField, consignmentBooleanField, scallableBooleanField);

		return ds;
	}

}