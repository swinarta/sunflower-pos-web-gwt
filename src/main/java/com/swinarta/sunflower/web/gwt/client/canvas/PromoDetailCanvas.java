package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.DateUtils;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

public class PromoDetailCanvas extends VLayout{

	final private PromoBasicCanvas basicCanvas;
	final private PromoListCanvas listCanvas;
	final private Label currentPromoLabel;
	
	@Inject
	public PromoDetailCanvas(
		PromoBasicCanvas basicCanvas,
		PromoListCanvas listCanvas,
		@Named("TodayPromotion") Label currentPromoLabel
			){
		
		this.basicCanvas = basicCanvas;
		this.listCanvas = listCanvas;
		this.currentPromoLabel = currentPromoLabel;
		
		setCanSelectText(true);
				
		basicCanvas.setHeight(125);
		currentPromoLabel.setShowEdges(true);

		addMember(currentPromoLabel);
		addMember(basicCanvas);
		addMember(listCanvas);
		
		currentPromoLabel.hide();
		listCanvas.setParentLayout(this);
	}
	
	public void loadBasicData(Record record){
		basicCanvas.loadData(record);
		listCanvas.loadData(record);
		
		Date today = new Date();
		Date startDate = record.getAttributeAsDate("startDate");
		
		DateUtils.resetTime(today);
		DateUtils.resetTime(startDate);
				
		if(startDate.compareTo(today) <= 0){
			currentPromoLabel.show();
		}		
	}
	
}
