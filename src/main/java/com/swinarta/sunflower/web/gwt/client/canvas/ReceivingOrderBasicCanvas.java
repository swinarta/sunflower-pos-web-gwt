package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrintBarcodeActionCallback;
import com.swinarta.sunflower.web.gwt.client.forms.ReceivingOrderForm;
import com.swinarta.sunflower.web.gwt.client.util.DateUtils;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLink;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class ReceivingOrderBasicCanvas extends DataCanvas{

	private DisplayLabel roCode = new DisplayLabel("RO Code");
	private DisplayLabel status = new DisplayLabel("Status");
	private DisplayLabel roDate = new DisplayLabel("Receive Date");
	private DisplayLabel remarks = new DisplayLabel("Remarks");
	private DisplayLink poCode = new DisplayLink("PO");

	private Record record;
	
	final private PurchasingOrderBasicCanvas poBasicCanvas;
	final private Window poWindow;
	final private ReceivingOrderForm roForm;
	final private LinkItem completeLinkItem;
	final private LinkItem downloadAsPdfLinkItem;
	final private LinkItem downloadAsTextLinkItem;
	final private LinkItem printBarcodeLinkItem;
	
	private DSCallback completeCallBack;
	private DSCallback saveCallback;
	private DSCallback updateStatusCallback;
	private OnPrintBarcodeActionCallback printBarcodeCallback;
		
	@Inject
	public ReceivingOrderBasicCanvas(
			PurchasingOrderBasicCanvas poBasicCanvas,
			@Named("DisplayPurchasingOrder") final Window poWindow,
			ReceivingOrderForm roForm,
			@Named("CompleteReceivingOrder") LinkItem completeLinkItem,
			@Named("DownloadAsPdf") LinkItem downloadAsPdfLinkItem,
			@Named("DownloadAsText") LinkItem downloadAsTextLinkItem,
			@Named("PrintBarcode") LinkItem printBarcodeLinkItem
		){
		super(DataCanvasLayout.VERTICAL);
		
		this.poBasicCanvas = poBasicCanvas;
		this.poWindow = poWindow;
		this.roForm = roForm;
		this.completeLinkItem = completeLinkItem;
		this.downloadAsPdfLinkItem = downloadAsPdfLinkItem;
		this.downloadAsTextLinkItem = downloadAsTextLinkItem;
		this.printBarcodeLinkItem = printBarcodeLinkItem;
		
		setGroupTitle("Receiving Order Information");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setMargin(3);
				
		setForm(roForm);						

		editForm.setWidth(400);
		editForm.setColWidths(100,100,100,100,100);
		editForm.setNumCols(3);
		editForm.setFields(editItemLink, completeLinkItem, downloadAsPdfLinkItem, downloadAsTextLinkItem, printBarcodeLinkItem);

		HLayout layout1 = new HLayout(15);
		layout1.addMember(roCode);
		layout1.addMember(status);
		layout1.addMember(roDate);

		HLayout layout2 = new HLayout(15);
		layout2.addMember(poCode);
		layout2.addMember(remarks);

		dataComposite.addMember(layout1);
		dataComposite.addMember(layout2);
		dataComposite.addMember(editForm);

		initCallback();
		roForm.setSaveCallback(saveCallback);

		initWindow();
		initComponent();
		initHandler();
	}
	
	public void setPrintBarcodeCallback(OnPrintBarcodeActionCallback printBarcodeCallback) {
		this.printBarcodeCallback = printBarcodeCallback;
	}

	public void setUpdateStatusCallback(DSCallback updateStatusCallback) {
		this.updateStatusCallback = updateStatusCallback;
	}

	private void initCallback() {
		saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
					addOrUpdateWindow.hide();
					if(updateStatusCallback != null){
						updateStatusCallback.execute(response, rawData, request);
					}
				}
				
			}
		};		
		completeCallBack = new DSCallback() {			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					SC.say("Receiving Order is successfuly COMPLETED");
					saveCallback.execute(response, rawData, request);
				}
			}
		};		
	}

	private void initComponent(){
		poBasicCanvas.setIsGroup(false);
		poBasicCanvas.setShowEditLink(false);
	}
	
	private void initHandler(){
		poCode.addClickHandlerOverride(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				poWindow.centerInPage();
				poWindow.show();				
			}
		});
		completeLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				completeLinkClicked();
			}
		});
		printBarcodeLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				printBarcodeLinkClicked();
			}
		});
	}
	
	protected void printBarcodeLinkClicked() {
		printBarcodeCallback.execute();
	}

	protected void completeLinkClicked() {
		SC.ask("Complete Receiving Order?", new BooleanCallback() {			
			public void execute(Boolean value) {
				if(value){
					record.setAttribute("status", "COMPLETED");
					roForm.editRecord(record);
					roForm.saveData(completeCallBack);
				}
			}
		});
	}

	private void initWindow(){
		poWindow.setWidth(400);
		poWindow.setHeight(300);
		poWindow.addItem(poBasicCanvas);		
	}
	
	public void loadData(Record r) {
		this.record = r;
		
		String statusStr = r.getAttributeAsString("status");
		Integer roId = r.getAttributeAsInt("id");
		
		roCode.setContents(r.getAttributeAsString("roId"));
		status.setContents(statusStr);
		roDate.setContents(r.getAttributeAsDate("roDate"), true);
		remarks.setContents(r.getAttributeAsString("remarks"));
		
		Record poRecord = r.getAttributeAsRecord("po");
		
		poCode.setContents(poRecord.getAttribute("poId"));
		
		Date poDate = DateUtils.getIsoDateFormat().parse(poRecord.getAttribute("poDate"));
		Date cancelDate = DateUtils.getIsoDateFormat().parse(poRecord.getAttribute("cancelDate"));
		
		poRecord.setAttribute("poDate", poDate);
		poRecord.setAttribute("cancelDate", cancelDate);
		poBasicCanvas.loadData(poRecord);
		poWindow.setTitle(poRecord.getAttribute("poId"));
		
		if("NEW".equalsIgnoreCase(statusStr)){
			editItemLink.show();
			completeLinkItem.show();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			printBarcodeLinkItem.hide();
		}else{
			editItemLink.hide();
			completeLinkItem.hide();
			downloadAsPdfLinkItem.show();
			downloadAsTextLinkItem.show();
			printBarcodeLinkItem.show();
			downloadAsPdfLinkItem.setValue(IConstants.REST_DS + "/report/ro/" + roId + "/pdf");
			downloadAsTextLinkItem.setValue(IConstants.REST_DS + "/report/ro/" + roId + "/txt");			
		}
	}

	public void setPrinterAvailability(Boolean isAvailable){
		if(isAvailable){
			printBarcodeLinkItem.enable();
			printBarcodeLinkItem.setLinkTitle("Print Barcode >>");
		}else{
			printBarcodeLinkItem.disable();
			printBarcodeLinkItem.setLinkTitle("Print Barcode (OFFLINE)");
		}
	}
	
	protected void editLinkClicked(){
		customizeWindow("Edit Receiving Order", 250, 240);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
		roForm.editRecord(record);
	}

	protected void buttonWindowClicked(){
		if(roForm.validate(false)){
			roForm.saveData();
		}
	}
}
