package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;

public class ProductMeasurementRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;

	@Inject
	public ProductMeasurementRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			@Override
			public Object transformRequest(DSRequest dsRequest){
				
				if(dsRequest.getOperationType() == DSOperationType.FETCH){
					Integer productId = JSOHelper.getAttributeAsInt(dsRequest.getData(), "productId");
					Integer measurementId = JSOHelper.getAttributeAsInt(dsRequest.getData(), "measurementId");
					dsRequest.setActionURL(getFetchDataURL()+productId+"/"+measurementId);
				}
				
				return super.transformRequest(dsRequest);
			}			
		};		
		
		ds.setFetchDataURL("/productmeasurement/");
		ds.setDataFormat(DSDataFormat.JSON);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id");
		idTextField.setHidden(true);
		
		ds.setOperationBindings(fetchOperationBinding);

		return ds;
	}

}