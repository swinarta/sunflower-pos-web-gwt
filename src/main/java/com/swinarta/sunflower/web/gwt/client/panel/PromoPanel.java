package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.forms.PromoForm;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PromoListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.PromoTabSet;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class PromoPanel extends BasePanel{

	private static final String DESCRIPTION = "promo";

    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
        	PromoPanel panel = injector.getPromoPanel();
        	id = DESCRIPTION;
            return panel;
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }
    
    private DynamicForm addPromoForm;
    
    final private PromoListGrid currPromoGrid;
    final private PromoTabSet tabSet;
    final private Tab tab;
    final private LinkItem addPromoLinkItem;
    final private Window addPromoWindow;
    final private Button saveButton;
    final private PromoForm promoForm;
    
    @Inject
    public PromoPanel(
    	PromoTabSet tabSet,
    	@Named("Promo") Tab tab,
    	PromoListGrid currPromoGrid,
		@Named("AddPromo") LinkItem addPromoLinkItem,
		@Named("AddPromo") Window addPromoWindow,
		@Named("SaveInForm") Button saveButton,
		PromoForm promoForm
    		){
    	
    	this.tabSet = tabSet;
    	this.tab = tab;
    	this.currPromoGrid = currPromoGrid;
    	this.addPromoLinkItem = addPromoLinkItem;
    	this.addPromoWindow = addPromoWindow;
    	this.saveButton = saveButton;
    	this.promoForm = promoForm;

    	currPromoGrid.setWidth(600);
    	currPromoGrid.setHeight(600);
    	
    	init();
    	initWindow();
    	initHandler();
    }

	@Override
	public Canvas getViewPanel() {
		
		addPromoForm = new DynamicForm();
		addPromoForm.setFields(addPromoLinkItem);
		
		Label currentLabel = new Label("<b>Current and Future Promotion</b>");
		currentLabel.setHeight(14);

		currPromoGrid.fetchData();

		VLayout layout = new VLayout();
		layout.setLayoutTopMargin(10);
		layout.setLayoutLeftMargin(25);
		
		layout.addMember(addPromoForm);
		layout.addMember(currentLabel);
		layout.addMember(currPromoGrid);

		tab.setPane(layout);
		tabSet.addTab(tab);

		return tabSet;
	}
	
	private void initWindow(){
		addPromoWindow.setTitle("Add Promo");
		addPromoWindow.setWidth(270);
		addPromoWindow.setHeight(350);
		
		addPromoWindow.addItem(promoForm);
		addPromoWindow.addItem(saveButton);
	}
	
	private void initHandler(){
		addPromoLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				addPromoWindow.centerInPage();
				addPromoWindow.show();
				promoForm.editNewRecord();
			}
		});
		
		saveButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {			
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				promoForm.validate();
				if(promoForm.validate()){
					promoForm.saveData(new DSCallback() {						
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							tabSet.loadPromo(response.getData()[0]);
							addPromoWindow.hide();
						}
					});
				}
			}
		});
		
		currPromoGrid.addDoubleClickHandler(new DoubleClickHandler() {			
			public void onDoubleClick(DoubleClickEvent event) {
				PromoListGrid listGrid = (PromoListGrid)event.getSource();
				Record record = listGrid.getSelectedRecord();
				tabSet.loadPromo(record);
			}
		});
	}

}
