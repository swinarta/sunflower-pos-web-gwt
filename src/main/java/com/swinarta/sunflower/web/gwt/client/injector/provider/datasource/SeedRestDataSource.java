package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.smartgwt.client.data.RestDataSource;

public class SeedRestDataSource extends RestDataSource{
	
	public void setFetchDataURL(String url){
		super.setFetchDataURL(IConstants.REST_DS + url);
	}
	
	public void setAddDataURL(String url){
		super.setAddDataURL(IConstants.REST_DS + url);
	}
	
	public void setUpdateDataURL(String url){
		super.setUpdateDataURL(IConstants.REST_DS + url);
	}
	
	public void setRemoveDataURL(String url){
		super.setRemoveDataURL(IConstants.REST_DS + url);
	}
}