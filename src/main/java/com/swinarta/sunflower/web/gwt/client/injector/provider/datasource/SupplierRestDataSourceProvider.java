package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;

public class SupplierRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	final private OperationBinding fetchOperationBinding;
	final private IntegerRangeValidator positiveIntegerRangeValidatorProvider;

	@Inject
	public SupplierRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding,
			@Named("GetFetch") OperationBinding fetchOperationBinding,
			@Named("Positive") IntegerRangeValidator positiveIntegerRangeValidatorProvider
			){
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
		this.fetchOperationBinding = fetchOperationBinding;
		this.positiveIntegerRangeValidatorProvider = positiveIntegerRangeValidatorProvider;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.UPDATE){
					int id = JSOHelper.getAttributeAsInt(dsRequest.getData(), "id");
					dsRequest.setActionURL(getUpdateDataURL()+id);					
				}
				return super.transformRequest(dsRequest);
			}
		};
		ds.setFetchDataURL("/suppliers/");
		ds.setUpdateDataURL("/supplier/");
		ds.setAddDataURL("/suppliers/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, updateOperationBinding, addOperationBinding);

		DataSourceIntegerField idIntegerField = new DataSourceIntegerField("id");
		idIntegerField.setPrimaryKey(true);
		idIntegerField.setHidden(true);
		
		DataSourceTextField supplierCodeTextField = new DataSourceTextField("supplierCode");		
		supplierCodeTextField.setCanEdit(true);
		supplierCodeTextField.setCanSave(true);
		supplierCodeTextField.setRequired(true);
		
		DataSourceTextField supplierNameTextField = new DataSourceTextField("name");		
		supplierNameTextField.setCanEdit(true);
		supplierNameTextField.setCanSave(true);
		supplierNameTextField.setRequired(true);

		DataSourceIntegerField termOfPaymentTextField = new DataSourceIntegerField("termOfPayment");		
		termOfPaymentTextField.setCanEdit(true);
		termOfPaymentTextField.setCanSave(true);
		termOfPaymentTextField.setRequired(true);
		
		termOfPaymentTextField.setValidators(positiveIntegerRangeValidatorProvider);

		ds.setFields(idIntegerField, supplierCodeTextField, supplierNameTextField, termOfPaymentTextField);

		return ds;
	}

}