package com.swinarta.sunflower.web.gwt.client.widget;

import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.Layout;

public class DisplayLink extends Layout{

	private DynamicForm form = new DynamicForm();
	private LinkItem link = new LinkItem();
	
	public DisplayLink(String title){
		link.setTitle(title);
		link.setTitleStyle("labelTitle");
		
		form.setTitleOrientation(TitleOrientation.TOP);
				
		setMargin(1);
		setHeight(12);
		
		form.setFields(link);
		
		addMember(form);
	}
	
	public void setContents(String contents){
		link.setValue(contents);
	}
	
	public void addClickHandlerOverride(ClickHandler clickHandler){
		link.addClickHandler(clickHandler);
	}
	
}
