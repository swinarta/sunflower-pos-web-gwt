package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.callback.OnStoreLoadedCompleteEvent;
import com.swinarta.sunflower.web.gwt.client.forms.ProductMainForm;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.sessionobject.Store;
import com.swinarta.sunflower.web.gwt.client.util.BarcodeUtil;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.ProductTabSet;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class ProductMainPanel extends BasePanel implements OnStoreLoadedCompleteEvent{

	private static final String DESCRIPTION = "product";
	
    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
            id = DESCRIPTION;
            return injector.getProductMainPanel();
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }
	
    private DynamicForm addSupplierForm;
    private DynamicForm searchForm;
    private TextItem searchProductTextItem;
    private ButtonItem searchButton;
    
    final private LinkItem addProductLinkItem;
    final private Window addProductWindow;
    final private Tab productMainTab;
    final private ProductTabSet tabSet;
    final private ProductListGrid productListGrid;
    final private ProductMainForm productForm;
    final private Button saveButton;
    final private RestDataSource utilityDataSource;
    final private Criteria getNextSkuCriteria;
    final private CustomComboBoxItem supplierComboBoxItem;
        
    @Inject
    public ProductMainPanel(
    	@Named("SupplierAny") CustomComboBoxItem supplierComboBoxItem, 
    	@Named("AddProduct") LinkItem addSupplierLinkItem,
    	@Named("AddProduct") Window addProductWindow,
    	@Named("SaveInForm") Button saveButton,
    	ProductTabSet tabSet,
    	ProductListGrid productListGrid,
    	@Named("SearchProduct") Tab productMainTab,
    	ProductMainForm productForm,
    	@Named("Utility") RestDataSource utilityDataSource,
    	@Named("GetNextSku") Criteria getNextSkuCriteria
    	){
    	
    	this.addProductLinkItem = addSupplierLinkItem;
    	this.addProductWindow = addProductWindow;
    	this.saveButton = saveButton;
    	this.productMainTab = productMainTab;
    	this.tabSet = tabSet;
    	this.productListGrid = productListGrid;
    	this.productForm = productForm;
    	this.utilityDataSource = utilityDataSource;
    	this.getNextSkuCriteria = getNextSkuCriteria;
    	this.supplierComboBoxItem = supplierComboBoxItem;
    	
    	init();
    	initComponent();
    }
    
	@Override
	public Canvas getViewPanel() {

		addSupplierForm = new DynamicForm();
		addSupplierForm.setWidth(400);
		addSupplierForm.setFields(addProductLinkItem);
		
		searchForm = new DynamicForm();
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);

		HiddenItem searchActionId = new HiddenItem("actionId");
		searchActionId.setValue("searchProduct");
		
		searchProductTextItem = new TextItem("searchProduct");  
		searchProductTextItem.setTitle("Search Product");  
		searchProductTextItem.setSelectOnFocus(true);
		searchProductTextItem.setWrapTitle(false);
		searchProductTextItem.setCharacterCasing(CharacterCasing.UPPER);
		
		searchButton = new ButtonItem("searchProduct", "Search");
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);
		
		searchForm.setFields(supplierComboBoxItem, searchProductTextItem, searchButton, searchActionId);

		productListGrid.setWidth(600);
		productListGrid.setHeight(600);
		
		productListGrid.hide();
		
		initComponentHandler();
		
		VLayout c = new VLayout();
		c.addMember(addSupplierForm);
		c.addMember(searchForm);
		c.addMember(productListGrid);
				
		c.setLayoutTopMargin(10);
		c.setLayoutLeftMargin(25);

		productMainTab.setPane(c);
		
		tabSet.addTab(productMainTab);
		return tabSet;
	}

	private void initComponentHandler(){

		addProductLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {				
				addProductWindow.centerInPage();
				addProductWindow.show();
				productForm.editNewRecord();
			}
		});

		saveButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				if(productForm.isAutoSku()){
					utilityDataSource.fetchData(getNextSkuCriteria, new DSCallback() {
						
						public void execute(DSResponse response, Object rawData, DSRequest request) {							
							Record record = (Record)response.getData()[0];
							String sku = record.getAttributeAsString("value");
							productForm.setSku(sku);
							if(productForm.isAutoBarcode()){
								String barcode = BarcodeUtil.generateBarcodeFromSku(sku);
								productForm.setBarcode(barcode);
							}
							performValidateAndSave();
						}
					});				
				}else{
					performValidateAndSave();
				}
			}
		});


		productListGrid.addDoubleClickHandler(new DoubleClickHandler() {			
			public void onDoubleClick(DoubleClickEvent event) {
				ProductListGrid listGrid = (ProductListGrid)event.getSource();				
				tabSet.loadProduct(listGrid.getSelectedRecord());
			}
		});
		
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchProduct();
				}  
		});
		searchProductTextItem.addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					searchProduct();
				}				
			}
		});
		
	}

	private void initComponent(){
		addProductWindow.setTitle("Add Product");
		addProductWindow.setWidth(270);
		addProductWindow.setHeight(425);		
		addProductWindow.addItem(productForm);
		addProductWindow.addItem(saveButton);
	}

	private void searchProduct(){
		String text = searchForm.getValueAsString("searchProduct");
		Integer supplierId = FormUtil.getIntegerValueFromFormItem(supplierComboBoxItem);
		if((text != null && text.length() > 2) || (supplierId != null && supplierId != -1)){
			productListGrid.show();
			productListGrid.fetchData(searchForm.getValuesAsCriteria());
		}
		searchForm.focus();	
	}


	private void performValidateAndSave(){
		if(productForm.validate()){
			productForm.saveData(new DSCallback() {				
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					tabSet.loadProduct(response.getData()[0]);
					addProductWindow.hide();
				}
			});
		}
	}

    public void changeHistoryState(String token){
      	 
        if(token.startsWith("product")){
        	String idStr = token.substring("product".length()+1, token.length());
        	try{
            	String id = String.valueOf(Integer.parseInt(idStr));
            	tabSet.selectTab(id);
        	}catch (Exception e) {
        		tabSet.selectTab(0);
			}
        	
        }
    }

	public void performOnStoreLoaded(Store store) {
		
		if(store != null && store.getIsHq()){
			addProductLinkItem.show();
		}else{
			addProductLinkItem.hide();
		}
		
	}

}