package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class PurchasingOrderListGrid extends ListGrid{

	@Inject
	public PurchasingOrderListGrid(
		@Named("PurchasingOrders") RestDataSource purchasingOrdersDataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
		setDataSource(purchasingOrdersDataSource);
		
		setDataPageSize(50);
		setCanGroupBy(false); 
		setCanFreezeFields(false);
		
		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);
		
		ListGridField poIdField = new ListGridField("poId");		
		poIdField.setWidth(80);
		poIdField.setAlign(Alignment.CENTER);

		ListGridField supplierField = new ListGridField("supplier");
		supplierField.setName("SUPPLIER");
		supplierField.setWidth(320);
		supplierField.setCellFormatter(new CellFormatter() {			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return String.valueOf(record.getAttributeAsMap("supplier").get("name"));
			}
		});

		ListGridField poDateField = new ListGridField("poDate");
		poDateField.setWidth(100);
		poDateField.setAlign(Alignment.CENTER);
		poDateField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return StringFormatUtil.getShortDateFormat(value);
			}
		});

		ListGridField poStatusField = new ListGridField("status");
		poStatusField.setWidth(100);
		poStatusField.setAlign(Alignment.CENTER);

		setFields(idField, poIdField, supplierField, poDateField, poStatusField);
						
	}

}
