package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.smartgwt.client.data.RestDataSource;

public class SupplierCustomComboBoxItemProvider implements Provider<CustomComboBoxItem>{

	final private RestDataSource supplierDataSource;
	final private CustomComboBoxItem comboBox;
	
	@Inject
	public SupplierCustomComboBoxItemProvider(
			@Named("Supplier") RestDataSource supplierDataSource,
			CustomComboBoxItem comboBox
			){
		
		this.supplierDataSource = supplierDataSource;
		this.comboBox = comboBox;
	}
	
	public CustomComboBoxItem get() {
				
		comboBox.setName("supplierId");
		comboBox.setTitle("Supplier");
		comboBox.setDisplayField("name");
		comboBox.setValueField("id");
		comboBox.setSortField("name");
		comboBox.setShowAllOptions(true);
		comboBox.setAnimatePickList(true);
		comboBox.setShowAnyRecordOption(false);
		comboBox.setDataSource(supplierDataSource);
		
		comboBox.fetchData();
		comboBox.setSelectOnFocus(true);
		
		return comboBox;
	}

}
