package com.swinarta.sunflower.web.gwt.client.validator;

import com.swinarta.sunflower.web.gwt.client.util.BarcodeUtil;
import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class IsValidBarcodeValidator extends CustomValidator{
	
	public IsValidBarcodeValidator(){
		setStopIfFalse(true);
	}
	
	@Override
	protected boolean condition(Object value) {
				
		if(!BarcodeUtil.isValidBarcode(String.valueOf(value))){
			setErrorMessage("Invalid Barcode");
			return false;
		}
		
		return true;
	}
	
}
