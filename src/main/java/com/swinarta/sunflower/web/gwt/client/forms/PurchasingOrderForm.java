package com.swinarta.sunflower.web.gwt.client.forms;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;

public class PurchasingOrderForm extends DynamicForm{

	final private CustomComboBoxItem supplierComboBoxItem;
	final private DateItem poDateItem = new DateItem("poDate", "PO Date");
	final private DateItem cancelDateItem = new DateItem("cancelDate", "Cancel Date");
	final private TextAreaItem remarksItem = new TextAreaItem("remarks", "Remarks");
	final private DateRangeValidator todayValidator;
	final private DateCompareValidator minDateCompareValidator;
	
	private DSCallback saveCallback;
	
	@Inject
	public PurchasingOrderForm(
			@Named("Supplier") CustomComboBoxItem supplierComboBoxItem,
			@Named("PurchasingOrders") RestDataSource purchasingOrdersDataSource,
			@Named("Today") DateRangeValidator todayValidator,
			@Named("Min") DateCompareValidator minDateCompareValidator
			){
		this.supplierComboBoxItem = supplierComboBoxItem;
		this.todayValidator = todayValidator;
		this.minDateCompareValidator = minDateCompareValidator;
		
		setTitleOrientation(TitleOrientation.TOP);

		setDataSource(purchasingOrdersDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);

		Date today = new Date();		
		poDateItem.setStartDate(today);
		cancelDateItem.setStartDate(today);

		supplierComboBoxItem.setRequired(true);
		poDateItem.setRequired(true);
		cancelDateItem.setRequired(true);
		
		minDateCompareValidator.setOtherField("poDate");
		minDateCompareValidator.setErrorMessage("Cancel Date must be greater than PO Date");
				
		setFields(supplierComboBoxItem, poDateItem, cancelDateItem, remarksItem);
	}
	
	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void editNewRecord(){
		clearErrors(true);
		supplierComboBoxItem.show();
		supplierComboBoxItem.focusInItem();
		
		poDateItem.setValidators(todayValidator);
		cancelDateItem.setValidators(todayValidator, minDateCompareValidator);

		super.editNewRecord();
	}
	
	public void editRecord(Record record){
		clearErrors(true);
		supplierComboBoxItem.hide();
				
		if("NEW".equalsIgnoreCase(record.getAttributeAsString("status")) || "PROCESS".equalsIgnoreCase(record.getAttributeAsString("status"))){
			poDateItem.setValidators(todayValidator);
			cancelDateItem.setValidators(todayValidator, minDateCompareValidator);
		}else{
			cancelDateItem.setValidators(minDateCompareValidator);
		}
		
		super.editRecord(record);
	}
		
	public void saveData(){
		super.saveData(saveCallback);
	}	
	
}
