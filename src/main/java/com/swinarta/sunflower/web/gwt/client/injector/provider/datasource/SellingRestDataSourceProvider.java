package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceFloatField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;

public class SellingRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding addOperationBinding;
	final private OperationBinding updateOperationBinding;
	final private OperationBinding fetchOperationBinding;
	final private FloatRangeValidator positiveFloatRangeValidatorProvider;

	@Inject
	public SellingRestDataSourceProvider(
			@Named("Add") OperationBinding addOperationBinding,
			@Named("Update") OperationBinding updateOperationBinding,
			@Named("Fetch") OperationBinding fetchOperationBinding,
			@Named("Positive") FloatRangeValidator positiveFloatRangeValidatorProvider
			){
		this.addOperationBinding = addOperationBinding;
		this.updateOperationBinding = updateOperationBinding;
		this.fetchOperationBinding = fetchOperationBinding;
		this.positiveFloatRangeValidatorProvider = positiveFloatRangeValidatorProvider;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/ds/selling/");
		ds.setUpdateDataURL("/ds/selling/");
		ds.setAddDataURL("/ds/selling/");		
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding, updateOperationBinding, addOperationBinding);

		DataSourceFloatField sellingPriceFloatField = new DataSourceFloatField("sellingPrice", "Selling Price");
		sellingPriceFloatField.setCanEdit(true);
		sellingPriceFloatField.setCanSave(true);
		sellingPriceFloatField.setRequired(true);

		sellingPriceFloatField.setValidators(positiveFloatRangeValidatorProvider);
		
		ds.setFields(sellingPriceFloatField);

		return ds;
	}

}