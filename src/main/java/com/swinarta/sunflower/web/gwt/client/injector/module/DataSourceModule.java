package com.swinarta.sunflower.web.gwt.client.injector.module;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.BuyingRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.CategoriesRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.CategoryRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ConfigurationRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.LoginRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.LogoutRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ProductMeasurementRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ProductMeasurementsRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ProductWithStocksRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ProductsRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.PromoDetailsRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.PromoRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.PromosRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.PurchasingOrderDetailsRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.PurchasingOrdersRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ReceivingOrderDetailsRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ReceivingOrderRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ReceivingOrdersRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ReturnSuppliersRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.ReturnToSupplierDetailsRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.SellingRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.StockRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.StoreRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.StoresRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.SupplierRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.TransactionRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.TransactionSummaryByPaymentTypeRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.TransactionSummaryByStationIdRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.TransferOrderDetailsRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.TransferOrdersRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.UtilityRestDataSourceProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding.AddOperationBindingProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding.FetchOperationBindingProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding.GetFetchOperationBindingProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding.RemoveOperationBindingProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.datasource.operationbinding.UpdateOperationBindingProvider;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;

public class DataSourceModule extends AbstractGinModule{

	@Override
	protected void configure() {
		
		bind(OperationBinding.class).annotatedWith(Names.named("Add")).toProvider(AddOperationBindingProvider.class).in(Singleton.class);
		bind(OperationBinding.class).annotatedWith(Names.named("Update")).toProvider(UpdateOperationBindingProvider.class).in(Singleton.class);
		bind(OperationBinding.class).annotatedWith(Names.named("Fetch")).toProvider(FetchOperationBindingProvider.class).in(Singleton.class);
		bind(OperationBinding.class).annotatedWith(Names.named("GetFetch")).toProvider(GetFetchOperationBindingProvider.class).in(Singleton.class);
		bind(OperationBinding.class).annotatedWith(Names.named("Remove")).toProvider(RemoveOperationBindingProvider.class).in(Singleton.class);

		bind(RestDataSource.class).annotatedWith(Names.named("Logout")).toProvider(LogoutRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Login")).toProvider(LoginRestDataSourceProvider.class).in(Singleton.class);

		bind(RestDataSource.class).annotatedWith(Names.named("Config")).toProvider(ConfigurationRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Products")).toProvider(ProductsRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Category")).toProvider(CategoryRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Categories")).toProvider(CategoriesRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Utility")).toProvider(UtilityRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Buying")).toProvider(BuyingRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Selling")).toProvider(SellingRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Store")).toProvider(StoreRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Stores")).toProvider(StoresRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Supplier")).toProvider(SupplierRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Stock")).toProvider(StockRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("ProductMeasurements")).toProvider(ProductMeasurementsRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("ProductMeasurement")).toProvider(ProductMeasurementRestDataSourceProvider.class).in(Singleton.class);
		
		bind(RestDataSource.class).annotatedWith(Names.named("PurchasingOrders")).toProvider(PurchasingOrdersRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("PurchasingOrderDetails")).toProvider(PurchasingOrderDetailsRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("ReceivingOrders")).toProvider(ReceivingOrdersRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("ReceivingOrder")).toProvider(ReceivingOrderRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("ReceivingOrderDetails")).toProvider(ReceivingOrderDetailsRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("ReturnSuppliers")).toProvider(ReturnSuppliersRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("ReturnSupplierDetails")).toProvider(ReturnToSupplierDetailsRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("TransferOrders")).toProvider(TransferOrdersRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("TransferOrderDetails")).toProvider(TransferOrderDetailsRestDataSourceProvider.class).in(Singleton.class);

		bind(RestDataSource.class).annotatedWith(Names.named("Promos")).toProvider(PromosRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Promo")).toProvider(PromoRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("PromoDetails")).toProvider(PromoDetailsRestDataSourceProvider.class).in(Singleton.class);

		bind(RestDataSource.class).annotatedWith(Names.named("TransactionSummaryByStationId")).toProvider(TransactionSummaryByStationIdRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("TransactionSummaryByPaymentType")).toProvider(TransactionSummaryByPaymentTypeRestDataSourceProvider.class).in(Singleton.class);
		bind(RestDataSource.class).annotatedWith(Names.named("Transaction")).toProvider(TransactionRestDataSourceProvider.class).in(Singleton.class);

		bind(RestDataSource.class).annotatedWith(Names.named("ProductWithStocks")).toProvider(ProductWithStocksRestDataSourceProvider.class).in(Singleton.class);

	}

}