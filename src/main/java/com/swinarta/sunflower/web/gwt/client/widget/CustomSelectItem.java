package com.swinarta.sunflower.web.gwt.client.widget;

import java.util.LinkedHashMap;

import com.swinarta.sunflower.web.gwt.client.injector.provider.criteria.GetAllCriteriaProvider;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.fields.SelectItem;

public class CustomSelectItem extends SelectItem{

	private Boolean showAnyRecordOption = false;
	private DataSource dataSource;
			
	public void setDataSource(DataSource dataSource){
		if(showAnyRecordOption){
			this.dataSource = dataSource;
		}else{
			setOptionDataSource(dataSource);
		}
	}
	
	public void setShowAnyRecordOption(Boolean showAnyRecordOption){
		this.showAnyRecordOption = showAnyRecordOption;
	}
		
	public void fetchData(){
		
		if(showAnyRecordOption){
			
			GetAllCriteriaProvider p = new GetAllCriteriaProvider();
			
			Criteria criteria = p.get();
			
			dataSource.fetchData(criteria, new DSCallback() {
				
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
					valueMap.put("-1", "---");

					Record[] incomingData = response.getData();
					for (Record record : incomingData) {
						valueMap.put(record.getAttribute(getValueField()), record.getAttribute(getDisplayField()));						
					}
										
					setValueMap(valueMap);
					setDisplayField(null);
					setValueField(null);
					setDefaultValue("-1");
				}
			});
		}
		
	}
	
}
