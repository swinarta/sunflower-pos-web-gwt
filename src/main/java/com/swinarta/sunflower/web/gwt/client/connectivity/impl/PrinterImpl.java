package com.swinarta.sunflower.web.gwt.client.connectivity.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.calclab.emite.core.client.xmpp.session.Session;
import com.calclab.emite.core.client.xmpp.stanzas.Message;
import com.calclab.emite.core.client.xmpp.stanzas.Presence;
import com.calclab.emite.core.client.xmpp.stanzas.XmppURI;
import com.calclab.suco.client.Suco;
import com.calclab.suco.client.events.Listener;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrinterChangePresenceEvent;
import com.swinarta.sunflower.web.gwt.client.connectivity.Printer;
import com.swinarta.sunflower.web.gwt.client.sessionobject.ClientSession;
import com.smartgwt.client.data.Record;

public class PrinterImpl implements Printer{

	public static String PRINTER01;
	public static String PRINTER02;

	public static String OFFLINE = "Offline";
	public static String ONLINE = "Online";
	
	private Map<String, Boolean> printerMap = new HashMap<String, Boolean>();
	private Map<String, String> printerMapStr = new HashMap<String, String>();

	final Session session = Suco.get(Session.class);
	final Set<String> printerIds = new HashSet<String>();
	
	private String host; 
	
	public Boolean getStatus(String id){
		if(id != null){
			return printerMap.get(id);			
		}else{
			return false;
		}
	}

	public String getStatusString(String id){
		if(id != null){
			return printerMapStr.get(id);
		}else{
			return OFFLINE;
		}
	}
		
	public void register(final OnPrinterChangePresenceEvent onPrinterChangePresenceEvent){

		session.onPresence(new Listener<Presence>() {			
			public void onEvent(Presence presence) {
				
				try{
					Boolean isAvailable;
					String statusStr;
					String username = presence.getFrom().getJID().getNode();
					if(printerIds.contains(username)){
						if(presence.getType() == null){
							isAvailable = true;
							statusStr = ONLINE;
						}else{
							isAvailable = false;
							statusStr = OFFLINE;
						}

						if(onPrinterChangePresenceEvent != null){
							onPrinterChangePresenceEvent.changeStatus(username, isAvailable, statusStr);
						}
						printerMap.put(username, isAvailable);
						printerMapStr.put(username, statusStr);
						
					}

				}catch (Exception e) {
				}
				
			}
		});		
	}
	
    public void sendPrintMessage(String printerId, String msg) {
    	XmppURI uri = XmppURI.jid(printerId + "@" + this.host);
        Message message = new Message(msg, uri);
        session.send(message);
    }

	public void init(){
		Record configRecord = (Record) ClientSession.objects.get("config");
		String printer1Username = configRecord.getAttribute("label.printer.1.username");
		String printer2Username = configRecord.getAttribute("label.printer.2.username");
		
		this.host = (String)configRecord.getAttribute("xmpp.web.host");
		
		PRINTER01 = printer1Username;
		PRINTER02 = printer2Username;

		printerIds.add(PRINTER01);
		printerIds.add(PRINTER02);
		
		printerMap.put(PRINTER01, false);
		printerMap.put(PRINTER02, false);
		
		printerMapStr.put(PRINTER01, OFFLINE);
		printerMapStr.put(PRINTER02, OFFLINE);
				
		register(null);
	}

}
