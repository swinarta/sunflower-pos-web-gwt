package com.swinarta.sunflower.web.gwt.client.injector.module;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.swinarta.sunflower.web.gwt.client.Init;
import com.swinarta.sunflower.web.gwt.client.canvas.BuyingCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.MainCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PrintActionCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ProductBasicCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ProductDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ProductImageCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ProductMeasurementCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PromoBasicCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PromoDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PromoListCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PurchasingOrderBasicCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PurchasingOrderDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.PurchasingOrderListCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReceivingOrderBasicCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReceivingOrderDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReceivingOrderListCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.RelatedProductCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReturnToSupplierBasicCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReturnToSupplierDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.ReturnToSupplierListCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.SellingCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.StockCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.SupplierBasicCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.SupplierDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.TransferOrderBasicCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.TransferOrderDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.canvas.TransferOrderListCanvas;
import com.swinarta.sunflower.web.gwt.client.forms.AddProductToDetailForm;
import com.swinarta.sunflower.web.gwt.client.forms.BuyingForm;
import com.swinarta.sunflower.web.gwt.client.forms.LoginForm;
import com.swinarta.sunflower.web.gwt.client.forms.ProductMainForm;
import com.swinarta.sunflower.web.gwt.client.forms.ProductMeasurementForm;
import com.swinarta.sunflower.web.gwt.client.forms.PromoForm;
import com.swinarta.sunflower.web.gwt.client.forms.PurchasingOrderForm;
import com.swinarta.sunflower.web.gwt.client.forms.ReturnSupplierForm;
import com.swinarta.sunflower.web.gwt.client.forms.SearchProductByTextForm;
import com.swinarta.sunflower.web.gwt.client.forms.SellingForm;
import com.swinarta.sunflower.web.gwt.client.forms.StockForm;
import com.swinarta.sunflower.web.gwt.client.forms.SupplierForm;
import com.swinarta.sunflower.web.gwt.client.forms.TransferOrderForm;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.CategoryCustomComboBoxItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.CategorySelectItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.MainTitleLabelProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.NotActiveLabelProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.POStatusSelectItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.ProductStatusLayoutProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.PromoTypeSelectItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.SaveButtonProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.StockModeSelectItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.StoreSelectItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.SupplierAnyCustomComboBoxItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.SupplierCustomComboBoxItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TodayPromotionLabelProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TransferOrderStatusSelectItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.UpperCaseFormItemInputTransformerProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.UserNameLabelProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.Address1TextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.Address2TextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.CityTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.ContactNameTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.FaxTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.MobileTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.PhoneTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.SupplierCodeTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.SupplierNameTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem.TermOfPaymentTextItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.AddProductLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.AddPromoLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.AddPurchasingOrderLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.AddReturnSupplierLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.AddSupplierLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.AddTransferOrderLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.CancelPuchasingOrderLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.CompleteReceivingOrderLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.DownloadAsPdfPurchasingOrderLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.DownloadAsTextPurchasingOrderLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.LogoutLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.PrintBarcodeLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem.ProcessPuchasingOrderLinkItemProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.PromoTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.ReceivingOrderTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.SearchProductTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.SearchPurchasingOrderTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.SearchReceivingOrderTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.SearchReturnSupplierTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.SearchSupplierTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.SearchTransactionTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab.SearchTransferOrderTabProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.window.AddProductWindowProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.window.AddSupplierWindowProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.window.DisplayPurchasingOrderWindowProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.ui.window.DisplaySupplierWindowProvider;
import com.swinarta.sunflower.web.gwt.client.panel.ProductMainPanel;
import com.swinarta.sunflower.web.gwt.client.panel.PromoPanel;
import com.swinarta.sunflower.web.gwt.client.panel.PurchasingOrderPanel;
import com.swinarta.sunflower.web.gwt.client.panel.ReceivingOrderPanel;
import com.swinarta.sunflower.web.gwt.client.panel.ReturnSupplierPanel;
import com.swinarta.sunflower.web.gwt.client.panel.StockSummaryPanel;
import com.swinarta.sunflower.web.gwt.client.panel.SupplierPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransactionPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransactionSummaryPanel;
import com.swinarta.sunflower.web.gwt.client.panel.TransferPanel;
import com.swinarta.sunflower.web.gwt.client.widget.AddProductToDetailWindow;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.swinarta.sunflower.web.gwt.client.widget.CustomSelectItem;
import com.swinarta.sunflower.web.gwt.client.widget.LoginWindow;
import com.swinarta.sunflower.web.gwt.client.widget.SideNavTree;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductMeasurementsGridList;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductWithStockListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PromoListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PurchasingOrderDetailListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PurchasingOrderListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ReceivingOrderDetailListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ReturnToSupplierDetailListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ReturnToSupplierListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.SupplierListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransactionListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransactionSummaryByPaymentListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransactionSummaryByStationListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransferOrderDetailListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransferOrderListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.ProductTabSet;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.PromoTabSet;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.PurchasingOrderTabSet;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.ReceivingOrderTabSet;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.ReturnToSupplierTabSet;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.SupplierTabSet;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.TransactionTabSet;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.TransferOrderTabSet;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.FormItemInputTransformer;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.tab.Tab;

public class GardenModule extends AbstractGinModule{

	@Override
	protected void configure() {
				
		bind(SelectItem.class).annotatedWith(Names.named("Category")).toProvider(CategorySelectItemProvider.class);
		bind(SelectItem.class).annotatedWith(Names.named("POStatus")).toProvider(POStatusSelectItemProvider.class);
		bind(SelectItem.class).annotatedWith(Names.named("PromoType")).toProvider(PromoTypeSelectItemProvider.class);
		bind(SelectItem.class).annotatedWith(Names.named("StockMode")).toProvider(StockModeSelectItemProvider.class);
		bind(SelectItem.class).annotatedWith(Names.named("Store")).toProvider(StoreSelectItemProvider.class);
		bind(SelectItem.class).annotatedWith(Names.named("TransferOrderStatus")).toProvider(TransferOrderStatusSelectItemProvider.class);

		bind(CustomComboBoxItem.class);
		bind(FormItemInputTransformer.class).annotatedWith(Names.named("UpperCase")).toProvider(UpperCaseFormItemInputTransformerProvider.class).in(Singleton.class);
		bind(CustomComboBoxItem.class).annotatedWith(Names.named("SupplierAny")).toProvider(SupplierAnyCustomComboBoxItemProvider.class);
		bind(CustomComboBoxItem.class).annotatedWith(Names.named("Supplier")).toProvider(SupplierCustomComboBoxItemProvider.class);
		bind(CustomComboBoxItem.class).annotatedWith(Names.named("Category")).toProvider(CategoryCustomComboBoxItemProvider.class);
		
		bind(LinkItem.class).annotatedWith(Names.named("Logout")).toProvider(LogoutLinkItemProvider.class).in(Singleton.class);
		bind(LinkItem.class).annotatedWith(Names.named("AddProduct")).toProvider(AddProductLinkItemProvider.class);
		bind(LinkItem.class).annotatedWith(Names.named("AddSupplier")).toProvider(AddSupplierLinkItemProvider.class).in(Singleton.class);
		bind(LinkItem.class).annotatedWith(Names.named("AddPurchasingOrder")).toProvider(AddPurchasingOrderLinkItemProvider.class).in(Singleton.class);
		bind(LinkItem.class).annotatedWith(Names.named("ProcessPurchasingOrder")).toProvider(ProcessPuchasingOrderLinkItemProvider.class);
		bind(LinkItem.class).annotatedWith(Names.named("CancelPurchasingOrder")).toProvider(CancelPuchasingOrderLinkItemProvider.class);
		bind(LinkItem.class).annotatedWith(Names.named("CompleteReceivingOrder")).toProvider(CompleteReceivingOrderLinkItemProvider.class);
		bind(LinkItem.class).annotatedWith(Names.named("DownloadAsPdf")).toProvider(DownloadAsPdfPurchasingOrderLinkItemProvider.class);
		bind(LinkItem.class).annotatedWith(Names.named("DownloadAsText")).toProvider(DownloadAsTextPurchasingOrderLinkItemProvider.class);
		bind(LinkItem.class).annotatedWith(Names.named("AddPromo")).toProvider(AddPromoLinkItemProvider.class).in(Singleton.class);
		bind(LinkItem.class).annotatedWith(Names.named("PrintBarcode")).toProvider(PrintBarcodeLinkItemProvider.class);
		bind(LinkItem.class).annotatedWith(Names.named("AddReturnSupplier")).toProvider(AddReturnSupplierLinkItemProvider.class).in(Singleton.class);
		bind(LinkItem.class).annotatedWith(Names.named("AddTransferOrder")).toProvider(AddTransferOrderLinkItemProvider.class).in(Singleton.class);
		
		bind(TextItem.class).annotatedWith(Names.named("SupplierCode")).toProvider(SupplierCodeTextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("SupplierName")).toProvider(SupplierNameTextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("TermOfPayment")).toProvider(TermOfPaymentTextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("ContactName")).toProvider(ContactNameTextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("Address1")).toProvider(Address1TextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("Address2")).toProvider(Address2TextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("City")).toProvider(CityTextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("Phone")).toProvider(PhoneTextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("Fax")).toProvider(FaxTextItemProvider.class);
		bind(TextItem.class).annotatedWith(Names.named("Mobile")).toProvider(MobileTextItemProvider.class);

		bind(Window.class).annotatedWith(Names.named("AddProduct")).toProvider(AddProductWindowProvider.class).in(Singleton.class);
		bind(Window.class).annotatedWith(Names.named("AddPurchasingOrder")).toProvider(AddProductWindowProvider.class).in(Singleton.class);
		bind(Window.class).annotatedWith(Names.named("AddSupplier")).toProvider(AddSupplierWindowProvider.class).in(Singleton.class);
		bind(Window.class).annotatedWith(Names.named("DisplaySupplier")).toProvider(DisplaySupplierWindowProvider.class);
		bind(Window.class).annotatedWith(Names.named("DisplayPurchasingOrder")).toProvider(DisplayPurchasingOrderWindowProvider.class);
		bind(Window.class).annotatedWith(Names.named("AddPromo")).toProvider(AddProductWindowProvider.class).in(Singleton.class);
		bind(Window.class).annotatedWith(Names.named("AddReturnSupplier")).toProvider(AddProductWindowProvider.class).in(Singleton.class);
		bind(Window.class).annotatedWith(Names.named("AddTransferOrder")).toProvider(AddProductWindowProvider.class).in(Singleton.class);
		bind(AddProductToDetailWindow.class);
		bind(LoginWindow.class).in(Singleton.class);
		
		bind(Button.class).annotatedWith(Names.named("SaveInForm")).toProvider(SaveButtonProvider.class);
		
		bind(Label.class).annotatedWith(Names.named("NotActiveProduct")).toProvider(NotActiveLabelProvider.class);
		bind(Label.class).annotatedWith(Names.named("TodayPromotion")).toProvider(TodayPromotionLabelProvider.class);
		bind(Label.class).annotatedWith(Names.named("MainTitle")).toProvider(MainTitleLabelProvider.class).in(Singleton.class);
		bind(Label.class).annotatedWith(Names.named("UserName")).toProvider(UserNameLabelProvider.class).in(Singleton.class);

		bind(Tab.class).annotatedWith(Names.named("SearchProduct")).toProvider(SearchProductTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("SearchSupplier")).toProvider(SearchSupplierTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("SearchPurchasingOrder")).toProvider(SearchPurchasingOrderTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("Promo")).toProvider(PromoTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("ReceivingOrder")).toProvider(ReceivingOrderTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("SearchReceivingOrder")).toProvider(SearchReceivingOrderTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("SearchReturnSupplier")).toProvider(SearchReturnSupplierTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("SearchTransaction")).toProvider(SearchTransactionTabProvider.class).in(Singleton.class);
		bind(Tab.class).annotatedWith(Names.named("SearchTransferOrder")).toProvider(SearchTransferOrderTabProvider.class).in(Singleton.class);
		
		bind(Layout.class).annotatedWith(Names.named("ProductStatus")).toProvider(ProductStatusLayoutProvider.class);
		
		bind(ProductTabSet.class).in(Singleton.class);
		bind(SupplierTabSet.class).in(Singleton.class);
		bind(PurchasingOrderTabSet.class).in(Singleton.class);
		bind(ReceivingOrderTabSet.class).in(Singleton.class);
		bind(PromoTabSet.class).in(Singleton.class);
		bind(ReturnToSupplierTabSet.class).in(Singleton.class);
		bind(TransactionTabSet.class).in(Singleton.class);
		bind(TransferOrderTabSet.class).in(Singleton.class);
		
		bind(ProductListGrid.class);
		bind(ProductWithStockListGrid.class);
		bind(SupplierListGrid.class);
		bind(ProductMeasurementsGridList.class);
		bind(PurchasingOrderListGrid.class);
		bind(PurchasingOrderDetailListGrid.class);
		bind(ReceivingOrderDetailListGrid.class);
		bind(PromoListGrid.class);
		bind(TransactionSummaryByStationListGrid.class);
		bind(TransactionSummaryByPaymentListGrid.class);
		bind(TransactionListGrid.class);
		bind(ReturnToSupplierListGrid.class);
		bind(ReturnToSupplierDetailListGrid.class);
		bind(TransferOrderListGrid.class);
		bind(TransferOrderDetailListGrid.class);
		
		bind(ReceivingOrderDetailCanvas.class);
		bind(ReceivingOrderBasicCanvas.class);
		bind(ReceivingOrderListCanvas.class);
		bind(PurchasingOrderBasicCanvas.class);
		bind(PurchasingOrderDetailCanvas.class);
		bind(PurchasingOrderListCanvas.class);
		bind(SupplierBasicCanvas.class);
		bind(SupplierDetailCanvas.class);
		bind(ProductDetailCanvas.class);
		bind(ProductBasicCanvas.class);
		bind(ProductImageCanvas.class);
		bind(BuyingCanvas.class);
		bind(SellingCanvas.class);
		bind(StockCanvas.class);
		bind(ProductMeasurementCanvas.class);
		bind(RelatedProductCanvas.class);
		bind(PrintActionCanvas.class);
		bind(PromoDetailCanvas.class);
		bind(PromoBasicCanvas.class);
		bind(PromoListCanvas.class);
		bind(ReturnToSupplierBasicCanvas.class);
		bind(ReturnToSupplierDetailCanvas.class);
		bind(ReturnToSupplierListCanvas.class);
		bind(TransferOrderBasicCanvas.class);
		bind(TransferOrderDetailCanvas.class);
		bind(TransferOrderListCanvas.class);
		bind(MainCanvas.class).in(Singleton.class);
		
		bind(LoginForm.class).in(Singleton.class);
		bind(ProductMainForm.class);
		bind(BuyingForm.class);
		bind(SellingForm.class);
		bind(StockForm.class);
		bind(ProductMeasurementForm.class);
		bind(SupplierForm.class);
		bind(PurchasingOrderForm.class);
		bind(PromoForm.class);
		bind(AddProductToDetailForm.class);
		bind(SearchProductByTextForm.class);
		bind(ReturnSupplierForm.class);
		bind(TransferOrderForm.class);
				
		bind(SideNavTree.class).in(Singleton.class);		
		
		bind(CustomSelectItem.class);
		
		bind(ProductMainPanel.class).in(Singleton.class);
		bind(SupplierPanel.class).in(Singleton.class);
		bind(PurchasingOrderPanel.class).in(Singleton.class);
		bind(PromoPanel.class).in(Singleton.class);
		bind(ReceivingOrderPanel.class).in(Singleton.class);
		bind(TransactionSummaryPanel.class).in(Singleton.class);
		bind(ReturnSupplierPanel.class).in(Singleton.class);
		bind(TransactionPanel.class).in(Singleton.class);
		bind(StockSummaryPanel.class).in(Singleton.class);
		bind(TransferPanel.class).in(Singleton.class);
		
		bind(Init.class).in(Singleton.class);
	}

}
