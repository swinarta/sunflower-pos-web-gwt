package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.TextItem;

import com.google.inject.Provider;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class CityTextItemProvider implements Provider<TextItem> {

	public TextItem get() {
		TextItem item = new TextItem("city", "City");
		item.setLength(255);
		item.setCharacterCasing(CharacterCasing.UPPER);
		return item;
	}

}
