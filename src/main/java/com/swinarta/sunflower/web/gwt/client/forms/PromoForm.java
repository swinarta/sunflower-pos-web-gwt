package com.swinarta.sunflower.web.gwt.client.forms;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator.COMPARATOR;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;

public class PromoForm extends DynamicForm{

	final private DateItem startDateItem = new DateItem("startDate", "Start Date");
	final private DateItem endDateItem = new DateItem("endDate", "End Date");
	final private TextItem descriptionTextItem = new TextItem("description", "Description");
	final private TextItem promoValueTextItem = new TextItem("promoValue", "Promo Value");
	
	final private DateRangeValidator todayValidator;
	final private DateCompareValidator minDateCompareValidator;
	
	private DSCallback saveCallback;
	
	@Inject
	public PromoForm(
		@Named("PromoType") SelectItem promoTypeItem,
		@Named("Promos") RestDataSource promoDataSource,
		@Named("Today") DateRangeValidator todayValidator,
		@Named("Min") DateCompareValidator minDateCompareValidator
			){
		
		this.todayValidator = todayValidator;
		this.minDateCompareValidator = minDateCompareValidator;
		
		setTitleOrientation(TitleOrientation.TOP);		
		setDataSource(promoDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);
		
		descriptionTextItem.setCharacterCasing(CharacterCasing.UPPER);

		minDateCompareValidator.setOtherField("startDate");
		minDateCompareValidator.setComparator(COMPARATOR.GREATER_OR_EQUALS);
		minDateCompareValidator.setErrorMessage("End Date must be greater or equals than Start Date");

		promoTypeItem.setRequired(true);
		
		promoValueTextItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		
		setFields(startDateItem, endDateItem, 
				promoTypeItem, promoValueTextItem, descriptionTextItem);

		this.getAttributeAsDate("startDate");

	}

	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void editNewRecord(){
		clearErrors(true);		
		promoValueTextItem.focusInItem();		
		startDateItem.setValidators(todayValidator);
		endDateItem.setValidators(todayValidator, minDateCompareValidator);		

		super.editNewRecord();
	}
	
	public boolean validate(){		
		return super.validate();
	}
	
}
