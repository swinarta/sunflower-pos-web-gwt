package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ReturnToSupplierListGrid extends ListGrid{

	@Inject
	public ReturnToSupplierListGrid(
		@Named("ReturnSuppliers") RestDataSource retSuppsDataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
		setDataSource(retSuppsDataSource);
		
		setDataPageSize(50);
		setCanGroupBy(false); 
		setCanFreezeFields(false);
		
		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);
		
		ListGridField returnIdField = new ListGridField("returnId", "Return Code");		
		returnIdField.setWidth(80);
		returnIdField.setAlign(Alignment.CENTER);

		ListGridField supplierField = new ListGridField("supplier");
		supplierField.setName("Supplier");
		supplierField.setWidth(320);
		supplierField.setCellFormatter(new CellFormatter() {			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return String.valueOf(record.getAttributeAsMap("supplier").get("name"));
			}
		});

		ListGridField returnDateField = new ListGridField("returnDate", "Return Date");
		returnDateField.setWidth(100);
		returnDateField.setAlign(Alignment.CENTER);
		returnDateField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return StringFormatUtil.getShortDateFormat(value);
			}
		});

		ListGridField poStatusField = new ListGridField("status", "Status");
		poStatusField.setWidth(100);
		poStatusField.setAlign(Alignment.CENTER);

		setFields(idField, returnIdField, supplierField, returnDateField, poStatusField);
						
	}

}
