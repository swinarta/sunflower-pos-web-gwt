package com.swinarta.sunflower.web.gwt.client.injector.provider.validator;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;

public class SkuLengthRangeValidatorProvider implements Provider<LengthRangeValidator>{

	public LengthRangeValidator get() {
		LengthRangeValidator skuLengthValidator = new LengthRangeValidator();
		skuLengthValidator.setMin(5);
		skuLengthValidator.setMax(8);
		skuLengthValidator.setErrorMessage("Invalid SKU Length");
		return skuLengthValidator;
	}

}
