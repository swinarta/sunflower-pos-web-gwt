package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.tab.Tab;

public class SearchPurchasingOrderTabProvider implements Provider<Tab>{

	public Tab get() {
		Tab tabSearch = new Tab("Search Purchasing Order", "icons/green/16/search.png");
		tabSearch.setCanClose(false);
		tabSearch.setID("searchpo");
		return tabSearch;
	}

}
