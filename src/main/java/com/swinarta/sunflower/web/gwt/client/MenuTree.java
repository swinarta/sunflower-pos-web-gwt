package com.swinarta.sunflower.web.gwt.client;

import com.smartgwt.client.types.SortArrow;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;

public class MenuTree extends TreeGrid{

	private MenuTreeNode[] data;
	
	public MenuTree(){
        setWidth100();
        setHeight100();
        setCustomIconProperty("icon");
        setShowSortArrow(SortArrow.CORNER);
        setNodeIcon("silk/application_view_list.png");
        setShowAllRecords(true);
        setLoadDataOnDemand(false);
        
        TreeGridField field = new TreeGridField();
        field.setCanFilter(true);
        field.setName("name");
        field.setTitle("<b>Menu</b>");
        setFields(field);
        
        Tree tree = new Tree();
        tree.setModelType(TreeModelType.PARENT);
        tree.setNameProperty("name");
        //tree.setOpenProperty("isOpen");
        tree.setIdField("id");
        tree.setParentIdField("parent");
        tree.setRootValue("root");

        initializeData();
        
        tree.setData(data);

        setData(tree);

	}
	
	private void initializeData(){
        data = new MenuTreeNode[]{
        		new MenuTreeNode("1","Menu", "root"),
        		new MenuTreeNode("2", "SubMenu", "1"),
            };

	}
}
