package com.swinarta.sunflower.web.gwt.client.widget;

import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickEvent;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickHandler;

public class CustomSectionStack extends SectionStackSection{

	private Boolean isExpand;
	
	public CustomSectionStack(){		
		isExpand = getAttributeAsBoolean("expanded");			
	}

	public CustomSectionStack(String title) {
		this();
		setTitle(title);
	}
	
	public void registerState(){
		getSectionStack().addSectionHeaderClickHandler(new SectionHeaderClickHandler() {
			
			public void onSectionHeaderClick(SectionHeaderClickEvent event) {
				isExpand = !isExpand;
			}
		});		
	}

	public Boolean isExpand() {
		return isExpand;
	}
		
	
}
