package com.swinarta.sunflower.web.gwt.client.widget.tabset;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.History;
import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.canvas.ReturnToSupplierDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

public class ReturnToSupplierTabSet extends TabSet{

	@Inject
	public ReturnToSupplierTabSet(){
		
		setTabBarPosition(Side.TOP);  
		setTabBarAlign(Side.LEFT);
		
		addTabSelectedHandler(new TabSelectedHandler() {
			
			public void onTabSelected(TabSelectedEvent event) {
				Tab selectedTab = event.getTab();				
				History.newItem("retsupp-"+ selectedTab.getID());
			}
		});
		
	}
	
	public void loadReturnToSupplier(final Record record){
		
		SC.showPrompt("Loading Return To Supplier ... ");
		
		DeferredCommand.addCommand(new Command() {
			
			public void execute() {
				String tabId = record.getAttribute("id");
				if(getTab(tabId) == null){
					String title = record.getAttribute("returnId");

					GardenGinjector injector = Ginjector.getInstance();
					ReturnToSupplierDetailCanvas detailCanvas = injector.getReturnToSupplierCanvas();
					
					Tab tab = new Tab(title);
					tab.setCanClose(true);
					tab.setPane(detailCanvas);
					tab.setID(tabId);
					
					detailCanvas.loadBasicData(record);
					
					addTab(tab);
				}
				
				selectTab(tabId);
				
				SC.clearPrompt();
			
			}
		});
		
	}
	
}