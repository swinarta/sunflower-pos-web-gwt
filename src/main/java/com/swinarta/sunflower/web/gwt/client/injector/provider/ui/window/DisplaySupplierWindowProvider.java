package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.window;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.Window;

public class DisplaySupplierWindowProvider implements Provider<Window> {

	public Window get() {
		Window window = new Window();
		window.setShowMinimizeButton(false);
		window.setIsModal(true);
		window.setShowModalMask(true);
		window.setDismissOnEscape(true);
		window.setDismissOnOutsideClick(true);
		window.setModalMaskOpacity(10);
		window.centerInPage();
		window.setShowCloseButton(true);

		return window;
	}

}
