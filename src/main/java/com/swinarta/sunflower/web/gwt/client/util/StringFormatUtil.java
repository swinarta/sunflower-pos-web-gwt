package com.swinarta.sunflower.web.gwt.client.util;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;

public class StringFormatUtil {

	public static String getFormat(Double d){
		return NumberFormat.getFormat("#,##0.00").format(d);
	}

	public static String getFormat(Float f){
		return NumberFormat.getFormat("#,##0.00").format(f);
	}

	public static String getFormat(Integer i){
		return NumberFormat.getFormat("#,##0").format(i);
	}

	public static String getPercentFormat(Double d){
		return NumberFormat.getFormat("#,##0.00%").format(d/100);
	}
	
	public static String getDateFormat(Date date){
		return DateTimeFormat.getFormat("dd/MM/yyyy HH:mm").format(date);
	}
	
	public static String getDateTimeFormat(Long date){
		return DateTimeFormat.getFormat("dd/MM/yyyy HH:mm").format(new Date(date));
	}

	public static String getShortDateFormat(Object date){
		Date d = null;
		if(date instanceof Date){
			d = (Date)date;
		}else if(date instanceof Long){
			d = new Date((Long)date);
		}
		
		return DateTimeFormat.getFormat("dd/MM/yyyy").format(d);
	}

	public static String getLongDateFormat(Object date){
		Date d = null;
		if(date instanceof Date){
			d = (Date)date;
		}else if(date instanceof Long){
			d = new Date((Long)date);
		}
		
		return DateTimeFormat.getFormat("dd/MM/yyyy HH:mm").format(d);
	}

	public static String getYesNo(Boolean b){
		if(b) return "Yes";
		else return "No";
	}

}
