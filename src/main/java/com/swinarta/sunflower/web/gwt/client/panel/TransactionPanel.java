package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransactionListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.TransactionTabSet;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class TransactionPanel extends BasePanel{

	private static final String DESCRIPTION = "transaction";
	
    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
            id = DESCRIPTION;
            return injector.getTransactionPanel();
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }

    final private DateItem transactionDateItem = new DateItem("transactionDate", "Date");
    final private ButtonItem searchButton = new ButtonItem("searchProduct", "Search");
    final private TextItem codeTextItem = new TextItem("code", "Transaction Code");
    final private TextItem stationIdTextItem = new TextItem("stationId", "Station ID");
    final private DynamicForm searchForm = new DynamicForm();    
    
    final private Tab tab;    
    final private TransactionListGrid gridList;
    final private TransactionTabSet tabSet;
    
	@Inject
	public TransactionPanel(
			TransactionTabSet tabSet,
			@Named("SearchTransaction") Tab tab,
			TransactionListGrid gridList
		){
		
		this.gridList = gridList;
		this.tab = tab;
		this.tabSet = tabSet;
		
		init();
		initComponentHandler();
	}

	@Override
	public Canvas getViewPanel() {
		
		transactionDateItem.setRequired(true);
		codeTextItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		stationIdTextItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);		
		
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);		
		
		searchForm.setFields(stationIdTextItem, codeTextItem, transactionDateItem, searchButton);
		
		gridList.setWidth(460);
		gridList.setHeight(500);
						
		gridList.hide();
		
		VLayout layout = new VLayout(10);
		layout.setLayoutTopMargin(20);
		layout.setLayoutLeftMargin(20);
		layout.setLayoutBottomMargin(20);
		
		layout.addMember(searchForm);
		layout.addMember(gridList);
		
		tab.setPane(layout);
		tabSet.addTab(tab);
		
		return tabSet;
	}
	
	private void initComponentHandler(){
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchTransaction();
				}  
		});		
	}

	protected void searchTransaction() {
		gridList.show();
		gridList.fetchData(searchForm.getValuesAsCriteria());		
	}
}
