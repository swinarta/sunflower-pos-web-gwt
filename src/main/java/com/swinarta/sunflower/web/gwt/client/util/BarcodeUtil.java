package com.swinarta.sunflower.web.gwt.client.util;

public class BarcodeUtil {

	public static String calculateCheckDigit(String str){
		int evenSumInt = Integer.parseInt(""+str.charAt(1)) + Integer.parseInt(""+str.charAt(3)) + Integer.parseInt(""+str.charAt(5)) + Integer.parseInt(""+str.charAt(7)) + Integer.parseInt(""+str.charAt(9)) + Integer.parseInt(""+str.charAt(11));
		int evenSumInt3 = evenSumInt * 3;
		int oddSumInt = Integer.parseInt(""+str.charAt(0)) + Integer.parseInt(""+str.charAt(2)) + Integer.parseInt(""+str.charAt(4)) + Integer.parseInt(""+str.charAt(6)) + Integer.parseInt(""+str.charAt(8)) + Integer.parseInt(""+str.charAt(10));
		int totalSum = evenSumInt3 + oddSumInt;
		
		int nextTen = (int) ((Math.ceil(totalSum/10)+1)*10);
		
		return String.valueOf((nextTen - totalSum)%10);		
	}
	
	public static boolean isValidBarcode(String str){
		if(str.length() == 13){
			String pre = str.substring(0, 12);
			String calculateDigit = calculateCheckDigit(pre);
			String checkDigit = str.substring(12, 13);
			return checkDigit.equalsIgnoreCase(calculateDigit); 
		}
		
		return false;
	}
	
	public static String generateBarcodeFromSku(String sku){
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("9900");
		strBuff.append(sku);
		strBuff.append(calculateCheckDigit("9900" + sku));
		return strBuff.toString();
	}
	
}
