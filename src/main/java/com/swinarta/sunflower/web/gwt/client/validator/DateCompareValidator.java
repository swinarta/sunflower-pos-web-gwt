package com.swinarta.sunflower.web.gwt.client.validator;

import java.util.Date;

import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class DateCompareValidator extends CustomValidator{

	private String otherField;
	
	private COMPARATOR comparator;

	public DateCompareValidator(){
		setStopIfFalse(true);
	}
	
	public void setOtherField(String otherField){
		this.otherField = otherField;
	}
	
	public void setComparator(COMPARATOR comparator){
		this.comparator = comparator;
	}
		
	@Override
	protected boolean condition(Object value) {
	
		if(getRecord() == null) return true;		
		Date otherFieldDate = getRecord().getAttributeAsDate(otherField);
		
		if(otherFieldDate == null) return true; 
		
		Date currDate = (Date)value;
		
		if(comparator == COMPARATOR.GREATER){
			return (currDate.compareTo(otherFieldDate) > 0);
		}else if(comparator == COMPARATOR.GREATER_OR_EQUALS){
			return (currDate.compareTo(otherFieldDate) >= 0);
		}
		
		return false;
	}
	
	public enum COMPARATOR{
		GREATER_OR_EQUALS,
		GREATER
	}

}
