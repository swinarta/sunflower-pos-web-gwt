package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ProductMeasurementsGridList extends ListGrid{

	@Override
	protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {
		
		if(record.getAttributeAsBoolean("isDefault")){
			return super.getCellCSSText(record, rowNum, colNum);
		}else{
			return "color:blue;";
		}		
	}
}
