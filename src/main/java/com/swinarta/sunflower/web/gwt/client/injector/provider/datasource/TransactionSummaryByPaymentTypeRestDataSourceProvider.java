package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.DateUtils;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.util.JSOHelper;

public class TransactionSummaryByPaymentTypeRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;

	@Inject
	public TransactionSummaryByPaymentTypeRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;

	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.FETCH){					
					Date startDate = JSOHelper.getAttributeAsDate(dsRequest.getData(), "transactionDate");					
					dsRequest.setActionURL(getFetchDataURL() + DateUtils.getIsoDateFormat().format(startDate));
					
				}
				return super.transformRequest(dsRequest);
			}			
		};
			
		ds.setFetchDataURL("/transactionsummary/paymentType/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding);

		return ds;
	}

}