package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TransactionSummaryByStationListGrid extends ListGrid{

	@Inject
	public TransactionSummaryByStationListGrid(
			@Named("TransactionSummaryByStationId") RestDataSource dataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
		setDataSource(dataSource);
		
		setDataPageSize(50);
		setCanGroupBy(false);
		setCanFreezeFields(false);
		setCanEdit(false);
		setSortField(0);
		

		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);

		ListGridField stationIdField = new ListGridField("stationId", "Station");
		ListGridField totalField = new ListGridField("total", "Total");
		ListGridField countField = new ListGridField("count", "Num. Of Transaction");

		stationIdField.setAlign(Alignment.RIGHT);
		totalField.setAlign(Alignment.RIGHT);
		countField.setAlign(Alignment.RIGHT);
		
		stationIdField.setWidth(50);
		countField.setWidth(50);

		totalField.setCellFormatter(new CellFormatter() {			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return StringFormatUtil.getFormat(record.getAttributeAsDouble("total"));
			}
		});


		setFields(stationIdField, totalField, countField);
	}

}
