package com.swinarta.sunflower.web.gwt.client.util;

import com.smartgwt.client.widgets.form.fields.FormItem;

public class FormUtil {

	public static Double getDoubleValueFromFormItem(FormItem item){
		Double retval = null;
		if(item.getValue() instanceof Number){
			retval = ((Number)item.getValue()).doubleValue();
		}else if(item.getValue() instanceof String){
			String retvalStr = (String)item.getValue();			
			retval = Double.parseDouble(retvalStr);
		}
		return retval;
	}

	public static Integer getIntegerValueFromFormItem(FormItem item){
		Integer retval = null;
		if(item.getValue() instanceof Number){
			retval = ((Number)item.getValue()).intValue();
		}else if(item.getValue() instanceof String){
			String retvalStr = (String)item.getValue();
			try{
				retval = Integer.parseInt(retvalStr);
			}catch (Exception e) {
			}
		}
		return retval;
	}

	public static Float getFloatValueFromFormItem(FormItem item){
		Float retval = null;
		if(item.getValue() instanceof Number){
			retval = ((Number)item.getValue()).floatValue();
		}else if(item.getValue() instanceof String){
			String retvalStr = (String)item.getValue();			
			retval = Float.parseFloat(retvalStr);
		}
		return retval;
	}

	public static Boolean getBooleanValueFromFormItem(FormItem item){
		Boolean retval = null;
		if(item.getValue() instanceof Boolean){
			retval = (Boolean)item.getValue();
		}else if(item.getValue() instanceof String){
			String retvalStr = (String)item.getValue();			
			retval = Boolean.parseBoolean(retvalStr);
		}
		return retval;
	}

	public static String getStringValueFromFormItem(FormItem item){
		String retval = null;
		if(item.getValue() instanceof String){
			retval = (String)item.getValue();			
		}
		return retval;
	}

}
