package com.swinarta.sunflower.web.gwt.client.forms;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class ReturnSupplierForm extends DynamicForm{

	final private CustomComboBoxItem supplierComboBoxItem;
	final private TextAreaItem remarksItem = new TextAreaItem("remarks", "Remarks");
	final private TextItem invoiceNumberTextItem = new TextItem("invoiceNumber", "Invoice Number");
	
	private DSCallback saveCallback;
	
	@Inject
	public ReturnSupplierForm(
			@Named("Supplier") CustomComboBoxItem supplierComboBoxItem,
			@Named("ReturnSuppliers") RestDataSource retSuppsDataSource
			){
		this.supplierComboBoxItem = supplierComboBoxItem;
		
		setTitleOrientation(TitleOrientation.TOP);

		setDataSource(retSuppsDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);

		invoiceNumberTextItem.setCharacterCasing(CharacterCasing.UPPER);
		
		supplierComboBoxItem.setRequired(true);
		invoiceNumberTextItem.setRequired(true);
						
		setFields(supplierComboBoxItem, invoiceNumberTextItem, remarksItem);
	}
	
	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void editNewRecord(){
		clearErrors(true);
		supplierComboBoxItem.show();
		supplierComboBoxItem.focusInItem();		
		super.editNewRecord();
	}
	
	public void editRecord(Record record){
		clearErrors(true);
		supplierComboBoxItem.hide();
				
		if("NEW".equalsIgnoreCase(record.getAttributeAsString("status")) || "PROCESS".equalsIgnoreCase(record.getAttributeAsString("status"))){

		}else{

		}
		
		super.editRecord(record);
	}
		
	public void saveData(){
		super.saveData(saveCallback);
	}	
	
}
