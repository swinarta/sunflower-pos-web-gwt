package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.widget.CustomSectionStack;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.ProductTabSet;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.types.VisibilityMode;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickEvent;
import com.smartgwt.client.widgets.layout.events.SectionHeaderClickHandler;

public class RelatedProductCanvas extends VLayout{

	private SectionStack relatedProductSectionStack = new SectionStack();
	private Boolean hasLoadRelatedProduct;
	
	private Integer productId;
	private Integer categoryId;
	private String description;

	final private ProductListGrid productGrid;

	@Inject
	public RelatedProductCanvas(
			ProductListGrid productGrid,
			final ProductTabSet tabSet
		){

		this.productGrid = productGrid;
		
		setGroupTitle("Related Product Information");
		setIsGroup(true);
		setCanSelectText(true);
		setMargin(3);
				
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutBottomMargin(8);
		setLayoutRightMargin(8);
		
		setHeight(50);

		relatedProductSectionStack.setVisibilityMode(VisibilityMode.MULTIPLE);
		
		CustomSectionStack section1 = new CustomSectionStack();		
		section1.setExpanded(false);
		section1.addItem(productGrid);
		
		relatedProductSectionStack.addSection(section1);
		section1.registerState();
		
		hasLoadRelatedProduct = false;
		
		
		relatedProductSectionStack.addSectionHeaderClickHandler(new SectionHeaderClickHandler() {
			
			public void onSectionHeaderClick(SectionHeaderClickEvent event) {
				
				CustomSectionStack section = (CustomSectionStack)event.getSection();
								
				if(section.isExpand()){
					setHeight(250);

					if(!hasLoadRelatedProduct){
						hasLoadRelatedProduct = true;
						fetchRelatedProductData(productId, categoryId, description);									
					}

				}else{
					setHeight(50);
				}
								
			}
		});		

		productGrid.addDoubleClickHandler(new DoubleClickHandler() {			
			public void onDoubleClick(DoubleClickEvent event) {
				ProductListGrid listGrid = (ProductListGrid)event.getSource();				
				tabSet.loadProduct(listGrid.getSelectedRecord());
			}
		});
				
		addMember(relatedProductSectionStack);
	}
		
	public void fetchRelatedProductData(Integer productId, Integer categoryId, String description){
						
		Criteria c = new Criteria();
		c.addCriteria("actionId", "searchRelated");
		c.addCriteria("description", description);
		c.addCriteria("categoryId", categoryId);
		c.addCriteria("productId", productId);
		
		productGrid.fetchData(c);
		
		hasLoadRelatedProduct = true;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}