package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ProductListGrid extends ListGrid{

	@Inject
	public ProductListGrid(
			@Named("Products") RestDataSource dataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
		setDataSource(dataSource);
		
		setDataPageSize(50);
		setCanGroupBy(false);
		setCanFreezeFields(false);
		setCanEdit(false);
		setSortField(2);

		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);

		ListGridField deletedField = new ListGridField("deleteInd");
		deletedField.setHidden(true);

		ListGridField skuField = new ListGridField("sku");
		skuField.setWidth(75);
		
		ListGridField barcodeField = new ListGridField("barcode");
		barcodeField.setWidth(100);
		
		ListGridField descriptionField = new ListGridField("longDescription");
		setFields(skuField, barcodeField, descriptionField);
	}

	@Override
	protected String getCellCSSText(ListGridRecord record, int rowNum, int colNum) {

		if(record.getAttributeAsBoolean("deleteInd")){
			return "color:red;";
		}else{
			return super.getCellCSSText(record, rowNum, colNum);
		}		
	}

}
