package com.swinarta.sunflower.web.gwt.client;

public interface IConstants {

	public static String NUMERIC_REGEXP = "[0-9.]";
	public static String REST_DS = "/sunflower/rest/seed";
	
	public static Integer PRINT_MODE_BARCODE_WITHOUT_PRICE = 1;
	public static Integer PRINT_MODE_BARCODE_WITH_PRICE = 2;	
	public static Integer PRINT_MODE_SHELVING = 3;
}
