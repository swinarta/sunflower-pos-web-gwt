package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.LinkItem;

public class CancelPuchasingOrderLinkItemProvider implements Provider<LinkItem>{

	public LinkItem get() {
		LinkItem link = new LinkItem("cancelPo");

		link.setTarget("javascript");
		link.setShowTitle(false);
		link.setLinkTitle("Cancel >>");

		return link;
	}

}
