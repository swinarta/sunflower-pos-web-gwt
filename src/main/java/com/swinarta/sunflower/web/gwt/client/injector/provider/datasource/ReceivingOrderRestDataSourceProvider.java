package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.types.DSOperationType;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.util.JSOHelper;

public class ReceivingOrderRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	
	@Inject
	public ReceivingOrderRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource(){
			@Override
			public Object transformRequest(DSRequest dsRequest){
				if(dsRequest.getOperationType() == DSOperationType.FETCH){
					String actionId = JSOHelper.getAttribute(dsRequest.getData(), "actionId");
					if("searchByPo".equalsIgnoreCase(actionId)){
						Integer poId = JSOHelper.getAttributeAsInt(dsRequest.getData(), "poId");
						dsRequest.setActionURL(getFetchDataURL()+"po/" + poId);				
					}else if("searchByCode".equalsIgnoreCase(actionId)){
						String code = JSOHelper.getAttribute(dsRequest.getData(), "code");
						dsRequest.setActionURL(getFetchDataURL()+"code/" + code);				
					}
				}
				return super.transformRequest(dsRequest);
			}
		};
		ds.setFetchDataURL("/receivingorder/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding);

		DataSourceTextField roIdTextField = new DataSourceTextField("roId", "RO NUMBER");
		roIdTextField.setHidden(true);
		
		DataSourceDateField roDateTextField = new DataSourceDateField("roDate", "Receive Date");
		roDateTextField.setRequired(true);
		roDateTextField.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		DataSourceTextField statusTextField = new DataSourceTextField("status", "STATUS");
		statusTextField.setHidden(true);

		DataSourceTextField remarksTextField = new DataSourceTextField("remarks", "Remarks");

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);
						
		ds.setFields(idTextField, roIdTextField, roDateTextField, remarksTextField, statusTextField);

		return ds;
	}

}