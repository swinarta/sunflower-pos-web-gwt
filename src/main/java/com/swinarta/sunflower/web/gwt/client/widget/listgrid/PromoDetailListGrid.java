package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import java.util.Date;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.PriceUtil;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.ListGridSummaryField;

public class PromoDetailListGrid extends ListGrid implements AddProductToDetail{

	private ListGridField skuField = new ListGridField("sku", "SKU");
	private ListGridField barcodeField = new ListGridField("barcode", "Barcode");
	private ListGridField descriptionField = new ListGridField("desc", "Desc.");
	private ListGridField measurementField = new ListGridField("sellingMeasurement", "Measurement");
	private ListGridField sellingPriceField = new ListGridField("sellingPrice", "Selling Price");
	private ListGridSummaryField promoSellingPriceField = new ListGridSummaryField("promoSellingPrice", "Promotion Price");
	
	final private RestDataSource promoDetailsDataSource;
	final private RestDataSource promoDataSource;
	
	private Record promoRecord;
		
	@Inject
	public PromoDetailListGrid(
		@Named("PromoDetails") RestDataSource promoDetailsDataSource,
		@Named("Promo") RestDataSource promoDataSource
		){
		
		this.promoDetailsDataSource = promoDetailsDataSource;
		this.promoDataSource = promoDataSource;
		
		setCanEdit(false);
		setAnimateRemoveRecord(true);
		setHeaderHeight(44);
		setDataSource(promoDetailsDataSource);
		setCanRemoveRecords(true);		
		
		final ListGrid list = this;

		skuField.setAlign(Alignment.CENTER);
		skuField.setWidth(65);
		skuField.setCellFormatter(new CellFormatter() {			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				String sku = productRecord.getAttribute("sku");
				list.getRecord(rowNum).setAttribute("sku", sku);
				return sku;
			}
		});		

		barcodeField.setAlign(Alignment.CENTER);
		barcodeField.setWidth(90);
		barcodeField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				String barcode = productRecord.getAttribute("barcode");				
				list.getRecord(rowNum).setAttribute("barcode", barcode);
				return barcode;
			}
		});
		
		descriptionField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				return productRecord.getAttribute("longDescription");
			}
		});

		measurementField.setAlign(Alignment.CENTER);
		measurementField.setWidth(60);
		measurementField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				Record sellingRecord = productRecord.getAttributeAsRecord("selling");
				Record sellingMeasurementRecord = sellingRecord.getAttributeAsRecord("measurement");
				Record productMeasurementRecord = productRecord.getAttributeAsRecord("productMeasurement");
				
				Boolean isMutable = sellingMeasurementRecord.getAttributeAsBoolean("mutable");
				
				Integer measurementQty;
				
				if(!isMutable){
					measurementQty = sellingMeasurementRecord.getAttributeAsInt("defaultQty");
				}else{
					measurementQty = getMeasurementQty(productMeasurementRecord, sellingMeasurementRecord);
				}
								
				return sellingMeasurementRecord.getAttribute("code") + " (" + measurementQty + ")";
			}
		});

		sellingPriceField.setAlign(Alignment.RIGHT);
		sellingPriceField.setWidth(140);
		
		sellingPriceField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				Record sellingRecord = productRecord.getAttributeAsRecord("selling");				
				return StringFormatUtil.getFormat(sellingRecord.getAttributeAsDouble("sellingPrice"));
			}
		});

		promoSellingPriceField.setAlign(Alignment.RIGHT);
		promoSellingPriceField.setWidth(140);
		promoSellingPriceField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
				Record productRecord = record.getAttributeAsRecord("product");
				Record sellingRecord = productRecord.getAttributeAsRecord("selling");				
				
				String promoType = promoRecord.getAttribute("promoType");
				Double promoValue = promoRecord.getAttributeAsDouble("promoValue");
				
				Double sellingPrice = sellingRecord.getAttributeAsDouble("sellingPrice");
				return StringFormatUtil.getFormat(PriceUtil.calculatePromoPrice(sellingPrice, promoType, promoValue));
			}
		});
		
		
		setHeaderSpans(
				new HeaderSpan("Product", new String[]{"sku", "barcode", "desc"}),
				new HeaderSpan("Selling", new String[]{"sellingMeasurement", "sellingPrice", "promoSellingPrice"})
				);
		
		setFields(skuField, barcodeField, descriptionField,
				measurementField, sellingPriceField, promoSellingPriceField);

	}
	
	public void setPromoRecord(Record promoRecord) {
		this.promoRecord = promoRecord;
	}

	private Integer getMeasurementQty(Record productMeasurementRecord, Record buyingMeasurementRecord){
		
		Record measurementFromPmRecord = productMeasurementRecord.getAttributeAsRecord("measurement");		
		Integer measurementQty = buyingMeasurementRecord.getAttributeAsInt("defaultQty");
		
		if(productMeasurementRecord != null && measurementFromPmRecord != null){
			if(measurementFromPmRecord.getAttributeAsInt("id").intValue() == buyingMeasurementRecord.getAttributeAsInt("id").intValue()){
				measurementQty = productMeasurementRecord.getAttributeAsInt("overrideQty");
			}			
		}
		
		return measurementQty;
	}

	public void addProduct(Integer productId, Float qty) {
		Integer promoId = promoRecord.getAttributeAsInt("id");
		final Record record = new Record();
		record.setAttribute("promoId", promoId);
		record.setAttribute("productId", productId);

		Date startDate = promoRecord.getAttributeAsDate("startDate");
		Date endDate = promoRecord.getAttributeAsDate("endDate");

		final ListGrid list = this;

		checkPromotion(productId, startDate, endDate, new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData().length > 0){
					Integer id = response.getData()[0].getAttributeAsInt("id");
					SC.warn("Promo Found for this product in this Promo period: Promo #" + id);
				}else{
					//no promotion found
					promoDetailsDataSource.addData(record, new DSCallback() {
						
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							DeferredCommand.addCommand(new Command() {
								public void execute() {
									list.scrollToRow(list.getTotalRows()-1);
								}
							});
						}
					});					
				}
			}
		});
				
	}
	
	protected void checkPromotion(Integer productId, Date startDate, Date endDate, 
			final DSCallback resultCallback){
		
		Criteria criteria = new Criteria();
		criteria.addCriteria("productId", productId);
		criteria.addCriteria("startDate", startDate);
		criteria.addCriteria("endDate", endDate);
		criteria.addCriteria("actionId", "checkPromo");
		
		promoDataSource.fetchData(criteria, resultCallback);
		
	}

	public int indexOfProductExistInDetail(String text) {
		String propertyToSearch;
		RecordList recordList = getDataAsRecordList();
		int index;
		
		if(text.length() > 10){
			propertyToSearch = "barcode";
		}else{
			propertyToSearch = "sku";
		}
		
		index = recordList.findIndex(propertyToSearch, text);
		
		return index;
	}

	public void productExist(int index) {
		selectSingleRecord(index);
	}
}
