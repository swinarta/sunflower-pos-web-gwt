package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.LinkItem;

public class DownloadAsPdfPurchasingOrderLinkItemProvider implements Provider<LinkItem>{

	public LinkItem get() {
		LinkItem link = new LinkItem("pdfPo");

		link.setShowTitle(false);
		link.setLinkTitle("PDF >>");

		return link;
	}

}
