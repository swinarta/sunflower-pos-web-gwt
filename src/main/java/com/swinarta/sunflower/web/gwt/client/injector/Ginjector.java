package com.swinarta.sunflower.web.gwt.client.injector;

import com.google.gwt.core.client.GWT;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;

public class Ginjector {

	private static GardenGinjector gardenGinjector;
	
	public static GardenGinjector getInstance(){
		if(gardenGinjector == null){
			gardenGinjector = GWT.create(GardenGinjector.class);
		}
		return gardenGinjector;
	}
	
}