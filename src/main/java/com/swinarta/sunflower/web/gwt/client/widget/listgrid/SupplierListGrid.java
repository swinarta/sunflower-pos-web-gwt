package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;

public class SupplierListGrid extends ListGrid{

	@Inject
	public SupplierListGrid(
			@Named("Supplier") RestDataSource supplierDataSource
		){

		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);

		ListGridField supplierCodeField = new ListGridField("supplierCode");
		supplierCodeField.setWidth(75);
		
		ListGridField nameField = new ListGridField("name");
		nameField.setWidth(400);

		ListGridField contactField = new ListGridField("contactName");
		contactField.setWidth(150);

		setFields(supplierCodeField, nameField, contactField);

		setDataSource(supplierDataSource);
		setSortField(2);
		setDataPageSize(50);
		setCanGroupBy(false);
		setCanFreezeFields(false);
		setCanEdit(false);

		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
				
	}
}
