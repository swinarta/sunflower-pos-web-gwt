package com.swinarta.sunflower.web.gwt.client.injector.provider.datasource;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.OperationBinding;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;

public class ProductWithStocksRestDataSourceProvider implements Provider<RestDataSource>{

	final private OperationBinding fetchOperationBinding;
	
	@Inject
	public ProductWithStocksRestDataSourceProvider(
			@Named("GetFetch") OperationBinding fetchOperationBinding
			){
		this.fetchOperationBinding = fetchOperationBinding;
	}
	
	public RestDataSource get() {
		RestDataSource ds = new SeedRestDataSource();
		ds.setFetchDataURL("/stocksummary/");
		ds.setDataFormat(DSDataFormat.JSON);
		
		ds.setOperationBindings(fetchOperationBinding);

		DataSourceIntegerField idTextField = new DataSourceIntegerField("id", "ID");
		idTextField.setPrimaryKey(true);
		idTextField.setHidden(true);

		DataSourceTextField skuTextField = new DataSourceTextField("sku", "SKU");		

		DataSourceTextField barcodeTextField = new DataSourceTextField("barcode", "Barcode");		
		
		DataSourceTextField descriptionTextField = new DataSourceTextField("longDescription", "Long Description");

		DataSourceTextField shortDescriptionTextField = new DataSourceTextField("shortDescription", "Short Description");

		DataSourceBooleanField consignmentBooleanField = new DataSourceBooleanField("consignment", "Consignment");

		DataSourceBooleanField scallableBooleanField = new DataSourceBooleanField("scallable", "Scallable");
										
		ds.setFields(idTextField, skuTextField, barcodeTextField, descriptionTextField, shortDescriptionTextField, consignmentBooleanField, scallableBooleanField);

		return ds;
	}

}