package com.swinarta.sunflower.web.gwt.client.widget;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.forms.AddProductToDetailForm;
import com.swinarta.sunflower.web.gwt.client.forms.SearchProductByTextForm;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.AddProductToDetail;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.ProductListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

public class AddProductToDetailWindow extends Window{
	
	final private LinkItem showHideLinkItem = new LinkItem("showHide");
	final VLayout layout1 = new VLayout();
	
	final private AddProductToDetailForm addProductToDetailForm;
	final private ProductListGrid productListGrid;
	final private SearchProductByTextForm searchProductByTextForm;
	final private RestDataSource searchProductDataSource;
	final private RestDataSource buyingDataSource;
	final private RestDataSource productMeasurementDataSource;
	
	private Integer supplierId;
	private AddProductToDetail addProductToDetail;
	private Boolean showQty;
	private Boolean hasSelling;

	@Inject
	public AddProductToDetailWindow(
		final AddProductToDetailForm addProductToDetailForm,
		final SearchProductByTextForm searchProductByTextForm,
		ProductListGrid productListGrid,
		@Named("Products") RestDataSource searchProductDataSource,
		@Named("Buying") RestDataSource buyingDataSource,
		@Named("ProductMeasurement") RestDataSource productMeasurementDataSource
		){

		this.addProductToDetailForm = addProductToDetailForm;
		this.productListGrid = productListGrid;
		this.searchProductByTextForm = searchProductByTextForm;
		this.searchProductDataSource = searchProductDataSource;
		this.buyingDataSource = buyingDataSource;
		this.productMeasurementDataSource = productMeasurementDataSource;

		setTitle("Add Product");
		setShowMinimizeButton(false);
		centerInPage();
		setShowCloseButton(true);
		setDismissOnEscape(true);
		setAutoSize(true);
		
		productListGrid.setHeight(200);
		productListGrid.setWidth(500);


		DynamicForm showHideForm = new DynamicForm();
		
		showHideLinkItem.setShowTitle(false);
		
		showHideForm.setFields(showHideLinkItem);
		showHideForm.setWidth(150);
		
		layout1.addMember(searchProductByTextForm);
		layout1.addMember(productListGrid);		
		layout1.setLayoutTopMargin(5);
		layout1.setLayoutLeftMargin(5);
		layout1.setLayoutRightMargin(5);
		layout1.setLayoutBottomMargin(5);		
		
		layout1.hide();
		
		VLayout layout2 = new VLayout();
		layout2.addMember(showHideForm);
		layout2.setHeight(12);
		layout2.setLayoutTopMargin(5);
		layout2.setLayoutLeftMargin(5);
		layout2.setLayoutRightMargin(5);
		layout2.setLayoutBottomMargin(5);
		
		VLayout layout3 = new VLayout();
		layout3.addMember(addProductToDetailForm);
		layout3.setHeight(12);
		layout3.setLayoutTopMargin(5);
		layout3.setLayoutLeftMargin(5);
		layout3.setLayoutRightMargin(5);
		layout3.setLayoutBottomMargin(5);
		
		addItem(layout1);
		addItem(layout2);
		addItem(layout3);
		
		initHandler();
		showHideLinkItem.setValue("Find By Description (Show)");
	}

	public void show(){
		if(layout1.isVisible()){
			searchProductByTextForm.focus();
		}else{
			addProductToDetailForm.focus();
		}
		super.show();
	}
	
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}
	
	public void setHasSelling(Boolean hasSelling) {
		this.hasSelling = hasSelling;
	}

	public void setShowQty(Boolean showQty) {
		this.showQty = showQty;
		addProductToDetailForm.setShowQty(showQty);
	}
	
	public void setAddProductToDetail(AddProductToDetail addProductToDetail) {
		this.addProductToDetail = addProductToDetail;
	}

	protected void initHandler(){
		searchProductByTextForm.getItem("searchProduct").addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					searchProductByDesc();
				}
			}
		});
		
		productListGrid.addDoubleClickHandler(new DoubleClickHandler() {
			public void onDoubleClick(DoubleClickEvent event) {
				ListGrid listGrid = (ListGrid)event.getSource();
				ListGridRecord record = listGrid.getSelectedRecord();
				String sku = record.getAttribute("sku");
				addProductToDetailForm.getItem("searchProduct").setValue(sku);
				searchProductToAdd();
			}
		});

		showHideLinkItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				LinkItem item = (LinkItem)event.getSource();
				if(layout1.isVisible()){
					layout1.hide();
					addProductToDetailForm.focus();
					item.setValue("Find By Description (Show)");
				}else{
					layout1.show();
					searchProductByTextForm.focusInItem("searchProduct");
					item.setValue("Find By Description (Hide)");
				}
			}
		});
		
		addProductToDetailForm.getItem("searchProduct").addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					searchProductToAdd();
				}
			}
		});
		
		addProductToDetailForm.getItem("qty").addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					addProductToDetail();
				}				
			}
		});
		
	}

	protected void addProductToDetail() {
		Integer productId = FormUtil.getIntegerValueFromFormItem(addProductToDetailForm.getItem("productId"));
		if(showQty != null && showQty){
			Float qty = FormUtil.getFloatValueFromFormItem(addProductToDetailForm.getItem("qty"));
			if(qty == null) return;
			if(qty > 0){
				addProductToDetail.addProduct(productId, qty);
			}		
		}else{
			addProductToDetail.addProduct(productId, null);
		}
		addProductToDetailForm.initState();
	}

	protected void searchProductToAdd() {
		String text = addProductToDetailForm.getValueAsString("searchProduct");
		int index = addProductToDetail.indexOfProductExistInDetail(text);
		
		if(index > -1){
			addProductToDetailForm.initState();
			addProductToDetail.productExist(index);
		}else{
			Criteria criteria = new Criteria();
			criteria.addCriteria(addProductToDetailForm.getValuesAsCriteria());
			criteria.addCriteria("supplierId", supplierId);
			criteria.addCriteria("deleteInd", false);
			criteria.addCriteria("hasSelling", hasSelling);
			
			if(text != null && text.length() > 2){			
				searchProductDataSource.fetchData(criteria, new DSCallback() {					
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getTotalRows() > 0){
							
							Integer productId = response.getData()[0].getAttributeAsInt("id");
							final Record productRecord = response.getData()[0];
							
							if(showQty != null && showQty){
								Criteria criteria = new Criteria();
								criteria.addCriteria("id", productId);
								buyingDataSource.fetchData(criteria, new DSCallback() {								
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										productFound(productRecord, response.getData()[0]);
									}
								});
							}else{
								productFound(productRecord, null);
								addProductToDetail();
							}
						}else{
							productNotFound();
						}
					}
				});
			}			
		}
				
	}

	private void productNotFound(){		
		SC.warn("Product Not Found", new BooleanCallback() {			
			public void execute(Boolean value) {
				addProductToDetailForm.initState();
			}
		});
	}

	private void productFound(Record prodRecord, Record buyingRecord){
		
		Integer productId = prodRecord.getAttributeAsInt("id");
		
		if(buyingRecord != null){
			Integer measurementQty;
			final String measurement = (String)buyingRecord.getAttributeAsMap("measurement").get("code");
			Integer measurementId = (Integer)buyingRecord.getAttributeAsMap("measurement").get("id");
			Boolean isMutable = (Boolean)buyingRecord.getAttributeAsMap("measurement").get("mutable");
			final Integer defaultQty = (Integer)buyingRecord.getAttributeAsMap("measurement").get("defaultQty");
			
			if(!isMutable){
				measurementQty = defaultQty;
				String measurementStr = measurement + " (" + measurementQty + ")";
				addProductToDetailForm.getItem("measurement").setValue(measurementStr);
			}else{
				Criteria criteria = new Criteria();
				criteria.addCriteria("productId", productId);
				criteria.addCriteria("measurementId", measurementId);
				productMeasurementDataSource.fetchData(criteria, new DSCallback() {					
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Integer measurementQty;
						if(response.getData().length > 0){
							Record measurementRecord = response.getData()[0];
							measurementQty = measurementRecord.getAttributeAsInt("overrideQty");
						}else{
							measurementQty = defaultQty;
						}
						String measurementStr = measurement + " (" + measurementQty + ")";
						addProductToDetailForm.getItem("measurement").setValue(measurementStr);
					}
				});
			}			
		}
				
		addProductToDetailForm.getItem("prodDesc").setValue(prodRecord.getAttributeAsString("longDescription"));
		addProductToDetailForm.getItem("productId").setValue(productId);
		addProductToDetailForm.getItem("searchProduct").disable();
		addProductToDetailForm.getItem("qty").enable();
		addProductToDetailForm.focus();
	}

	protected void searchProductByDesc() {
		String text = searchProductByTextForm.getValueAsString("searchProduct");
		if((text != null && text.length() > 2)){
			Criteria criteria = new Criteria();
			criteria.addCriteria(searchProductByTextForm.getValuesAsCriteria());
			criteria.addCriteria("supplierId", supplierId);
			criteria.addCriteria("deleteInd", false);
			criteria.addCriteria("hasSelling", hasSelling);
			productListGrid.fetchData(criteria);
		}
	}
	
}
