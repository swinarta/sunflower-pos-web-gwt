package com.swinarta.sunflower.web.gwt.client.widget.listgrid;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class TransactionListGrid extends ListGrid{

	@Inject
	public TransactionListGrid(
			@Named("Transaction") RestDataSource dataSource
			){
		
		setShowRecordComponents(true);
		setShowRecordComponentsByCell(true);
		setEmptyCellValue("--");
		setAlternateRecordStyles(true);
		setDataSource(dataSource);
		
		setDataPageSize(50);
		setCanGroupBy(false);
		setCanFreezeFields(false);
		setCanEdit(false);
		setSortField(0);		

		ListGridField idField = new ListGridField("id");
		idField.setHidden(true);

		ListGridField stationIdField = new ListGridField("stationId", "Station");
		ListGridField codeField = new ListGridField("code", "Transaction Code");
		ListGridField transactionDateField = new ListGridField("transactionDate", "Transaction Date");
		ListGridField totalField = new ListGridField("total", "Total");
				
		codeField.setAlign(Alignment.RIGHT);
		stationIdField.setAlign(Alignment.RIGHT);
		transactionDateField.setAlign(Alignment.CENTER);
		totalField.setAlign(Alignment.RIGHT);
		
		stationIdField.setWidth(50);
		codeField.setWidth(120);
		transactionDateField.setWidth(150);
		totalField.setWidth(120);

		transactionDateField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return StringFormatUtil.getLongDateFormat(value);
			}
		});

		totalField.setCellFormatter(new CellFormatter() {
			
			public String format(Object value, ListGridRecord record, int rowNum, int colNum) {				
				return StringFormatUtil.getFormat(record.getAttributeAsDouble("total"));
			}
		});

		setFields(codeField, transactionDateField, totalField, stationIdField);
	}

}
