package com.swinarta.sunflower.web.gwt.client.widget.tabset;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.History;
import com.swinarta.sunflower.web.gwt.client.canvas.ReceivingOrderDetailCanvas;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

public class ReceivingOrderTabSet extends TabSet{

	public ReceivingOrderTabSet(){
		setTabBarPosition(Side.TOP);  
		setTabBarAlign(Side.LEFT);
		
		addTabSelectedHandler(new TabSelectedHandler() {
			
			public void onTabSelected(TabSelectedEvent event) {
				Tab selectedTab = event.getTab();				
				History.newItem("ro-"+ selectedTab.getID());
			}
		});
		
	}

	public void loadRO(final Record record){
		
		SC.showPrompt("Loading Receiving Order ... ");
		
		DeferredCommand.addCommand(new Command() {
			
			public void execute() {
				String tabId = "ro"+record.getAttribute("id");
				if(getTab(tabId) == null){
					String title = record.getAttribute("roId");

					GardenGinjector injector = Ginjector.getInstance();
					ReceivingOrderDetailCanvas receivingOrderDetailCanvas = injector.getReceivingOrderCanvas();
					
					Tab tab = new Tab(title);
					tab.setCanClose(true);
					tab.setPane(receivingOrderDetailCanvas);
					tab.setID(tabId);
					
					receivingOrderDetailCanvas.loadBasicData(record);
					
					addTab(tab);			
				}
				
				selectTab(tabId);
				
				SC.clearPrompt();
			
			}
		});
	}
	
}
