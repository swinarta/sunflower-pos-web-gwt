package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.tab;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.tab.Tab;

public class SearchProductTabProvider implements Provider<Tab>{

	public Tab get() {
		Tab tabSearch = new Tab("Search Product", "icons/green/16/search.png");
		tabSearch.setCanClose(false);
		tabSearch.setID("searchprod");
		return tabSearch;
	}

}
