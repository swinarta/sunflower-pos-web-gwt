package com.swinarta.sunflower.web.gwt.client.widget;

import java.util.Map;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public abstract class DataCanvas extends VLayout{
	
	protected Layout dataComposite;
	protected Label notFoundLabel = new Label("Data not found");
	protected Layout notFoundComposite = new Layout();
	protected Layout addLinkComposite = new Layout();
	
	protected DynamicForm addForm = new DynamicForm();
	protected DynamicForm editForm = new DynamicForm();
	
	protected LinkItem addItemLink = new LinkItem("add");
	protected LinkItem editItemLink = new LinkItem("edit");
	
	protected Window addOrUpdateWindow = new Window();
	protected Button saveButton = new Button("Save");
	
	protected onCompleteCallback onCompleteCallback = null;
	
	public void setOnCompleteCallback(onCompleteCallback onCompleteCallback) {
		this.onCompleteCallback = onCompleteCallback;
	}

	public void setShowAddLink(Boolean b){
		if(b){
			addForm.show();
		}else{
			addForm.hide();
		}		
	}

	public void setShowEditLink(Boolean b){
		if(b){
			editForm.show();
		}else{
			editForm.hide();
		}
	}

	public DataCanvas(){
		this(DataCanvasLayout.HORIZONTAL);
	}
	
	public DataCanvas(DataCanvasLayout layout){
		setIsGroup(true);
		setCanSelectText(true);
		
		if(layout == DataCanvasLayout.VERTICAL){
			dataComposite = new VLayout();
		}else{
			dataComposite = new HLayout();
		}

		addItemLink.setTarget("javascript");
		addItemLink.setShowTitle(false);
		addItemLink.setLinkTitle("Add >>");
		addItemLink.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				addLinkClicked();
			}
		});

		editItemLink.setTarget("javascript");
		editItemLink.setShowTitle(false);
		editItemLink.setLinkTitle("Edit >>");
		editItemLink.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				editLinkClicked();
			}
		});

		initButton();
		initWindow();		
		
		addForm.setAlign(Alignment.CENTER);
		addForm.setFields(addItemLink);
		
		editForm.setAlign(Alignment.CENTER);
		editForm.setFields(editItemLink);

		addLinkComposite.addMember(addForm);
		
		notFoundComposite.addMember(notFoundLabel);
		notFoundComposite.setAlign(Alignment.CENTER);
		notFoundComposite.setHeight100();

		addMember(dataComposite);
		addMember(notFoundComposite);
		addMember(addLinkComposite);
		
		notFoundComposite.hide();
		addLinkComposite.hide();
	}

	private void initButton(){
		saveButton.setWidth100();
		
		saveButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				buttonWindowClicked();
			}
		});
		
	}
	
	private void initWindow(){
		addOrUpdateWindow.setShowMinimizeButton(false);
		addOrUpdateWindow.setIsModal(true);
		addOrUpdateWindow.setShowModalMask(true);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.setShowCloseButton(true);
	}
	
	protected void customizeWindow(String title, Integer width, Integer height){
		addOrUpdateWindow.setTitle(title);
		addOrUpdateWindow.setWidth(width);
		addOrUpdateWindow.setHeight(height);		
	}
	
	protected void setForm(DynamicForm form){
		addOrUpdateWindow.addItem(form);
		addOrUpdateWindow.addItem(saveButton);		
	}

	protected void loadDataNotFound(onCompleteCallback callback, Map<String, Object> attributes) {
		loadDataNotFound();
		callback.loadComplete(attributes);
	}
	
	protected void loadDataNotFound() {
		dataComposite.hide();
		notFoundComposite.show();
		addLinkComposite.show();
	}

	protected void loadDataFound(onCompleteCallback callback, Map<String, Object> attributes) {
		loadDataFound();
		callback.loadComplete(attributes);
	}
	
	protected void loadDataFound() {
		dataComposite.show();
		notFoundComposite.hide();
		addLinkComposite.hide();
	}

	protected void editLinkClicked() {
	}

	protected void addLinkClicked(){
	}
	
	protected void buttonWindowClicked(){
		
	}
	
	public interface onCompleteCallback{
		public void loadComplete(Map<String, Object> attributes);
	}

	public enum DataCanvasLayout{
		HORIZONTAL, VERTICAL;
	}
}