package com.swinarta.sunflower.web.gwt.client.canvas;

import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.callback.OnPrintBarcodeActionCallback;
import com.swinarta.sunflower.web.gwt.client.forms.PurchasingOrderForm;
import com.swinarta.sunflower.web.gwt.client.widget.DataCanvas;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLabel;
import com.swinarta.sunflower.web.gwt.client.widget.DisplayLink;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.JSOHelper;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;

public class PurchasingOrderBasicCanvas extends DataCanvas{

	private DisplayLabel poCode = new DisplayLabel("PO Code");
	private DisplayLabel status = new DisplayLabel("Status");
	private DisplayLabel poDate = new DisplayLabel("PO Date");
	private DisplayLabel cancelDate = new DisplayLabel("Cancel Date");
	private DisplayLabel remarks = new DisplayLabel("Remarks");
	private DisplayLink supplier = new DisplayLink("Supplier");
		
	final private Window supplierWindow;
	final private PurchasingOrderForm poForm;
	final private SupplierBasicCanvas suppCanvas;
	final private LinkItem processLinkItem;
	final private LinkItem cancelLinkItem;
	final private LinkItem downloadAsPdfLinkItem;
	final private LinkItem downloadAsTextLinkItem;
	final private LinkItem printBarcodeLinkItem;	
	
	private Record record;
	private ListGrid listGrid;
	private DSCallback updateStatusCallback;
	private DSCallback cancelCallBack;
	private DSCallback processCallBack;
	private OnPrintBarcodeActionCallback printBarcodeCallback;
	
	@Inject
	public PurchasingOrderBasicCanvas(
			SupplierBasicCanvas suppCanvas,
			PurchasingOrderForm poForm,
			@Named("DisplaySupplier") final Window supplierWindow,
			@Named("ProcessPurchasingOrder") LinkItem processLinkItem,
			@Named("CancelPurchasingOrder") LinkItem cancelLinkItem,
			@Named("DownloadAsPdf") LinkItem downloadAsPdfLinkItem,
			@Named("DownloadAsText") LinkItem downloadAsTextLinkItem,
			@Named("PrintBarcode") LinkItem printBarcodeLinkItem
		){
		super(DataCanvasLayout.VERTICAL);
		
		this.poForm = poForm;
		this.supplierWindow = supplierWindow;
		this.suppCanvas = suppCanvas;
		this.processLinkItem = processLinkItem;
		this.cancelLinkItem = cancelLinkItem;
		this.downloadAsPdfLinkItem = downloadAsPdfLinkItem;
		this.downloadAsTextLinkItem = downloadAsTextLinkItem;
		this.printBarcodeLinkItem = printBarcodeLinkItem;

		setGroupTitle("Purchasing Order Information");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setMargin(3);
				
		setForm(poForm);
		
		final DSCallback saveCallback = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == RPCResponse.STATUS_SUCCESS){
					Record r = ((Record[])response.getData())[0];
					loadData(r);
					addOrUpdateWindow.hide();
					updateStatusCallback.execute(response, rawData, request);
				}
				
			}
		};
		
		cancelCallBack = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Purchasing Order is successfuly CANCELLED");
				saveCallback.execute(response, rawData, request);
			}
		};
		
		processCallBack = new DSCallback() {
			
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				SC.say("Purchasing Order is successfuly PROCESSED");
				saveCallback.execute(response, rawData, request);
			}
		};

		suppCanvas.setIsGroup(false);
		suppCanvas.setShowAddLink(false);
		suppCanvas.setShowEditLink(false);
		
		poForm.setSaveCallback(saveCallback);

		HLayout poLayout = new HLayout(15);
		poLayout.addMember(poCode);
		poLayout.addMember(status);

		HLayout termLayout = new HLayout(15);
		termLayout.addMember(poDate);
		termLayout.addMember(cancelDate);

		HLayout suppRemarkLayout = new HLayout(15);
		suppRemarkLayout.addMember(supplier);
		suppRemarkLayout.addMember(remarks);

		editForm.setWidth(500);
		editForm.setColWidths(100,100,100,100,100,100);
		editForm.setNumCols(4);
		editForm.setFields(editItemLink, processLinkItem, cancelLinkItem, downloadAsPdfLinkItem, downloadAsTextLinkItem, printBarcodeLinkItem);

		dataComposite.addMember(poLayout);
		dataComposite.addMember(termLayout);
		dataComposite.addMember(suppRemarkLayout);
		dataComposite.addMember(editForm);
		
		initWindow();
		initHandler();
	}

	public void setUpdateStatusCallback(DSCallback updateStatusCallback) {
		this.updateStatusCallback = updateStatusCallback;
	}

	public void setPrintBarcodeCallback(OnPrintBarcodeActionCallback printBarcodeCallback) {
		this.printBarcodeCallback = printBarcodeCallback;
	}

	public void setListGrid(ListGrid listGrid) {
		this.listGrid = listGrid;
	}

	private void initHandler(){
		supplier.addClickHandlerOverride(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				supplierWindow.centerInPage();
				supplierWindow.show();
			}
		});
		processLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				processLinkClicked();
			}
		});
		cancelLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				cancelLinkClicked();
			}
		});
		printBarcodeLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				printBarcodeLinkClicked();
			}
		});		
	}
	
	protected void printBarcodeLinkClicked() {
		printBarcodeCallback.execute();
	}

	private void initWindow(){
		supplierWindow.setWidth(400);
		supplierWindow.setHeight(300);
		supplierWindow.addItem(suppCanvas);
	}
	
	public void loadData(Record r) {
		this.record = r;
		
		String statusStr = r.getAttributeAsString("status");
		
		poCode.setContents(r.getAttributeAsString("poId"));
		status.setContents(statusStr);
		poDate.setContents(r.getAttributeAsDate("poDate"), true);
		cancelDate.setContents(r.getAttributeAsDate("cancelDate"), true);
		remarks.setContents(r.getAttributeAsString("remarks"));
		
		@SuppressWarnings("unchecked")
		Map<String, Object> suppMap = r.getAttributeAsMap("supplier");
		
		supplier.setContents((String)suppMap.get("name"));
		suppCanvas.loadData(new Record(JSOHelper.convertMapToJavascriptObject(suppMap)));
		supplierWindow.setTitle((String)suppMap.get("name"));
		
		if("NEW".equalsIgnoreCase(statusStr)){
			processLinkItem.show();
			cancelLinkItem.show();
			editItemLink.show();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			printBarcodeLinkItem.hide();
			status.setContentStyle("poNewLabel");
		}else if("PROCESSED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.show();
			editItemLink.hide();
			downloadAsPdfLinkItem.show();
			downloadAsTextLinkItem.show();
			printBarcodeLinkItem.show();
			
			Integer id = r.getAttributeAsInt("id");
			downloadAsPdfLinkItem.setValue(IConstants.REST_DS + "/report/po/" + id + "/pdf");
			downloadAsTextLinkItem.setValue(IConstants.REST_DS + "/report/po/" + id + "/txt");
			status.setContentStyle("poProcessedLabel");
		}else if("COMPLETED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.hide();
			editItemLink.hide();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			printBarcodeLinkItem.hide();
			status.setContentStyle("poCompletedLabel");
		}else if("CANCELLED".equalsIgnoreCase(statusStr)){
			processLinkItem.hide();
			cancelLinkItem.hide();
			editItemLink.hide();
			downloadAsPdfLinkItem.hide();
			downloadAsTextLinkItem.hide();
			printBarcodeLinkItem.hide();
			status.setContentStyle("poCancelledLabel");
		}
	}
	
	private void processLinkClicked(){
				
		if(listGrid.getTotalRows() > 0){
			record.setAttribute("status", "PROCESSED");
			poForm.editRecord(record);
			if(!poForm.validate(false)){
				@SuppressWarnings("unchecked")
				Map<String, String> errors = (Map<String, String>)poForm.getErrors();
				String str = "";
				for(Map.Entry<String, String> e : errors.entrySet()){							
				    str = str + poForm.getItem(e.getKey()).getTitle()+": "+e.getValue() + "<br/>";
				}
				SC.warn(str);
			}else{
				SC.ask("Process Purchasing Order?", new BooleanCallback() {			
					public void execute(Boolean value) {
						if(value){
							poForm.editRecord(record);
							poForm.saveData(processCallBack);
						}
					}
				});
			}			
		}else{
			SC.warn("Order Product not found");
		}
		
	}

	private void cancelLinkClicked(){
		SC.ask("Cancel Purchasing Order?", new BooleanCallback() {
			
			public void execute(Boolean value) {
				if(value){
					record.setAttribute("status", "CANCELLED");					
					poForm.editRecord(record);
					poForm.saveData(cancelCallBack);
				}
			}
		});
	}

	public void setPrinterAvailability(Boolean isAvailable){
		if(isAvailable){
			printBarcodeLinkItem.enable();
			printBarcodeLinkItem.setLinkTitle("Print Barcode >>");
		}else{
			printBarcodeLinkItem.disable();
			printBarcodeLinkItem.setLinkTitle("Print Barcode (OFFLINE)");
		}
	}

	protected void editLinkClicked(){
		customizeWindow("Edit Purchasing Order", 250, 300);
		addOrUpdateWindow.centerInPage();
		addOrUpdateWindow.show();
		poForm.editRecord(record);
	}

	protected void buttonWindowClicked(){
		if(poForm.validate(false)){
			poForm.saveData();
		}
	}

}
