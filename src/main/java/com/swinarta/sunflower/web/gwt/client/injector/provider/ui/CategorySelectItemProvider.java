package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.widgets.form.fields.SelectItem;

public class CategorySelectItemProvider implements Provider<SelectItem>{

	final private RestDataSource datasource;
	final private Criteria getAllCriteria;
	
	@Inject
	public CategorySelectItemProvider(@Named("Category") RestDataSource datasource, @Named("GetAll") Criteria getAllCriteria){
		this.datasource = datasource;
		this.getAllCriteria = getAllCriteria;
	}
	
	public SelectItem get() {
		SelectItem item = new SelectItem("categoryId", "Category");		
		item.setOptionCriteria(getAllCriteria);
		item.setOptionDataSource(datasource);
		item.setDisplayField("description");
		item.setValueField("id");
		item.setAutoFetchData(false);
		item.setAnimatePickList(true);		

		return item;
	}

}
