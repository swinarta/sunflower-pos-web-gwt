package com.swinarta.sunflower.web.gwt.client.forms;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.IConstants;
import com.swinarta.sunflower.web.gwt.client.model.ClientDisplayProductMeasurement;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.util.PriceUtil;
import com.swinarta.sunflower.web.gwt.client.util.StringFormatUtil;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.SubmitValuesEvent;
import com.smartgwt.client.widgets.form.events.SubmitValuesHandler;
import com.smartgwt.client.widgets.form.fields.BooleanItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class BuyingForm extends DynamicForm{

	final private CustomComboBoxItem supplierComboBoxItem;

	private Map<Integer, ClientDisplayProductMeasurement> measurementOverrideMap;

	private SelectItem measurementItem = new SelectItem("measurementSelectionId", "Measurement");
	private StaticTextItem costPricePerUnitItem = new StaticTextItem("costPricePerUnitItem", "Cost Price per Unit");
	private TextItem buyingPriceItem = new TextItem("buyingPrice");
	private TextItem disc1Item = new TextItem("disc1");
	private TextItem disc2Item = new TextItem("disc2");
	private TextItem disc3Item = new TextItem("disc3");
	private TextItem disc4Item = new TextItem("disc4");
	private TextItem discPriceItem = new TextItem("discPrice");
	private BooleanItem taxIncludedItem = new BooleanItem();
	private HiddenItem idItem = new HiddenItem("id");
	
	private DSCallback saveCallback;
			
	@Inject
	public BuyingForm(
			@Named("Supplier") CustomComboBoxItem supplierComboBoxItem,
			@Named("Buying") RestDataSource buyingDataSource
			){
		
		this.supplierComboBoxItem = supplierComboBoxItem;
		
		setTitleOrientation(TitleOrientation.TOP);

		setDataSource(buyingDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);
		
		supplierComboBoxItem.setRequired(true);
		measurementItem.setRequired(true);
		costPricePerUnitItem.setRequired(true);
		buyingPriceItem.setRequired(true);
		disc1Item.setRequired(true);
		disc2Item.setRequired(true);
		disc3Item.setRequired(true);
		disc4Item.setRequired(true);
		discPriceItem.setRequired(true);
		
		disc1Item.setDefaultValue(0);
		disc2Item.setDefaultValue(0);
		disc3Item.setDefaultValue(0);
		disc4Item.setDefaultValue(0);
		discPriceItem.setDefaultValue(0);
		
		taxIncludedItem.setName("taxIncluded");
		taxIncludedItem.setDefaultValue(true);
		
		buyingPriceItem.setSelectOnFocus(true);
		disc1Item.setSelectOnFocus(true);
		disc2Item.setSelectOnFocus(true);
		disc3Item.setSelectOnFocus(true);
		disc4Item.setSelectOnFocus(true);
		discPriceItem.setSelectOnFocus(true);
		
		buyingPriceItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		disc1Item.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		disc2Item.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		disc3Item.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		disc4Item.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
		discPriceItem.setKeyPressFilter(IConstants.NUMERIC_REGEXP);
				
		setFields(idItem, supplierComboBoxItem, measurementItem, costPricePerUnitItem, buyingPriceItem, disc1Item, 
				disc2Item, disc3Item, disc4Item, discPriceItem, taxIncludedItem);
		
		initHandler();
	}

	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void setMeasurementOverrideMap(Map<Integer, ClientDisplayProductMeasurement> measurementOverrideMap) {
		this.measurementOverrideMap = measurementOverrideMap;
	}

	private void initHandler(){
		
		addSubmitValuesHandler(new SubmitValuesHandler() {
			
			public void onSubmitValues(SubmitValuesEvent event) {
				saveData();
			}
		});
		
		ChangedHandler priceChangedHandler = new ChangedHandler() {
			
			public void onChanged(ChangedEvent event) {
				calculatePriceOnChange();
			}
		};
		
		buyingPriceItem.addChangedHandler(priceChangedHandler);
		disc1Item.addChangedHandler(priceChangedHandler);
		disc2Item.addChangedHandler(priceChangedHandler);
		disc3Item.addChangedHandler(priceChangedHandler);
		disc4Item.addChangedHandler(priceChangedHandler);
		discPriceItem.addChangedHandler(priceChangedHandler);
		taxIncludedItem.addChangedHandler(priceChangedHandler);
		
	}
	
	public void saveData(){
		super.saveData(saveCallback);
	}
	
	@SuppressWarnings("unchecked")
	public void editRecord(Record record){
				
		Map<String, Object> measurementMap = record.getAttributeAsMap("measurement");
		Integer defaultId = (Integer)measurementMap.get("id");
				
		measurementItem.setDefaultValue(String.valueOf(defaultId));
		measurementItem.setValueMap(constructValueMap(measurementOverrideMap));
		
		Integer supplierId = (Integer) record.getAttributeAsMap("supplier").get("id");
		supplierComboBoxItem.setDefaultValue(supplierId);
		
		super.editRecord(record);
		
		calculatePriceOnChange();
	}
	
	public void editNewRecord(Integer id){
		measurementItem.setValueMap(constructValueMap(measurementOverrideMap));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		
		super.editNewRecord(map);
	}

	private void calculatePriceOnChange(){
		
		Double buyingPrice = null;
		Double disc1 = null;
		Double disc2 = null;
		Double disc3 = null;
		Double disc4 = null;
		Double discPrice = null;
		Integer qty = null;
		Boolean taxIncluded = null;
		
		try{
			buyingPrice = FormUtil.getDoubleValueFromFormItem(buyingPriceItem);
			disc1 = FormUtil.getDoubleValueFromFormItem(disc1Item);
			disc2 = FormUtil.getDoubleValueFromFormItem(disc2Item);
			disc3 = FormUtil.getDoubleValueFromFormItem(disc3Item);
			disc4 = FormUtil.getDoubleValueFromFormItem(disc4Item);
			discPrice = FormUtil.getDoubleValueFromFormItem(discPriceItem);
			taxIncluded = FormUtil.getBooleanValueFromFormItem(taxIncludedItem);
			
			String measurementIdStr = measurementItem.getSelectedRecord().getAttribute("id");
			Integer measurementId = Integer.parseInt(measurementIdStr);
			
			ClientDisplayProductMeasurement measurement = measurementOverrideMap.get(measurementId);
			qty = measurement.getQty();
			
		}catch (Exception e) {
		}
	
		calculateBuyingPrice(buyingPrice, disc1, disc2, disc3, disc4, discPrice, taxIncluded, qty);
		
	}

	private void calculateBuyingPrice(Double buyingPrice, Double disc1, Double disc2
			, Double disc3, Double disc4, Double discPrice, Boolean taxIncluded, Integer qty){
		
		Double costPricePerUnit = 0d;
		
		try{
			costPricePerUnit = PriceUtil.getCostPricePerUnit(buyingPrice, disc1, disc2, disc3, disc4, discPrice, taxIncluded, qty);
		}catch (Exception e) {
		}
		costPricePerUnitItem.setValue(StringFormatUtil.getFormat(costPricePerUnit));
	}

	
	private LinkedHashMap<String, String> constructValueMap(Map<Integer, ClientDisplayProductMeasurement> measurementOverrideMap){
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		for (Integer measurementId : measurementOverrideMap.keySet()) {
			ClientDisplayProductMeasurement measument = measurementOverrideMap.get(measurementId);
			valueMap.put(String.valueOf(measurementId), measument.getDescription() + " (" + measument.getQty() + ")");
		}
		
		return valueMap;
	}
	
}