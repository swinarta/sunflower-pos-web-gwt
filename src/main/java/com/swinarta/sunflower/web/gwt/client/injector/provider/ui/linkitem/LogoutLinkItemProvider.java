package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.Init;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;

public class LogoutLinkItemProvider implements Provider<LinkItem>{

	final private RestDataSource logoutDataSource;
	final private Init init;
	
	@Inject
	public LogoutLinkItemProvider(
			Init init,
			@Named("Logout") RestDataSource logoutDataSource
		){
		this.logoutDataSource = logoutDataSource;
		this.init = init;
	}
	
	public LinkItem get() {
		LinkItem link = new LinkItem("logout");

		link.setTarget("javascript");
		link.setShowTitle(false);
		link.setLinkTitle("Sign out");
		
		link.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				logoutDataSource.fetchData(null, new DSCallback() {
					
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						init.initUserNameLabel();
					}
				});
			}
		});

		return link;
	}

}
