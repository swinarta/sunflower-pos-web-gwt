package com.swinarta.sunflower.web.gwt.client.panel;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransactionSummaryByPaymentListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.TransactionSummaryByStationListGrid;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class TransactionSummaryPanel extends BasePanel{

	private static final String DESCRIPTION = "transactionSummary";
	
    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
            id = DESCRIPTION;
            return injector.getTransactionSummaryPanel();
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }

    final private DateItem transactionDateItem = new DateItem("transactionDate", "Date");
    final private ButtonItem searchButton = new ButtonItem("searchProduct", "Search");;
    final private DynamicForm searchForm = new DynamicForm();
    
    final private TransactionSummaryByStationListGrid gridList;
    final private TransactionSummaryByPaymentListGrid pGridList;
    
	@Inject
	public TransactionSummaryPanel(
			TransactionSummaryByStationListGrid gridList,
			TransactionSummaryByPaymentListGrid pGridList
		){
		
		this.gridList = gridList;
		this.pGridList = pGridList;
		
		init();
		initComponentHandler();
	}

	@Override
	public Canvas getViewPanel() {
		
		transactionDateItem.setRequired(true);
		
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);		
		
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);
		
		searchForm.setFields(transactionDateItem, searchButton);
		
		gridList.setWidth(400);
		gridList.setHeight(1);
		gridList.setOverflow(Overflow.VISIBLE);
		gridList.setBodyOverflow(Overflow.VISIBLE);
		gridList.setShowAllRecords(true);		
		
		pGridList.setWidth(400);
		pGridList.setHeight(1);
		pGridList.setOverflow(Overflow.VISIBLE);
		pGridList.setBodyOverflow(Overflow.VISIBLE);
		pGridList.setShowAllRecords(true);
		
		pGridList.setGroupStartOpen(GroupStartOpen.ALL);  
		pGridList.setGroupByField("paymentType");  
		
		gridList.hide();
		pGridList.hide();
		
		VLayout layout = new VLayout(10);
		layout.setBorder("1px solid gray");
		layout.setLayoutTopMargin(20);
		layout.setLayoutLeftMargin(20);
		layout.setLayoutBottomMargin(20);
		
		layout.addMember(searchForm);
		layout.addMember(gridList);
		layout.addMember(pGridList);
		return layout;
	}
	
	private void initComponentHandler(){
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchTransaction();
				}  
		});		
	}

	protected void searchTransaction() {
		gridList.show();
		gridList.invalidateCache();
		gridList.fetchData(searchForm.getValuesAsCriteria());
		
		pGridList.show();
		pGridList.invalidateCache();
		pGridList.fetchData(searchForm.getValuesAsCriteria());
	}
}
