package com.swinarta.sunflower.web.gwt.client.injector.module;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.swinarta.sunflower.web.gwt.client.injector.provider.validator.BarcodeLengthRangeValidatorProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.validator.MinCompareValidatorProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.validator.MinDateCompareValidatorProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.validator.MinTodayValidatorProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.validator.PositiveFloatRangeValidatorProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.validator.PositiveIntegerRangeValidatorProvider;
import com.swinarta.sunflower.web.gwt.client.injector.provider.validator.SkuLengthRangeValidatorProvider;
import com.swinarta.sunflower.web.gwt.client.validator.CompareValidator;
import com.swinarta.sunflower.web.gwt.client.validator.DateCompareValidator;
import com.swinarta.sunflower.web.gwt.client.validator.IsStringNumberValidator;
import com.swinarta.sunflower.web.gwt.client.validator.IsValidBarcodeValidator;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.form.validator.FloatRangeValidator;
import com.smartgwt.client.widgets.form.validator.IntegerRangeValidator;
import com.smartgwt.client.widgets.form.validator.IsFloatValidator;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;

public class ValidatorModule extends AbstractGinModule{

	@Override
	protected void configure() {
		bind(LengthRangeValidator.class).annotatedWith(Names.named("Sku")).toProvider(SkuLengthRangeValidatorProvider.class).in(Singleton.class);
		bind(LengthRangeValidator.class).annotatedWith(Names.named("Barcode")).toProvider(BarcodeLengthRangeValidatorProvider.class).in(Singleton.class);
		bind(FloatRangeValidator.class).annotatedWith(Names.named("Positive")).toProvider(PositiveFloatRangeValidatorProvider.class).in(Singleton.class);
		bind(IntegerRangeValidator.class).annotatedWith(Names.named("Positive")).toProvider(PositiveIntegerRangeValidatorProvider.class).in(Singleton.class);
		bind(CompareValidator.class).annotatedWith(Names.named("Min")).toProvider(MinCompareValidatorProvider.class).in(Singleton.class);
		bind(DateCompareValidator.class).annotatedWith(Names.named("Min")).toProvider(MinDateCompareValidatorProvider.class);
		bind(DateRangeValidator.class).annotatedWith(Names.named("Today")).toProvider(MinTodayValidatorProvider.class).in(Singleton.class);
		bind(IsStringNumberValidator.class).in(Singleton.class);
		bind(IsValidBarcodeValidator.class).in(Singleton.class);
		bind(IsFloatValidator.class).in(Singleton.class);
		
	}

}
