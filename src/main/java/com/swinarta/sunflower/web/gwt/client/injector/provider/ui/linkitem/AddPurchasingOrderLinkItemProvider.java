package com.swinarta.sunflower.web.gwt.client.injector.provider.ui.linkitem;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.LinkItem;

public class AddPurchasingOrderLinkItemProvider implements Provider<LinkItem>{

	public LinkItem get() {
		LinkItem link = new LinkItem("addPo");

		link.setTarget("javascript");
		link.setShowTitle(false);
		link.setLinkTitle("Add Purchasing Order >>");

		return link;
	}

}
