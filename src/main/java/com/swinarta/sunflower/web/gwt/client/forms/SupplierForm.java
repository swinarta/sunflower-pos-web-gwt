package com.swinarta.sunflower.web.gwt.client.forms;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.SubmitValuesEvent;
import com.smartgwt.client.widgets.form.events.SubmitValuesHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class SupplierForm extends DynamicForm{

	private DSCallback saveCallback;
	
	@Inject
	public SupplierForm(
			@Named("Supplier") RestDataSource supplierDataSource,
			@Named("SupplierCode") TextItem supplierCodeTextItem,
			@Named("SupplierName") TextItem supplierNameTextItem,
			@Named("TermOfPayment") TextItem termOfPaymentTextItem,
			@Named("ContactName") TextItem contactNameTextItem,
			@Named("Address1") TextItem addr1TextItem,
			@Named("Address2") TextItem addr2TextItem,
			@Named("City") TextItem cityTextItem,
			@Named("Phone") TextItem phoneTextItem,
			@Named("Fax") TextItem faxTextItem,
			@Named("Mobile") TextItem mobileTextItem
			){
		setTitleOrientation(TitleOrientation.TOP);		
		setDataSource(supplierDataSource);
		setUseAllDataSourceFields(true);
		setMargin(10);
		setNumCols(1);
		setWidth100();
		setHeight100();
		setSaveOnEnter(true);
				
		setFields(supplierCodeTextItem, supplierNameTextItem, termOfPaymentTextItem,
				contactNameTextItem, addr1TextItem, addr2TextItem, cityTextItem,
				phoneTextItem, faxTextItem, mobileTextItem);

		addSubmitValuesHandler(new SubmitValuesHandler() {
			
			public void onSubmitValues(SubmitValuesEvent event) {
				saveData();
			}
		});

	}
	
	public void setSaveCallback(DSCallback saveCallback) {
		this.saveCallback = saveCallback;
	}

	public void editRecord(Record record){
		clearErrors(true);
		super.editRecord(record);
		setAutoFocus(true);
	}
	
	public void saveData(){
		if(validate()){
			super.saveData(saveCallback);
		}
	}
}
