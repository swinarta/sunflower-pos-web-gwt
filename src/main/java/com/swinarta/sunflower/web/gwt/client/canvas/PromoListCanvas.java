package com.swinarta.sunflower.web.gwt.client.canvas;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.widget.AddProductToDetailWindow;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PromoDetailListGrid;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

public class PromoListCanvas extends VLayout{

	final private PromoDetailListGrid promoList;
	final private AddProductToDetailWindow addProductWindow;
	final private LinkItem addProductLinkItem;
	
	@Inject
	public PromoListCanvas(
			PromoDetailListGrid promoList,
			@Named("AddProduct") LinkItem addProductLinkItem,
			AddProductToDetailWindow addProductWindow
			){
		
		this.promoList = promoList;
		this.addProductLinkItem = addProductLinkItem;
		this.addProductWindow = addProductWindow;
		
		setGroupTitle("Promo Details");
		setIsGroup(true);
		setCanSelectText(true);
		setLayoutTopMargin(8);
		setLayoutLeftMargin(8);
		setLayoutRightMargin(8);
		setLayoutBottomMargin(8);
		setMargin(3);
		
		addProductWindow.setHasSelling(true);
				
		DynamicForm addProductForm = new DynamicForm();				
		addProductForm.setFields(addProductLinkItem);
		
		addProductWindow.setAddProductToDetail(promoList);
		
		addMember(addProductForm);
		addMember(promoList);
		
		initWindow();
		initHandler();
		
	}
	
	private void initWindow() {
		addProductWindow.hide();
	}

	private void initHandler(){
		addProductLinkItem.addClickHandler(new ClickHandler() {			
			public void onClick(ClickEvent event) {
				addProductWindow.centerInPage();
				addProductWindow.show();
			}
		});
	}

	public void setParentLayout(Layout parent){
		parent.addChild(addProductWindow);
	}

	public void loadData(Record record){		
		Integer promoId = record.getAttributeAsInt("id");
		
		Criteria c = new Criteria();
		c.setAttribute("promoId", promoId);
		promoList.fetchData(c);
		promoList.setPromoRecord(record);
	}

}
