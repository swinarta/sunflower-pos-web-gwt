package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import com.google.inject.Provider;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Label;

public class NotActiveLabelProvider implements Provider<Label>{

	public Label get() {

		Label label = new Label();
		label.setHeight(20);  
		label.setPadding(10);
		label.setAlign(Alignment.CENTER);  
		label.setValign(VerticalAlignment.CENTER);
		label.setWrap(false);
		label.setIcon("sunflower/no.png");
		label.setContents("<h3><p style=\"color: red;\">PRODUCT IS NOT ACTIVE</p></h3>");		
		return label;
		
	}

}
