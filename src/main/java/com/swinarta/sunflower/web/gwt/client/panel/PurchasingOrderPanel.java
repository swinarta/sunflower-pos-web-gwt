package com.swinarta.sunflower.web.gwt.client.panel;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.swinarta.sunflower.web.gwt.client.PanelFactory;
import com.swinarta.sunflower.web.gwt.client.forms.PurchasingOrderForm;
import com.swinarta.sunflower.web.gwt.client.injector.Ginjector;
import com.swinarta.sunflower.web.gwt.client.injector.module.GardenGinjector;
import com.swinarta.sunflower.web.gwt.client.util.FormUtil;
import com.swinarta.sunflower.web.gwt.client.widget.CustomComboBoxItem;
import com.swinarta.sunflower.web.gwt.client.widget.listgrid.PurchasingOrderListGrid;
import com.swinarta.sunflower.web.gwt.client.widget.tabset.PurchasingOrderTabSet;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.DoubleClickEvent;
import com.smartgwt.client.widgets.events.DoubleClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

public class PurchasingOrderPanel extends BasePanel{

	private static final String DESCRIPTION = "purchasingorder";

    public static class Factory implements PanelFactory {
        private String id;
        private GardenGinjector injector = Ginjector.getInstance();

        public Canvas create() {
        	PurchasingOrderPanel panel = injector.getPurchasingOrderPanel();
        	id = DESCRIPTION;
            return panel;
        }

        public String getID() {
            return id;
        }

        public String getDescription() {
          return DESCRIPTION;
        }
    }

    private DynamicForm addPoForm;
    private DynamicForm searchForm;
    private TextItem searchTextItem;
    private ButtonItem searchButton;
    
    final private SelectItem poStatusSelectItem;
    final private CustomComboBoxItem supplierComboBoxItem;
    final private PurchasingOrderListGrid listGrid;
    final private Tab searchPoTab;
    final private PurchasingOrderTabSet purchasingOrderTabSet;
    final private LinkItem addPurchasingOrderLinkItem;
    final private Window addPurchasingOrderWindow;
    final private DynamicForm purchasingOrderForm;
    final private Button saveButton;
    
    @Inject
    public PurchasingOrderPanel(
    		PurchasingOrderTabSet purchasingOrderTabSet,
    		@Named("POStatus") SelectItem poStatusSelectItem,
    		@Named("SupplierAny") CustomComboBoxItem supplierComboBoxItem,
    		PurchasingOrderListGrid listGrid,
    		@Named("SearchPurchasingOrder") Tab searchPoTab,
    		@Named("AddPurchasingOrder") LinkItem addPurchasingOrderLinkItem,
    		@Named("AddPurchasingOrder") Window addPurchasingOrderWindow,
    		PurchasingOrderForm purchasingOrderForm,
    		@Named("SaveInForm") Button saveButton
    		){
    	
    	this.supplierComboBoxItem = supplierComboBoxItem;
    	this.poStatusSelectItem = poStatusSelectItem;
    	this.listGrid = listGrid;
    	this.searchPoTab = searchPoTab;
    	this.purchasingOrderTabSet = purchasingOrderTabSet;
    	this.addPurchasingOrderLinkItem = addPurchasingOrderLinkItem;
    	this.addPurchasingOrderWindow = addPurchasingOrderWindow;
    	this.purchasingOrderForm = purchasingOrderForm;
    	this.saveButton = saveButton;
    	
    	init();
    	initComponentHandler();
		initWindow();    	
    	
    }
    
	@Override
	public Canvas getViewPanel() {
		
		addPoForm = new DynamicForm();
		addPoForm.setWidth(300);
		addPoForm.setFields(addPurchasingOrderLinkItem);
		
		searchTextItem = new TextItem("searchPO");  
		searchTextItem.setTitle("Search PO");  
		searchTextItem.setSelectOnFocus(true);
		searchTextItem.setWrapTitle(false);
		searchTextItem.setCharacterCasing(CharacterCasing.UPPER);

		searchForm = new DynamicForm();
		searchForm.setAutoFocus(true);
		searchForm.setNumCols(3);
		searchForm.setWidth(400);

		searchButton = new ButtonItem("searchProduct", "Search");
		searchButton.setTitle("Search");
		searchButton.setStartRow(false);
		searchButton.setWidth(80);
		searchButton.setIcon("icons/message.png");
		searchButton.setDisabled(false);
		
		searchForm.setFields(supplierComboBoxItem, poStatusSelectItem, searchTextItem, searchButton);		
		
		listGrid.setWidth(610);
		listGrid.setHeight(600);
		listGrid.hide();
		
		VLayout c = new VLayout();
		c.addMember(addPoForm);
		c.addMember(searchForm);
		c.addMember(listGrid);
				
		c.setLayoutTopMargin(10);
		c.setLayoutLeftMargin(25);
		
		searchPoTab.setPane(c);
		
		purchasingOrderTabSet.addTab(searchPoTab);
		
		//Record record = new Record();
		//Response: {"response":{"data":[{"status":"NEW","supplier":{"name":"BOSTEK","supplierCode":"304","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":1,"version":1,"createdDt":1216196777000,"updatedDt":1216196777000,"createdBy":2,"updatedBy":2,"deleteInd":false,"id":452},"poId":"P094526091","poDate":1252771200000,"remarks":"","cancelDate":1253376000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":6091},{"status":"NEW","supplier":{"name":"ANTARMITRA SEMBADA, PT","supplierCode":"AMS","address1":"JL. PENGASINAN RAYA NO. 32 ","address2":"RAWA LUMBU","city":"BEKASI","phone":"02679105030","fax":"021-8219342","mobile":"021-82434582","contactName":"MARCHEL","termOfPayment":30,"version":4,"createdDt":1197702688000,"updatedDt":1241754041000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":91},"poId":"P090916009","poDate":1242144000000,"remarks":"","cancelDate":1242748800000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":6009},{"status":"NEW","supplier":{"name":"PAIS","supplierCode":"PAIS","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":1,"version":1,"createdDt":1240625173000,"updatedDt":1240625173000,"createdBy":27,"updatedBy":27,"deleteInd":false,"id":602},"poId":"P096025957","poDate":1242057600000,"remarks":"","cancelDate":1242662400000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5957},{"status":"NEW","supplier":{"name":"STUP","supplierCode":"STUP","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":0,"version":2,"createdDt":1215657914000,"updatedDt":1229651488000,"createdBy":2,"updatedBy":2,"deleteInd":false,"id":421},"poId":"P094215901","poDate":1241971200000,"remarks":"","cancelDate":1242576000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5901},{"status":"NEW","supplier":{"name":"CI TITIN ","supplierCode":"TITIN","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"085691749001","contactName":"","termOfPayment":0,"version":3,"createdDt":1215658099000,"updatedDt":1240362284000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":423},"poId":"P094235855","poDate":1241798400000,"remarks":"","cancelDate":1242403200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5855},{"status":"NEW","supplier":{"name":"DAYA MUDA AGUNG, PT","supplierCode":"DMA","address1":"JL.HOLIS NO.294/12A BANDUNG","address2":"BANDUNG","city":"BANDUNG","phone":"022-6016644","fax":"022-6016691","mobile":"","contactName":"","termOfPayment":0,"version":2,"createdDt":1197702688000,"updatedDt":1209549652000,"createdBy":1,"updatedBy":2,"deleteInd":false,"id":136},"poId":"P091365635","poDate":1241280000000,"remarks":"","cancelDate":1241884800000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5635},{"status":"NEW","supplier":{"name":"GRAHA PANGAN LESTARI, PT","supplierCode":"GPL","address1":"","address2":"","city":"KARAWANG","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":0,"version":3,"createdDt":1197702688000,"updatedDt":1209527784000,"createdBy":1,"updatedBy":2,"deleteInd":false,"id":37},"poId":"P090375500","poDate":1240934400000,"remarks":"","cancelDate":1241539200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5500},{"status":"NEW","supplier":{"name":"GUNUNG SLAMAT, PT","supplierCode":"GSL","address1":"JL.SUROTO KUNTO NO.22","address2":"KARAWANG","city":"KARAWANG","phone":"0267-402567","fax":"085285589354","mobile":"","contactName":"INE","termOfPayment":30,"version":6,"createdDt":1197702688000,"updatedDt":1241591532000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":152},"poId":"P091525498","poDate":1240934400000,"remarks":"","cancelDate":1241539200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5498},{"status":"NEW","supplier":{"name":"MITRA PERIANGAN PERSADA, PT","supplierCode":"MPPS","address1":"JL TUPAREV 667","address2":"BY PASS","city":"CIKAMPEK","phone":"69884575","fax":"08882055656","mobile":"0264822610","contactName":"YAYAN S","termOfPayment":30,"version":7,"createdDt":1197702688000,"updatedDt":1279043977000,"createdBy":1,"updatedBy":999,"deleteInd":false,"id":26},"poId":"P090265497","poDate":1240934400000,"remarks":"","cancelDate":1241539200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5497},{"status":"NEW","supplier":{"name":"MITRA SARANA PURNAMA, PT","supplierCode":"MSP","address1":"JL.RAYA BOGOR KM.31","address2":"","city":"CIMANGGIS","phone":"021-87706252","fax":"08568042443","mobile":"0218710180","contactName":"fachrul","termOfPayment":0,"version":3,"createdDt":1197702688000,"updatedDt":1241579445000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":191},"poId":"P091915324","poDate":1240502400000,"remarks":"","cancelDate":1241107200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5324},{"status":"NEW","supplier":{"name":"MITRA SARANA PURNAMA, PT","supplierCode":"MSP","address1":"JL.RAYA BOGOR KM.31","address2":"","city":"CIMANGGIS","phone":"021-87706252","fax":"08568042443","mobile":"0218710180","contactName":"fachrul","termOfPayment":0,"version":3,"createdDt":1197702688000,"updatedDt":1241579445000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":191},"poId":"P091915325","poDate":1240502400000,"remarks":"","cancelDate":1241107200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5325},{"status":"NEW","supplier":{"name":"GRAHA KERINDO UTAMA, PT","supplierCode":"GKU","address1":"JL.GAJAH MADA NO 19-26","address2":"","city":"JAKARTA","phone":"0216338832","fax":"021-2601262","mobile":"021-91045714","contactName":"RONNY","termOfPayment":0,"version":3,"createdDt":1197702688000,"updatedDt":1256785037000,"createdBy":1,"updatedBy":27,"deleteInd":false,"id":205},"poId":"P092055284","poDate":1240416000000,"remarks":"","cancelDate":1241020800000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5284},{"status":"NEW","supplier":{"name":"SETUJU","supplierCode":"STJ","address1":"JL.TERMINAL NO.14","address2":"","city":"","phone":"0264-314771","fax":"","mobile":"","contactName":"","termOfPayment":0,"version":2,"createdDt":1197702688000,"updatedDt":1209527849000,"createdBy":1,"updatedBy":2,"deleteInd":false,"id":30},"poId":"P090305251","poDate":1240329600000,"remarks":"","cancelDate":1240934400000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5251},{"status":"NEW","supplier":{"name":"SETUJU","supplierCode":"STJ","address1":"JL.TERMINAL NO.14","address2":"","city":"","phone":"0264-314771","fax":"","mobile":"","contactName":"","termOfPayment":0,"version":2,"createdDt":1197702688000,"updatedDt":1209527849000,"createdBy":1,"updatedBy":2,"deleteInd":false,"id":30},"poId":"P090305250","poDate":1240329600000,"remarks":"","cancelDate":1240934400000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5250},{"status":"NEW","supplier":{"name":"MITRA PERIANGAN PERSADA, PT","supplierCode":"MPPS","address1":"JL TUPAREV 667","address2":"BY PASS","city":"CIKAMPEK","phone":"69884575","fax":"08882055656","mobile":"0264822610","contactName":"YAYAN S","termOfPayment":30,"version":7,"createdDt":1197702688000,"updatedDt":1279043977000,"createdBy":1,"updatedBy":999,"deleteInd":false,"id":26},"poId":"P090265248","poDate":1240329600000,"remarks":"","cancelDate":1240934400000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5248},{"status":"NEW","supplier":{"name":"INDOMARCO ADIPRIMA, PT","supplierCode":"INDOM","address1":"JL.JABABEKA RAYA BLOK A NO.6-15","address2":"INDUSTRIAL ESTATE","city":"CIKARANG","phone":"021-8934266","fax":"021-8934497","mobile":"021-89830257","contactName":"JOHAN","termOfPayment":14,"version":7,"createdDt":1197702688000,"updatedDt":1259562199000,"createdBy":1,"updatedBy":25,"deleteInd":false,"id":119},"poId":"P091195210","poDate":1240243200000,"remarks":"","cancelDate":1240848000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5210},{"status":"NEW","supplier":{"name":"IGATAMA NUSANTARA PERMAI, PT","supplierCode":"IGA","address1":"JL PEGANGSAAN INDAH BARAT 33","address2":"KELAPA GADING","city":"JAKARTA","phone":"021-45840908","fax":"021-45846178","mobile":"","contactName":"","termOfPayment":30,"version":3,"createdDt":1197702688000,"updatedDt":1236319915000,"createdBy":1,"updatedBy":27,"deleteInd":false,"id":215},"poId":"P092155218","poDate":1240243200000,"remarks":"","cancelDate":1240848000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5218},{"status":"NEW","supplier":{"name":"PUJI SURYA INDAH, PT","supplierCode":"PSI","address1":"","address2":"","city":"BANDUNG","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":30,"version":2,"createdDt":1197702688000,"updatedDt":1253951995000,"createdBy":1,"updatedBy":27,"deleteInd":false,"id":301},"poId":"P093015183","poDate":1240156800000,"remarks":"","cancelDate":1240761600000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5183},{"status":"NEW","supplier":{"name":"KUKU BIMA, PT","supplierCode":"BIMA","address1":"JAKARTA","address2":"","city":"JAKARTA","phone":"","fax":"","mobile":"081381177665","contactName":"WITANTO","termOfPayment":1,"version":2,"createdDt":1235360705000,"updatedDt":1236314702000,"createdBy":27,"updatedBy":27,"deleteInd":false,"id":583},"poId":"P095835182","poDate":1240156800000,"remarks":"","cancelDate":1240761600000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5182},{"status":"NEW","supplier":{"name":"GIRI PUTRA PRATAMA, CV","supplierCode":"GIRI","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":0,"version":2,"createdDt":1197702688000,"updatedDt":1209383948000,"createdBy":1,"updatedBy":2,"deleteInd":false,"id":14},"poId":"P090145181","poDate":1240156800000,"remarks":"","cancelDate":1240761600000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5181},{"status":"NEW","supplier":{"name":"PARIT PADANG,PT","supplierCode":"PARIT","address1":"JL.PULO KAMBING II NO.26","address2":"","city":"JAKARTA TIMUR","phone":"021-46823615","fax":"021-4615492","mobile":"085281375255","contactName":"martinus","termOfPayment":0,"version":3,"createdDt":1197702688000,"updatedDt":1241580322000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":55},"poId":"P090555178","poDate":1240156800000,"remarks":"","cancelDate":1240761600000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5178},{"status":"NEW","supplier":{"name":"DUTAMASINDO (DLJ), PT","supplierCode":"DLJ","address1":"JL.AHMAD YANI NO.31","address2":"KARAWANG","city":"KARAWANG","phone":"0267-8454661","fax":"0267-8454663","mobile":"","contactName":"TEDDY-MGR","termOfPayment":30,"version":6,"createdDt":1197702688000,"updatedDt":1259804957000,"createdBy":1,"updatedBy":28,"deleteInd":false,"id":9},"poId":"P090095177","poDate":1240156800000,"remarks":"","cancelDate":1240761600000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":5177},{"status":"NEW","supplier":{"name":"WICAKSANA, PT","supplierCode":"WICAK","address1":"JL ANCOL BARAT VII BLK A5D NO.2","address2":"ANCOL-PENJARINGAN","city":"JAKARTA","phone":"021-8980620","fax":"","mobile":"021-8980621","contactName":"HARJONI","termOfPayment":30,"version":4,"createdDt":1197702688000,"updatedDt":1242352409000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":128},"poId":"P091284814","poDate":1239292800000,"remarks":"","cancelDate":1239897600000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":4814},{"status":"NEW","supplier":{"name":"BU HAJI","supplierCode":"HAJI","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"601946","contactName":"","termOfPayment":0,"version":3,"createdDt":1215666842000,"updatedDt":1240362415000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":430},"poId":"P094303895","poDate":1236787200000,"remarks":"","cancelDate":1237392000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":3895},{"status":"NEW","supplier":{"name":"SARI ROTI, CV","supplierCode":"SROTI","address1":"CIKARANG","address2":"","city":"CIKARANG","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":7,"version":2,"createdDt":1202802077000,"updatedDt":1219660917000,"createdBy":1,"updatedBy":2,"deleteInd":false,"id":377},"poId":"P093773497","poDate":1235750400000,"remarks":"","cancelDate":1236355200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":3497},{"status":"NEW","supplier":{"name":"TITIK","supplierCode":"TITIK","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"0856412409","contactName":"","termOfPayment":0,"version":3,"createdDt":1215659890000,"updatedDt":1240361082000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":424},"poId":"P094242966","poDate":1234281600000,"remarks":"","cancelDate":1234886400000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2966},{"status":"NEW","supplier":{"name":"KUE KO","supplierCode":"KO","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"402475","contactName":"","termOfPayment":0,"version":3,"createdDt":1215674865000,"updatedDt":1240362318000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":431},"poId":"P094312892","poDate":1234108800000,"remarks":"","cancelDate":1234713600000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2892},{"status":"NEW","supplier":{"name":"ROTI SOLO","supplierCode":"ROTI","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":1,"version":1,"createdDt":1230095076000,"updatedDt":1230095076000,"createdBy":2,"updatedBy":2,"deleteInd":false,"id":555},"poId":"P095552843","poDate":1233936000000,"remarks":"","cancelDate":1234540800000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2843},{"status":"NEW","supplier":{"name":"ROTI SOLO","supplierCode":"ROTI","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":1,"version":1,"createdDt":1230095076000,"updatedDt":1230095076000,"createdBy":2,"updatedBy":2,"deleteInd":false,"id":555},"poId":"P095552841","poDate":1233936000000,"remarks":"","cancelDate":1234540800000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2841},{"status":"NEW","supplier":{"name":"CI TITIN ","supplierCode":"TITIN","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"085691749001","contactName":"","termOfPayment":0,"version":3,"createdDt":1215658099000,"updatedDt":1240362284000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":423},"poId":"P094232682","poDate":1233590400000,"remarks":"","cancelDate":1234195200000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2682},{"status":"NEW","supplier":{"name":"WITA CAKE","supplierCode":"WITA","address1":"SENTIONG","address2":"","city":"","phone":"","fax":"","mobile":"","contactName":"","termOfPayment":30,"version":3,"createdDt":1197702688000,"updatedDt":1224981509000,"createdBy":1,"updatedBy":2,"deleteInd":false,"id":323},"poId":"P093232669","poDate":1233504000000,"remarks":"","cancelDate":1234108800000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2669},{"status":"NEW","supplier":{"name":"TITIK","supplierCode":"TITIK","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"0856412409","contactName":"","termOfPayment":0,"version":3,"createdDt":1215659890000,"updatedDt":1240361082000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":424},"poId":"P094242045","poDate":1231776000000,"remarks":"","cancelDate":1232380800000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2045},{"status":"NEW","supplier":{"name":"SARIMOS BUANA SAKTI, PT","supplierCode":"SBS","address1":"SUROTOKUNTO NO.36","address2":"","city":"KARAWANG","phone":"0267408259","fax":"","mobile":"","contactName":"","termOfPayment":0,"version":3,"createdDt":1197702688000,"updatedDt":1240969647000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":67},"poId":"P090672005","poDate":1231603200000,"remarks":"","cancelDate":1232208000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":2005},{"status":"NEW","supplier":{"name":"PENTA VALENT ","supplierCode":"PENTA","address1":"JL. MALEBER BARAT NO. 8","address2":"","city":"BANDUNG","phone":"022-6013278","fax":"022-6016811","mobile":"022-70985175","contactName":"ROEDIN.F","termOfPayment":30,"version":4,"createdDt":1217570216000,"updatedDt":1228189343000,"createdBy":2,"updatedBy":2,"deleteInd":false,"id":476},"poId":"P084760906","poDate":1228147200000,"remarks":"PO harap dibawa sewaktu pengiriman","cancelDate":1228752000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":906},{"status":"NEW","supplier":{"name":"BU HAJI","supplierCode":"HAJI","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"601946","contactName":"","termOfPayment":0,"version":3,"createdDt":1215666842000,"updatedDt":1240362415000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":430},"poId":"P084300258","poDate":1225641600000,"remarks":"","cancelDate":1226246400000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":258},{"status":"NEW","supplier":{"name":"CI MEMEY","supplierCode":"memey","address1":"","address2":"","city":"","phone":"","fax":"","mobile":"085924082015","contactName":"","termOfPayment":0,"version":4,"createdDt":1215666409000,"updatedDt":1240447391000,"createdBy":2,"updatedBy":25,"deleteInd":false,"id":429},"poId":"P084290130","poDate":1225123200000,"remarks":"","cancelDate":1225728000000,"deliverStoreId":1,"version":2,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":130},{"status":"NEW","supplier":{"name":"DOS NI ROHA, PT","supplierCode":"DNR","address1":"JL. MT HARYONO KAV 10","address2":"TEBET","city":"JAKARTA","phone":"mifta","fax":"400121","mobile":"","contactName":"","termOfPayment":0,"version":3,"createdDt":1197702688000,"updatedDt":1240969994000,"createdBy":1,"updatedBy":26,"deleteInd":false,"id":13},"poId":"P080130018","poDate":1217347200000,"remarks":"PO harap di bawa sewaktu pengiriman","cancelDate":1217952000000,"deliverStoreId":1,"version":1,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":18},{"status":"NEW","supplier":{"name":"INDOMOP MULTI MAKMUR, PT","supplierCode":"IMM","address1":"TAMAN TEKNO BLOK E1 NO. 21","address2":"BUMI SERPONG DAMAI, 15314","city":"TANGERANG","phone":"021-7560425","fax":"021-75882456","mobile":"","contactName":"BUDI ISWANTO","termOfPayment":30,"version":4,"createdDt":1197702688000,"updatedDt":1259200525000,"createdBy":1,"updatedBy":27,"deleteInd":false,"id":213},"poId":"P082130017","poDate":1213286400000,"remarks":"","cancelDate":1213891200000,"deliverStoreId":1,"version":1,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":17},{"status":"NEW","supplier":{"name":"INDOMOP MULTI MAKMUR, PT","supplierCode":"IMM","address1":"TAMAN TEKNO BLOK E1 NO. 21","address2":"BUMI SERPONG DAMAI, 15314","city":"TANGERANG","phone":"021-7560425","fax":"021-75882456","mobile":"","contactName":"BUDI ISWANTO","termOfPayment":30,"version":4,"createdDt":1197702688000,"updatedDt":1259200525000,"createdBy":1,"updatedBy":27,"deleteInd":false,"id":213},"poId":"P082130015","poDate":1211472000000,"remarks":"","cancelDate":1212076800000,"deliverStoreId":1,"version":1,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":15},{"status":"NEW","supplier":{"name":"INDOMOP MULTI MAKMUR, PT","supplierCode":"IMM","address1":"TAMAN TEKNO BLOK E1 NO. 21","address2":"BUMI SERPONG DAMAI, 15314","city":"TANGERANG","phone":"021-7560425","fax":"021-75882456","mobile":"","contactName":"BUDI ISWANTO","termOfPayment":30,"version":4,"createdDt":1197702688000,"updatedDt":1259200525000,"createdBy":1,"updatedBy":27,"deleteInd":false,"id":213},"poId":"P082130004","poDate":1206288000000,"remarks":"","cancelDate":1206892800000,"deliverStoreId":1,"version":1,"createdDt":null,"updatedDt":null,"createdBy":null,"updatedBy":null,"deleteInd":false,"id":4}],"status":0,"errors":null,"startRow":0,"endRow":40,"totalRows":40}}
		//record.setAttribute("status", "NEW");
		//purchasingOrderTabSet.loadPO(record);
		
		return purchasingOrderTabSet;
	}
	
	private void initComponentHandler(){
		addPurchasingOrderLinkItem.addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				addPurchasingOrderWindow.centerInPage();
				addPurchasingOrderWindow.show();

				purchasingOrderForm.editNewRecord();
			}
		});
		
		saveButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				performValidateAndSave();
			}
		});
		
		searchButton.addClickHandler(new ClickHandler() {  
			public void onClick(ClickEvent event) {  
				searchPO();
				}  
			});
		
		searchTextItem.addKeyUpHandler(new KeyUpHandler() {			
			public void onKeyUp(KeyUpEvent event) {
				if(event.getKeyName().equalsIgnoreCase("enter")){
					searchPO();
				}				
			}
		});
		listGrid.addDoubleClickHandler(new DoubleClickHandler() {

			public void onDoubleClick(DoubleClickEvent event) {
				PurchasingOrderListGrid listGrid = (PurchasingOrderListGrid)event.getSource();
				purchasingOrderTabSet.loadPO(listGrid.getSelectedRecord());
			}
		});
		
	}

	private void performValidateAndSave(){
		if(purchasingOrderForm.validate()){
			purchasingOrderForm.saveData(new DSCallback() {						
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					purchasingOrderTabSet.loadPO(response.getData()[0]);
					addPurchasingOrderWindow.hide();
				}
			});
		}
	}
	
	private void initWindow(){
		addPurchasingOrderWindow.setTitle("Add Purchasing Order");
		addPurchasingOrderWindow.setWidth(250);
		addPurchasingOrderWindow.setHeight(340);
		
		addPurchasingOrderWindow.addItem(purchasingOrderForm);
		addPurchasingOrderWindow.addItem(saveButton);
	}

	private void searchPO(){
		String text = searchForm.getValueAsString("searchPO");
		Integer supplierId = FormUtil.getIntegerValueFromFormItem(supplierComboBoxItem);
		String poStatus = FormUtil.getStringValueFromFormItem(poStatusSelectItem);
		
		if((text != null && text.length() > 3) || 
				(supplierId != null && supplierId != -1) ||
				(poStatus != null && !poStatus.equals("")) 
				){
			listGrid.show();
			listGrid.fetchData(searchForm.getValuesAsCriteria());
		}
		searchForm.focus();	
	}
	
}