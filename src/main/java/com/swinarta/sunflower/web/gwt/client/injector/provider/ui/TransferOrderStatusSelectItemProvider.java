package com.swinarta.sunflower.web.gwt.client.injector.provider.ui;

import java.util.LinkedHashMap;

import com.google.inject.Provider;
import com.smartgwt.client.widgets.form.fields.SelectItem;

public class TransferOrderStatusSelectItemProvider implements Provider<SelectItem>{
	
	public SelectItem get() {
		
		SelectItem selectItem = new SelectItem();
		
		selectItem.setName("toStatusStr");
		selectItem.setTitle("Transfer Order Status");
		selectItem.setShowAllOptions(true);
		selectItem.setAnimatePickList(true);
		selectItem.setAutoFetchData(false);
		
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("", "---");
		valueMap.put("NEW", "NEW");
		valueMap.put("PROCESSED", "PROCESSED");
		valueMap.put("CANCELLED", "CANCELLED");
		valueMap.put("COMPLETED", "COMPLETED");
		selectItem.setValueMap(valueMap);
		
		selectItem.setDefaultValue("NEW");
		
		return selectItem;
	}

}
