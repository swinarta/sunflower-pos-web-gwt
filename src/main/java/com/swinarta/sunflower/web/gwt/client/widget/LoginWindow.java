package com.swinarta.sunflower.web.gwt.client.widget;

import com.google.inject.Inject;
import com.swinarta.sunflower.web.gwt.client.LoginSuccessfullCallback;
import com.swinarta.sunflower.web.gwt.client.forms.LoginForm;
import com.smartgwt.client.widgets.Window;

public class LoginWindow extends Window{

	final private LoginForm loginForm;
	
	@Inject
	public LoginWindow(LoginForm loginForm){
		
		this.loginForm = loginForm;
		
        setShowModalMask(true);
        centerInPage();
        setShowCloseButton(false);
        setShowMinimizeButton(false);
        setIsModal(true);
        setAutoSize(true);
        setTitle("Authentication");
        
        addItem(loginForm);
	}
	
	public void show(){
		loginForm.clearValues();
		loginForm.focus();
		super.show();
	}

	public void setLoginSuccessfullCallback(LoginSuccessfullCallback loginSuccessfullCallback) {
		loginForm.setCallback(loginSuccessfullCallback);
	}
	
}
