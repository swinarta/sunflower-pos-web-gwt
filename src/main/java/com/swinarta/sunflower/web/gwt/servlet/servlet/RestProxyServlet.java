package com.swinarta.sunflower.web.gwt.servlet.servlet;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;

public class RestProxyServlet extends ProxyServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4712024152638623596L;

	private Logger logger = Logger.getLogger("RestProxyServlet");
	
	@Override
	public void init(ServletConfig servletConfig) {
				
		String proxyHost = "localhost";
		Integer proxyPort = 8080;
		
		InitialContext ic;
		try {

			ic = new InitialContext();
			proxyHost = (String)ic.lookup("java:comp/env/seedProxyHost");
			proxyPort = (Integer)ic.lookup("java:comp/env/seedProxyPort");

			setFollowRedirects(true);
			setRemovePrefix(true);
			setProxyPort(proxyPort);
			setProxyHost(proxyHost);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "error:" + e.getMessage());
		}
		
	}
}
